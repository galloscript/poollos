/// <reference path="../../../src/Poollos.ts"/>


class Bullet extends PSSprite
{
	constructor(texturePath : string, _locX : number, _locY : number, _lifetime : number, _dirX : number, _dirY : number)
	{
		super(texturePath);
		this.x = _locX;
		this.y = _locY;
		this.lifetime = _lifetime;
		this.directionX = _dirX;
		this.directionY = _dirY;
	}
	
	setupCollider(radius : number)
	{
		this.circleCollider = new PSCircle(this.x, this.y, radius);
	}
	
	update(clock : PSClock) : void
	{
		super.update(clock)
		this.circleCollider.updateLocation(this.x, this.y);
	}
	
	directionX : number;
	directionY : number;
	speed : number;
	lifetime : number;
	circleCollider : PSCircle;
};

class SmallBullet extends Bullet
{
	constructor(_locX : number, _locY : number, _lifetime : number, _dirX : number, _dirY : number)
	{
		super('./assets/smallbullet.png', _locX, _locY, _lifetime, _dirX, _dirY);
		this.setupCollider(this.m_Texture.width);
	}
};

class ProjectileGenerator extends PSEntity
{
		
};