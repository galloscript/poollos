/// <reference path="../../../src/Poollos.ts"/>
/// <reference path="./Monster.ts"/>
/// <reference path="./Globals.ts"/>
/// <reference path="./ProjectileGenerator.ts"/>

class Game extends PSGame
{	
	init() : void
	{
		this.setupCanvas('PongCanvas');
		//this.m_PostProcessPipeline = new PSPostProcessPipeline(PSBuiltInGfx.PS_GLOW_EFFECT);
		//this.m_PostProcessPipeline.init(this.m_RenderSupport, this.m_RenderSupport.m_GLContext, PSScreen.width, PSScreen.height, 2);
	}

	loadResources(manager : PSResourceManager) : void
	{
		PSTexture.TEXTURE_FILTER_STYLE = ETexFilterStyle.TS_PIXELART;
		
		manager.verbose = true;
		
		var textures = [];
		textures.push(['./assets/monster.png', false]);
		textures.push(['./assets/smallbullet.png', false]);
		textures.push(['./assets/mediumbullet.png', false]);
		textures.push(['./assets/skyline.png', true]);
		
		for(var i = 0; i < textures.length; i++){ manager.loadResource(new PSTexture(textures[i][0], textures[i][1])); }
	}

	setupScene() : void
	{
		this.m_ClearColor.setValues(0.6, 0.6, 0.9, 1.0);

		this.m_Skyline = new PSSprite('./assets/skyline.png');
		this.m_Skyline.x = 0;
		this.m_Skyline.y = PSScreen.height - this.m_Skyline.height;
		this.m_SkyRepeats = PSScreen.width / this.m_Skyline.width;
		this.m_Skyline.width = PSScreen.width;
		this.m_Skyline.setTextureTransform(this.m_SkyPan, 0, this.m_SkyRepeats, 1, 0);
		this.stage.addChild(this.m_Skyline);

		this.m_Monster = new Monster();
		this.m_Monster.x = 80;
		this.m_Monster.y = 80;
		this.stage.addChild(this.m_Monster);
		
		this.m_Projectiles = new ProjectileGenerator();
		this.stage.addChild(this.m_Projectiles);
		
		this.m_PointsText = new PSTextSprite('0', 42);
		this.m_PointsText.color.setValues(1.0, 1.0, 1.0, 1.0);
		this.m_PointsText.compose();
		this.m_PointsText.pivotX = 0.5;
		this.m_PointsText.x = PSScreen.width * 0.5;
		this.m_PointsText.y = 20;
		this.stage.addChild(this.m_PointsText);
	}	

	update(clock : PSClock) : void
	{
		super.update(clock);
		
		this.m_PointsCount += clock.deltaTime * 8;
		this.m_PointsText.setText(Math.floor(this.m_PointsCount)+'');
		this.m_SkyPan += clock.deltaTime * 0.08;
		this.m_Skyline.setTextureTransform(this.m_SkyPan, 0, this.m_SkyRepeats, 1, 0);
	}

	render(support : PSRenderSupport, gl : WebGLRenderingContext) : void
	{
		super.render(support, gl);
	}

	onEvent(type : string, event : PSEvent) : void
	{
		//console.log('Event received ', type, event);

	}

	m_PointsText : PSTextSprite;
	m_PointsCount : number = 0;
	
	m_Monster : Monster;
	m_Projectiles : ProjectileGenerator;
	m_Skyline : PSSprite;
	
	m_SkyRepeats : number = 1;
	m_SkyPan : number = 0;
};