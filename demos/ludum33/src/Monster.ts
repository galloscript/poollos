/// <reference path="../../../src/Poollos.ts"/>

class Monster extends PSEntity
{
	constructor()
	{
		super();
		this.m_MoveUpKeyCode = PSKeyMap.W;
		this.m_MoveDownKeyCode = PSKeyMap.S;
		this.m_MoveLeftKeyCode = PSKeyMap.A;
		this.m_MoveRightKeyCode = PSKeyMap.D;

		var sprite = new PSSprite(PSResourceManager.instance.getResource('./assets/monster.png'));
		//sprite.color.setValues(4.0, 4.0, 4.0, 1.0);
		//sprite.width = 64;
		//sprite.height = 64;
		this.addChild(sprite);
		this.m_Sprite = sprite;
		
		this.m_Speed.x = 0;
		this.m_Speed.y = 0;
		
		this.updateBounds(true);

		this.m_LimitTop = 50;
		this.m_LimitBottom = PSScreen.height - 50;
		this.m_LimitLeft = 50;
		this.m_LimitRight = PSScreen.width - 50;

		this.m_CaptureInput = true;
		this.addEventListener(PSTouchEvent.TOUCH_BEGIN, this);
		this.addEventListener(PSTouchEvent.TOUCH_END, this);
		this.transform.scale = 1;
		
		this.pivotX = this.m_Sprite.width * 0.5;
		this.pivotY = this.m_Sprite.height * 0.5;
		
		this.m_CircleCollider = new PSCircle(this.x, this.y, 60);
	}

	updateSpeed(clock : PSClock) : void
	{
		var verticalPressed = false;
		var horizontalPressed = false;
		
		if(PSInput.isKeyPressed(this.m_MoveUpKeyCode))
		{
			if(this.m_Speed.y > 0) this.m_Speed.y = 0;
			this.m_Speed.y += (this.m_Speed.y > -500) ? -3000 * clock.deltaTime : 0;
			verticalPressed = true;
		}
		if(PSInput.isKeyPressed(this.m_MoveDownKeyCode))
		{
			if(this.m_Speed.y < 0) this.m_Speed.y = 0;
			this.m_Speed.y += (this.m_Speed.y < 500) ? 3000 * clock.deltaTime : 0;
			verticalPressed = true;
		}
		
		if(!verticalPressed)
		{
			
			if(Math.abs(this.m_Speed.y) <= 10)
				this.m_Speed.y = 0;
				
			if(this.m_Speed.y < 0)
			{
				this.m_Speed.y += 3000 * clock.deltaTime;
				if(this.m_Speed.y > 0)
				{
					this.m_Speed.y  = 0;
				}
			}
			
			if(this.m_Speed.y > 0)
			{
				this.m_Speed.y -= 3000 * clock.deltaTime;
				if(this.m_Speed.y < 0)
				{
					this.m_Speed.y  = 0;
				}
			}	
		}
		
		if(PSInput.isKeyPressed(this.m_MoveLeftKeyCode))
		{
			if(this.m_Speed.x > 0) this.m_Speed.x = 0;
			this.m_Speed.x += (this.m_Speed.x > -500) ? -3000 * clock.deltaTime : 0;
			horizontalPressed = true;
		}
		if(PSInput.isKeyPressed(this.m_MoveRightKeyCode))
		{
			if(this.m_Speed.x < 0) this.m_Speed.x = 0;
			this.m_Speed.x += (this.m_Speed.x < 500) ? 3000 * clock.deltaTime : 0;
			horizontalPressed = true;
		}
		
		if(!horizontalPressed)
		{
			
			if(Math.abs(this.m_Speed.x) <= 10)
				this.m_Speed.x = 0;
				
			if(this.m_Speed.x < 0)
			{
				this.m_Speed.x += 3000 * clock.deltaTime;
				if(this.m_Speed.x > 0)
				{
					this.m_Speed.x  = 0;
				}
			}
			
			if(this.m_Speed.x > 0)
			{
				this.m_Speed.x -= 3000 * clock.deltaTime;
				if(this.m_Speed.x < 0)
				{
					this.m_Speed.x  = 0;
				}
			}
			
		}
	}
	
	updateLocation(clock : PSClock) : void
	{
		this.y += this.m_Speed.y * clock.deltaTime;
		this.x += this.m_Speed.x * clock.deltaTime;

		if(this.y < this.m_LimitTop)
			this.y = this.m_LimitTop;

		if(this.y > this.m_LimitBottom)
			this.y = this.m_LimitBottom;
			
		if(this.x < this.m_LimitLeft)
			this.x = this.m_LimitLeft;

		if(this.x > this.m_LimitRight)
			this.x = this.m_LimitRight;
			
		this.m_CircleCollider.updateLocation(this.x, this.y);
	}
	
	update(clock : PSClock) : void
	{
		this.updateSpeed(clock);
		this.updateLocation(clock);

	}

	onEvent(type : string, event : PSEvent) : void
	{

	}


	m_MoveUpKeyCode : number;
	m_MoveDownKeyCode : number;
	m_MoveLeftKeyCode : number;
	m_MoveRightKeyCode : number;
	m_Sprite : PSSprite;
	m_Speed : PSPoint = new PSPoint(0 ,0);
	m_LimitTop : number;
	m_LimitBottom : number;
	m_LimitLeft : number;
	m_LimitRight : number;
	m_CircleCollider : PSCircle;
}