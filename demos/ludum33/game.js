/** math/Point.ts */
var PSPoint = (function () {
    /** public */
    //constructor(xy : number);
    //constructor(x : number, y : number);
    //constructor(p : PSPoint);
    function PSPoint(a, b) {
        if (typeof a == 'number') {
            this.m_X = a;
            this.m_Y = (typeof b == 'number') ? b : a;
        }
        else if (a instanceof PSPoint) {
            this.m_X = a.m_X;
            this.m_Y = a.m_Y;
        }
        else {
            this.m_X = 0;
            this.m_Y = 0;
        }
    }
    Object.defineProperty(PSPoint.prototype, "x", {
        get: function () { return this.m_X; },
        set: function (x) { this.m_X = x; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSPoint.prototype, "y", {
        get: function () { return this.m_Y; },
        set: function (y) { this.m_Y = y; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSPoint.prototype, "length", {
        get: function () {
            return Math.sqrt((this.m_X * this.m_X) + (this.m_Y * this.m_Y));
        },
        enumerable: true,
        configurable: true
    });
    PSPoint.prototype.setValues = function (x, y) {
        this.m_X = x;
        this.m_Y = y;
        return this;
    };
    PSPoint.prototype.setFrom = function (other) {
        this.m_X = other.m_X;
        this.m_Y = other.m_Y;
        return this;
    };
    PSPoint.prototype.normalize = function () {
        var len = this.length;
        var scalar = (len > 0) ? 1.0 / len : 0.0;
        return PSPoint.multiply(this, scalar);
    };
    PSPoint.prototype.multiply = function (p2) {
        if (typeof p2 == 'number') {
            this.m_X *= p2;
            this.m_Y *= p2;
        }
        else if (typeof p2 == 'object') {
            this.m_X *= p2.m_X;
            this.m_Y *= p2.m_Y;
        }
        return this;
    };
    PSPoint.prototype.add = function (p2) {
        this.m_X += p2.m_X;
        this.m_Y += p2.m_Y;
        return this;
    };
    PSPoint.prototype.subtract = function (p2) {
        this.m_X -= p2.m_X;
        this.m_Y -= p2.m_Y;
        return this;
    };
    PSPoint.prototype.resetValues = function () {
        this.m_X = 0;
        this.m_Y = 0;
    };
    PSPoint.prototype.distanceTo = function (other) {
        return PSPoint.subtract(this, other).length;
    };
    Object.defineProperty(PSPoint, "X_AXIS", {
        /** static */
        get: function () { return new PSPoint(1, 0); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSPoint, "Y_AXIS", {
        get: function () { return new PSPoint(0, 1); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSPoint, "ZERO", {
        get: function () { return new PSPoint(0, 0); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSPoint, "ONE", {
        get: function () { return new PSPoint(1, 1); },
        enumerable: true,
        configurable: true
    });
    PSPoint.add = function (p1, p2) {
        return new PSPoint(p1.m_X + p2.m_X, p1.m_Y + p2.m_Y);
    };
    PSPoint.subtract = function (p1, p2) {
        return new PSPoint(p1.m_X - p2.m_X, p1.m_Y - p2.m_Y);
    };
    PSPoint.multiply = function (p1, p2) {
        if (typeof p2 == 'number') {
            return new PSPoint(p1.m_X * p2, p1.m_Y * p2);
        }
        else
            (typeof p2 == 'object');
        {
            return new PSPoint(p1.m_X * p2.m_X, p1.m_Y * p2.m_Y);
        }
    };
    PSPoint.dot = function (p1, p2) {
        return (p1.m_X * p2.m_X) + (p1.m_Y * p2.m_Y);
    };
    PSPoint.angle = function (p1, p2) {
        var dotProd = PSPoint.dot(p1, p2);
        var lenProd = p1.length * p2.length;
        var divOp = dotProd / lenProd;
        return Math.acos(divOp);
    };
    PSPoint.distanceBetween = function (p1, p2) {
        return PSPoint.subtract(p1, p2).length;
    };
    PSPoint.POOL = [];
    return PSPoint;
})();
;
//Memory utilities for Typescript and Javascript
var PSPool = (function () {
    function PSPool(classType, initialSize) {
        this.m_FreeElements = [];
        this.m_UsedElements = [];
        this.m_ClassType = classType;
        this.m_InitialSize = initialSize;
        this.fill();
    }
    PSPool.createPool = function (classType, initialSize) {
        if (initialSize === void 0) { initialSize = 100; }
        var className = classType.name;
        if (className != 'Object' && PSPool.s_PoolMap[className] == null) {
            PSPool.s_PoolMap[className] = new PSPool(classType, initialSize);
        }
    };
    PSPool.alloc = function (classType) {
        var className = classType.name;
        return PSPool.s_PoolMap[className].getAvailableElement();
    };
    PSPool.free = function (obj) {
        var className = obj['__proto__'].constructor.name;
        PSPool.s_PoolMap[className].freeElement(obj);
    };
    PSPool.poolFor = function (classType) {
        if (PSPool.s_PoolMap[classType.name] == null) {
            throw 'PSPool for ' + classType.name + ' uninitialized. Call PSPool.createPool(' + classType.name + ');';
        }
        return PSPool.s_PoolMap[classType.name];
    };
    PSPool.prototype.fill = function () {
        for (var i = 0; i < this.m_InitialSize; i++) {
            this.m_FreeElements.push(new this.m_ClassType);
        }
    };
    PSPool.prototype.getAvailableElement = function () {
        if (this.m_FreeElements.length == 0) {
            this.fill();
        }
        var element = this.m_FreeElements.pop();
        this.m_UsedElements.push(element);
        element.resetValues();
        return element;
    };
    PSPool.prototype.freeElement = function (element) {
        var index = this.m_UsedElements.indexOf(element);
        if (index != -1) {
            this.m_UsedElements.splice(index, 1);
            this.m_FreeElements.push(element);
        }
    };
    PSPool.s_PoolMap = {};
    return PSPool;
})();
/// <reference path="./Point.ts"/>
/// <reference path="../system/Memory.ts"/>
/** math/Matrix.ts */
var PSMatrix = (function () {
    /** public */
    function PSMatrix(m) {
        if (m instanceof PSMatrix) {
            this.m_M = new Float32Array(9);
            this.setValues(m.m_M);
        }
        else {
            this.m_M = new Float32Array(9);
            this.identity();
        }
    }
    PSMatrix.prototype.setFrom = function (other) {
        return this.setValues(other.m_M);
    };
    PSMatrix.prototype.setValues = function (otherArray) {
        this.m_M[0] = otherArray[0]; /** a */
        this.m_M[1] = otherArray[1]; /** b */
        this.m_M[2] = otherArray[2];
        this.m_M[3] = otherArray[3]; /** c */
        this.m_M[4] = otherArray[4]; /** d */
        this.m_M[5] = otherArray[5];
        this.m_M[6] = otherArray[6]; /** tx */
        this.m_M[7] = otherArray[7]; /** ty */
        this.m_M[8] = otherArray[8];
        return this;
    };
    /// Resets this matrix to an identity matrix
    PSMatrix.prototype.identity = function () {
        this.m_M[0] = 1.0; /** a */
        this.m_M[1] = 0.0; /** b */
        this.m_M[2] = 0.0;
        this.m_M[3] = 0.0; /** c */
        this.m_M[4] = 1.0; /** d */
        this.m_M[5] = 0.0;
        this.m_M[6] = 0.0; /** tx */
        this.m_M[7] = 0.0; /** ty */
        this.m_M[8] = 1.0;
        return this;
    };
    Object.defineProperty(PSMatrix.prototype, "determinant", {
        /// Returns the determinant
        get: function () {
            return (this.m_M[0] * this.m_M[4]) - (this.m_M[3] * this.m_M[1]);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSMatrix.prototype, "arrayBuffer", {
        /// Returns a reference to the internal Float32Array 
        get: function () {
            return this.m_M;
        },
        enumerable: true,
        configurable: true
    });
    /// multply this matrix with another and stores the result itslef
    PSMatrix.prototype.concat = function (otherMatrix) {
        var newThis = PSPool.alloc(PSMatrix);
        newThis.m_M[0] = otherMatrix.m_M[0] * this.m_M[0] + (otherMatrix.m_M[3] * this.m_M[1]);
        newThis.m_M[1] = otherMatrix.m_M[1] * this.m_M[0] + (otherMatrix.m_M[4] * this.m_M[1]);
        newThis.m_M[3] = otherMatrix.m_M[0] * this.m_M[3] + (otherMatrix.m_M[3] * this.m_M[4]);
        newThis.m_M[4] = otherMatrix.m_M[1] * this.m_M[3] + (otherMatrix.m_M[4] * this.m_M[4]);
        newThis.m_M[6] = otherMatrix.m_M[0] * this.m_M[6] + (otherMatrix.m_M[3] * this.m_M[7]) + otherMatrix.m_M[6] * 1;
        newThis.m_M[7] = otherMatrix.m_M[1] * this.m_M[6] + (otherMatrix.m_M[4] * this.m_M[7]) + otherMatrix.m_M[7] * 1;
        this.setValues(newThis.m_M);
        PSPool.free(newThis);
        return this;
    };
    /// Move the matrix in x and y
    PSMatrix.prototype.translate = function (x, y) {
        this.m_M[6] += x;
        this.m_M[7] += y;
        return this;
    };
    /// Rotate the matrix in Z
    PSMatrix.prototype.rotate = function (angle) {
        var rotMatrix = PSPool.alloc(PSMatrix);
        rotMatrix.m_M[0] = Math.cos(angle);
        rotMatrix.m_M[1] = -Math.sin(angle);
        rotMatrix.m_M[3] = Math.sin(angle);
        rotMatrix.m_M[4] = Math.cos(angle);
        rotMatrix.m_M[6] = 0.0;
        rotMatrix.m_M[7] = 0.0;
        var result = this.concat(rotMatrix);
        PSPool.free(rotMatrix);
        return result;
    };
    /// Scale the matrix in x and y
    PSMatrix.prototype.scale = function (sx, sy) {
        this.m_M[0] *= sx;
        this.m_M[1] *= sy;
        this.m_M[3] *= sx;
        this.m_M[4] *= sy;
        this.m_M[6] *= sx;
        this.m_M[7] *= sy;
        return this;
    };
    /// Transfomr a given point to the matrix space and return a new one
    PSMatrix.prototype.transformPoint = function (point, targetPoint) {
        targetPoint = targetPoint || new PSPoint(0.0, 0.0);
        var orgX = point.x;
        targetPoint.x = (this.m_M[0] * point.x) + (this.m_M[3] * point.y) + this.m_M[6];
        targetPoint.y = (this.m_M[1] * orgX) + (this.m_M[4] * point.y) + this.m_M[7];
        return targetPoint;
    };
    /// Inverts this matrix
    PSMatrix.prototype.invert = function () {
        var det = this.determinant;
        var newThis = PSPool.alloc(PSMatrix);
        newThis.m_M[0] = this.m_M[4] / det;
        newThis.m_M[1] = -this.m_M[1] / det;
        newThis.m_M[3] = -this.m_M[3] / det;
        newThis.m_M[4] = this.m_M[0] / det;
        newThis.m_M[6] = ((this.m_M[3] * this.m_M[7]) - (this.m_M[4] * this.m_M[6])) / det;
        newThis.m_M[7] = ((this.m_M[1] * this.m_M[6]) - (this.m_M[0] * this.m_M[7])) / det;
        this.setValues(newThis.m_M);
        PSPool.free(newThis);
        return this;
    };
    /// Returns an inverse of this matrix as a new matrix
    PSMatrix.prototype.inverse = function () {
        return new PSMatrix(this).invert();
    };
    PSMatrix.prototype.resetValues = function () {
        this.identity();
    };
    PSMatrix.make2DProjection = function (width, height, target) {
        var mat = target || new PSMatrix();
        mat.identity();
        mat.m_M[0] = 2 / width;
        mat.m_M[4] = -2 / height;
        mat.m_M[6] = -1;
        mat.m_M[7] = 1;
        mat.m_M[8] = 1;
        return mat;
    };
    return PSMatrix;
})();
/// <reference path="./Matrix.ts"/>
/** math/Transform.ts */
var PSTransform = (function () {
    function PSTransform() {
        this.m_Pivot = new PSPoint(0, 0);
        this.m_Location = new PSPoint(0, 0);
        this.m_Scale = new PSPoint(1, 1);
        this.m_Rotation = 0.0;
        this.m_Width = 0.0;
        this.m_Height = 0.0;
    }
    Object.defineProperty(PSTransform.prototype, "pivotX", {
        get: function () { return this.m_Pivot.x; },
        set: function (v) { this.m_Pivot.x = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTransform.prototype, "pivotY", {
        get: function () { return this.m_Pivot.y; },
        set: function (v) { this.m_Pivot.y = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTransform.prototype, "x", {
        get: function () { return this.m_Location.x; },
        set: function (v) { this.m_Location.x = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTransform.prototype, "y", {
        get: function () { return this.m_Location.y; },
        set: function (v) { this.m_Location.y = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTransform.prototype, "scaleX", {
        get: function () { return this.m_Scale.x; },
        set: function (v) { this.m_Scale.x = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTransform.prototype, "scaleY", {
        get: function () { return this.m_Scale.y; },
        set: function (v) { this.m_Scale.y = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTransform.prototype, "rotation", {
        get: function () { return this.m_Rotation; },
        set: function (angle) { this.m_Rotation = angle; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTransform.prototype, "width", {
        get: function () { return this.m_Width; },
        set: function (w) { this.m_Width = w; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTransform.prototype, "height", {
        get: function () { return this.m_Height; },
        set: function (h) { this.m_Height = h; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTransform.prototype, "pivot", {
        get: function () { return this.m_Pivot; },
        set: function (p) {
            this.m_Pivot.x = p.x;
            this.m_Pivot.y = p.y;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTransform.prototype, "location", {
        get: function () { return this.m_Location; },
        set: function (p) {
            this.m_Location.x = p.x;
            this.m_Location.y = p.y;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTransform.prototype, "scale", {
        get: function () { return this.m_Scale; },
        set: function (p) {
            if (p instanceof PSPoint) {
                this.m_Scale.x = p.x;
                this.m_Scale.y = p.y;
            }
            else if (typeof p == 'number') {
                this.m_Scale.x = this.m_Scale.y = p;
            }
        },
        enumerable: true,
        configurable: true
    });
    PSTransform.prototype.getTransformationMatrix = function (targetMatrix, includeWH) {
        if (includeWH === void 0) { includeWH = false; }
        targetMatrix = targetMatrix || new PSMatrix();
        targetMatrix.identity();
        var tempScaleX = this.m_Scale.x;
        var tempScaleY = this.m_Scale.y;
        if (includeWH && this.m_Width != 0.0 && this.m_Height != 0.0) {
            tempScaleX = this.m_Scale.x * this.m_Width;
            tempScaleY = this.m_Scale.y * this.m_Height;
        }
        if (this.m_Pivot.x != 0.0 || this.m_Pivot.y != 0.0) {
            targetMatrix.translate(-this.m_Pivot.x, -this.m_Pivot.y);
        }
        if (tempScaleX != 1.0 || tempScaleY != 1.0) {
            targetMatrix.scale(tempScaleX, tempScaleY);
        }
        if (this.m_Rotation != 0.0) {
            targetMatrix.rotate(this.m_Rotation);
        }
        if (this.m_Location.x != 0.0 || this.m_Location.y != 0.0) {
            targetMatrix.translate(this.m_Location.x, this.m_Location.y);
        }
        return targetMatrix;
    };
    return PSTransform;
})();
;
/// <reference path="../math/Transform.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var PSCamera = (function (_super) {
    __extends(PSCamera, _super);
    function PSCamera() {
        _super.apply(this, arguments);
    }
    return PSCamera;
})(PSTransform);
;
/** display/Event.ts */
var PSEvent = (function () {
    function PSEvent(type) {
        this.m_Captured = false;
        this.m_Target = null;
        this.m_Type = type;
    }
    Object.defineProperty(PSEvent.prototype, "type", {
        get: function () { return this.m_Type; },
        set: function (t) { this.m_Type = t; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEvent.prototype, "captured", {
        get: function () { return this.m_Captured; },
        set: function (c) { this.m_Captured = c; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEvent.prototype, "target", {
        get: function () { return this.m_Target; },
        set: function (e) { this.m_Target = e; },
        enumerable: true,
        configurable: true
    });
    PSEvent.prototype.toString = function () { return 'PSEvent("' + this.m_Type + '")'; };
    PSEvent.prototype.resetValues = function (type) {
        this.m_Type = type;
        this.m_Captured = false;
        this.m_Target = null;
    };
    return PSEvent;
})();
;
/// <reference path="./Event.ts"/>
/// <reference path="../math/Transform.ts"/>
/** display/EventListener.ts */
var PSEventListener = (function () {
    function PSEventListener() {
    }
    PSEventListener.prototype.onEvent = function (type, event) {
    };
    return PSEventListener;
})();
;
/** stdlib.ts */
;
/*
class PSHashMap<T>
{
    
    get length() : number{ return m_Elements.length };
    set(key : string, value : T) : void
    {

    }
    private m_Elements : { [index: string]: T };
}
*/
/// <reference path="./EventListener.ts"/>
/// <reference path="../shared/stdlib.ts"/>
/** display/EventDispatcher.ts */
var PSEventDispatcher = (function (_super) {
    __extends(PSEventDispatcher, _super);
    function PSEventDispatcher() {
        _super.call(this);
        this.m_Listeners = {};
    }
    PSEventDispatcher.prototype.addEventListener = function (type, listener) {
        if (this.m_Listeners[type] == null) {
            var newVec = [];
            newVec.push(listener);
            this.m_Listeners[type] = newVec;
        }
        else {
            if (this.m_Listeners[type].indexOf(listener) != -1) {
                this.m_Listeners[type].push(listener);
            }
        }
    };
    PSEventDispatcher.prototype.dispatchEvent = function (event) {
        var elisteners = this.m_Listeners[event.type];
        if (elisteners != null) {
            var size = elisteners.length;
            for (var i = 0; i < size; ++i) {
                elisteners[i].onEvent(event.type, event);
            }
        }
    };
    PSEventDispatcher.prototype.trigger = function (type) {
        var elisteners = this.m_Listeners[type];
        if (elisteners != null) {
            var size = elisteners.length;
            for (var i = 0; i < size; ++i) {
                elisteners[i].onEvent(type, new PSEvent(type));
            }
        }
    };
    PSEventDispatcher.prototype.hasEventListener = function (type) {
        return (this.m_Listeners[type] != null);
    };
    PSEventDispatcher.prototype.removeEventListener = function (type, listener) {
        if (this.hasEventListener(type) != null) {
            var indexOfListener = this.m_Listeners[type].indexOf(listener);
            if (indexOfListener != -1) {
                this.m_Listeners[type].splice(indexOfListener, 1);
            }
        }
    };
    PSEventDispatcher.prototype.clearEventListeners = function () {
        this.m_Listeners = {};
    };
    return PSEventDispatcher;
})(PSEventListener);
;
var PSColor = (function () {
    /** public */
    //constructor(c : PSColor);
    //constructor(r : number, g : number, b : number, a : number);
    function PSColor(r, g, b, a) {
        if (r === void 0) { r = 1.0; }
        if (g === void 0) { g = 1.0; }
        if (b === void 0) { b = 1.0; }
        if (a === void 0) { a = 1.0; }
        if (r instanceof PSColor) {
            this.m_ColorVector = new Float32Array(r.m_ColorVector);
        }
        else {
            this.m_ColorVector = new Float32Array(4);
            if (typeof r == 'number') {
                this.m_ColorVector[0] = r;
                this.m_ColorVector[1] = g;
                this.m_ColorVector[2] = b;
                this.m_ColorVector[3] = a;
            }
        }
    }
    Object.defineProperty(PSColor, "WHITE", {
        get: function () { return new PSColor(1.0, 1.0, 1.0, 1.0); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSColor.prototype, "red", {
        get: function () { return this.m_ColorVector[0]; },
        set: function (v) { this.m_ColorVector[0] = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSColor.prototype, "green", {
        get: function () { return this.m_ColorVector[1]; },
        set: function (v) { this.m_ColorVector[1] = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSColor.prototype, "blue", {
        get: function () { return this.m_ColorVector[2]; },
        set: function (v) { this.m_ColorVector[2] = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSColor.prototype, "alpha", {
        get: function () { return this.m_ColorVector[3]; },
        set: function (v) { this.m_ColorVector[3] = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSColor.prototype, "arrayBuffer", {
        /// Returns a reference to the internal Float32Array 
        get: function () {
            return this.m_ColorVector;
        },
        enumerable: true,
        configurable: true
    });
    PSColor.prototype.setFrom = function (other) {
        this.m_ColorVector[0] = other.m_ColorVector[0];
        this.m_ColorVector[1] = other.m_ColorVector[1];
        this.m_ColorVector[2] = other.m_ColorVector[2];
        this.m_ColorVector[3] = other.m_ColorVector[3];
        return this;
    };
    PSColor.prototype.resetValues = function () {
        this.m_ColorVector[0] = 1.0;
        this.m_ColorVector[1] = 1.0;
        this.m_ColorVector[2] = 1.0;
        this.m_ColorVector[3] = 1.0;
    };
    PSColor.prototype.setValues = function (r, g, b, a) {
        if (a === void 0) { a = 1.0; }
        this.m_ColorVector[0] = r;
        this.m_ColorVector[1] = g;
        this.m_ColorVector[2] = b;
        this.m_ColorVector[3] = a;
        return this;
    };
    PSColor.prototype.multiply = function (other) {
        this.m_ColorVector[0] *= other.m_ColorVector[0];
        this.m_ColorVector[1] *= other.m_ColorVector[1];
        this.m_ColorVector[2] *= other.m_ColorVector[2];
        this.m_ColorVector[3] *= other.m_ColorVector[3];
        return this;
    };
    PSColor.prototype.divide = function (other) {
        this.m_ColorVector[0] /= other.m_ColorVector[0];
        this.m_ColorVector[1] /= other.m_ColorVector[1];
        this.m_ColorVector[2] /= other.m_ColorVector[2];
        this.m_ColorVector[3] /= other.m_ColorVector[3];
        return this;
    };
    PSColor.prototype.getRGBHexString = function () {
        var r = Math.floor(this.m_ColorVector[0] * 255).toString(16);
        r = (r.length == 1) ? '0' + r : r;
        var g = Math.floor(this.m_ColorVector[1] * 255).toString(16);
        g = (g.length == 1) ? '0' + g : g;
        var b = Math.floor(this.m_ColorVector[2] * 255).toString(16);
        b = (b.length == 1) ? '0' + b : b;
        return '#' + r + g + b;
    };
    PSColor.multiply = function (other, result) {
        result = result || new PSColor(this);
        return result.multiply(other);
    };
    PSColor.divide = function (other, result) {
        result = result || new PSColor(this);
        return result.divide(other);
    };
    return PSColor;
})();
;
var PSRectangle = (function () {
    function PSRectangle(x, y, w, h) {
        if (x === void 0) { x = 0.0; }
        if (y === void 0) { y = 0.0; }
        if (w === void 0) { w = 0.0; }
        if (h === void 0) { h = 0.0; }
        this.m_X = x;
        this.m_Y = y;
        this.m_Width = w;
        this.m_Height = h;
    }
    Object.defineProperty(PSRectangle.prototype, "x", {
        get: function () { return this.m_X; },
        set: function (v) { this.m_X = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSRectangle.prototype, "y", {
        get: function () { return this.m_Y; },
        set: function (v) { this.m_Y = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSRectangle.prototype, "width", {
        get: function () { return this.m_Width; },
        set: function (v) { this.m_Width = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSRectangle.prototype, "height", {
        get: function () { return this.m_Height; },
        set: function (v) { this.m_Height = v; },
        enumerable: true,
        configurable: true
    });
    PSRectangle.prototype.clone = function () {
        return new PSRectangle(this.m_X, this.m_Y, this.m_Width, this.m_Height);
    };
    PSRectangle.prototype.rectUnion = function (other) {
        if (other.m_Width == 0 || other.m_Height == 0) {
            return this.clone();
        }
        if (this.m_Width == 0 || this.m_Height == 0) {
            return other.clone();
        }
        var left = Math.min(this.m_X, other.m_X);
        var top = Math.min(this.m_Y, other.m_X);
        var right = Math.max(this.m_X + this.m_Width, other.m_X + other.m_Width);
        var bottom = Math.max(this.m_X + this.m_Height, other.m_X + other.m_Height);
        return new PSRectangle(left, top, right - left, bottom - top);
    };
    PSRectangle.prototype.unite = function (other) {
        if (other.m_Width == 0 || other.m_Height == 0)
            return;
        if (this.m_Width == 0 || this.m_Height == 0) {
            this.m_X = other.m_X;
            this.m_Y = other.m_Y;
            this.m_Width = other.m_Width;
            this.m_Height = other.m_Height;
        }
        var left = Math.min(this.m_X, other.m_X);
        var top = Math.min(this.m_Y, other.m_Y);
        var right = Math.max(this.m_X + this.m_Width, other.m_X + other.m_Width);
        var bottom = Math.max(this.m_Y + this.m_Height, other.m_Y + other.m_Height);
        this.m_X = left;
        this.m_Y = top;
        this.m_Width = right - left;
        this.m_Height = bottom - top;
    };
    PSRectangle.prototype.containsPoint = function (point) {
        var x = point.x;
        var y = point.y;
        return (x >= this.m_X &&
            y >= this.m_Y &&
            x <= this.m_X + this.m_Width &&
            y <= this.m_Y + this.m_Height);
    };
    PSRectangle.prototype.intersectsWidth = function (toIntersect) {
        return !(this.m_X + this.m_Width < toIntersect.m_X ||
            this.m_X > toIntersect.m_X + toIntersect.m_Width ||
            this.m_Y + this.m_Height < toIntersect.m_Y ||
            this.m_Y > toIntersect.m_Y + toIntersect.m_Height);
    };
    PSRectangle.prototype.intersectionResult = function (toIntersect) {
        if (!this.intersectsWidth(toIntersect)) {
            return new PSRectangle(0, 0, 0, 0);
        }
        //they do intersect, so get the intersection
        var xMin, xMax, yMin, yMax;
        xMin = Math.max(this.m_X, toIntersect.m_X);
        xMax = Math.min(this.m_X + this.m_Width, toIntersect.m_X + toIntersect.m_Width);
        yMin = Math.max(this.m_Y, toIntersect.m_Y);
        yMax = Math.min(this.m_Y + this.m_Height, toIntersect.m_Y + toIntersect.m_Height);
        return new PSRectangle(xMin, yMin, xMax - xMin, yMax - yMin);
    };
    PSRectangle.prototype.toString = function () {
        return '(x =' + this.m_X + ', y =' + this.m_Y + ', width =' + this.m_Width + ', height=' + this.m_Height + ')';
    };
    return PSRectangle;
})();
;
/// <reference path="./Event.ts"/>
/// <reference path="../math/Point.ts"/>
/** display/TouchEvent.ts */
var PSTouchEvent = (function (_super) {
    __extends(PSTouchEvent, _super);
    /** public */
    function PSTouchEvent(type) {
        _super.call(this, type);
        this.m_LocalX = 0;
        this.m_LocalY = 0;
        this.m_StageX = 0;
        this.m_StageY = 0;
        this.m_DisplacementX = 0;
        this.m_DisplacementY = 0;
        this.m_Duration = 0;
        this.m_Velocity = 0;
        this.m_Factor = 0;
    }
    Object.defineProperty(PSTouchEvent, "TOUCH_BEGIN", {
        /** static */
        get: function () { return 'TouchBegin'; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent, "TOUCH_END", {
        get: function () { return 'TouchEnd'; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent, "TOUCH_MOVE", {
        get: function () { return 'TouchMove'; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent, "TOUCH_OUT", {
        get: function () { return 'TouchOut'; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent, "TOUCH_OVER", {
        get: function () { return 'TouchOver'; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent, "TOUCH_TAP", {
        get: function () { return 'TouchTap'; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent, "TOUCH_PINCH", {
        get: function () { return 'TouchPinch'; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent.prototype, "localX", {
        get: function () { return this.m_LocalX; },
        set: function (v) { this.m_LocalX = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent.prototype, "localY", {
        get: function () { return this.m_LocalY; },
        set: function (v) { this.m_LocalY = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent.prototype, "stageX", {
        get: function () { return this.m_StageX; },
        set: function (v) { this.m_StageX = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent.prototype, "stageY", {
        get: function () { return this.m_StageY; },
        set: function (v) { this.m_StageY = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent.prototype, "displacementX", {
        get: function () { return this.m_DisplacementX; },
        set: function (v) { this.m_DisplacementX = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent.prototype, "displacementY", {
        get: function () { return this.m_DisplacementY; },
        set: function (v) { this.m_DisplacementY = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent.prototype, "duration", {
        get: function () { return this.m_Duration; },
        set: function (v) { this.m_Duration = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent.prototype, "velocity", {
        get: function () { return this.m_Velocity; },
        set: function (v) { this.m_Velocity = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTouchEvent.prototype, "factor", {
        get: function () { return this.m_Factor; },
        set: function (v) { this.m_Factor = v; },
        enumerable: true,
        configurable: true
    });
    PSTouchEvent.prototype.toString = function () {
        return '(type = ' + this.type + ', lx = ' + this.m_LocalX + ', ly = ' + this.m_LocalY + ', sx = ' + this.m_StageX + ', sy = ' + this.m_StageY + ')';
    };
    PSTouchEvent.prototype.resetValues = function (type) {
        _super.prototype.resetValues.call(this, type);
    };
    return PSTouchEvent;
})(PSEvent);
;
/// <reference path="./EventDispatcher.ts"/>
/// <reference path="../math/Transform.ts"/>
/// <reference path="../math/Matrix.ts"/>
/// <reference path="../math/Color.ts"/>
/// <reference path="../math/Rectangle.ts"/>
/// <reference path="../display/TouchEvent.ts"/>
/** display/Entity.ts */
var PSEntity = (function (_super) {
    __extends(PSEntity, _super);
    function PSEntity() {
        _super.call(this);
        /** Inner deferred object destruction */
        this.m_EntitiesToDestroy = [];
        this.m_DirtyList = false;
        /** Touch events handling */
        this.m_MouseDown = false;
        this.m_MouseDownPoint = new PSPoint();
        this.m_CaptureInput = false;
        this.m_IncludeWidthHeight = false;
        this.m_Transform = new PSTransform();
        this.m_Parent = null;
        this.m_Children = [];
        this.m_TimeScale = 1;
        this.m_Name = 'Unnamed';
        this.m_Visible = true;
        this.m_WorldMatrix = new PSMatrix();
        this.m_Color = new PSColor();
        this.m_Bounds = new PSRectangle();
        this.m_ClipRectangle = null;
    }
    Object.defineProperty(PSEntity.prototype, "transform", {
        /** Public properties */
        get: function () { return this.m_Transform; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "pivotX", {
        get: function () { return this.m_Transform.m_Pivot.x; },
        set: function (v) { this.m_Transform.m_Pivot.x = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "pivotY", {
        get: function () { return this.m_Transform.m_Pivot.y; },
        set: function (v) { this.m_Transform.m_Pivot.y = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "x", {
        get: function () { return this.m_Transform.m_Location.x; },
        set: function (v) { this.m_Transform.m_Location.x = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "y", {
        get: function () { return this.m_Transform.m_Location.y; },
        set: function (v) { this.m_Transform.m_Location.y = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "scaleX", {
        get: function () { return this.m_Transform.m_Scale.x; },
        set: function (v) { this.m_Transform.m_Scale.x = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "scaleY", {
        get: function () { return this.m_Transform.m_Scale.y; },
        set: function (v) { this.m_Transform.m_Scale.y = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "rotation", {
        get: function () { return this.m_Transform.m_Rotation; },
        set: function (angle) { this.m_Transform.m_Rotation = angle; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "width", {
        get: function () { return this.m_Transform.m_Width; },
        set: function (w) { this.m_Transform.m_Width = w; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "height", {
        get: function () { return this.m_Transform.m_Height; },
        set: function (h) { this.m_Transform.m_Height = h; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "name", {
        get: function () { return this.m_Name; },
        set: function (newName) { this.m_Name = newName; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "numChildren", {
        get: function () { return this.m_Children.length; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "timeScale", {
        get: function () { return this.m_TimeScale; },
        set: function (ts) { this.m_TimeScale = ts; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "visible", {
        get: function () { return this.m_Visible; },
        set: function (v) { this.m_Visible = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "captureInput", {
        get: function () { return this.m_CaptureInput; },
        set: function (v) { this.m_CaptureInput = v; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "color", {
        get: function () { return this.m_Color; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "parent", {
        get: function () { return this.m_Parent; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSEntity.prototype, "alpha", {
        get: function () { return this.m_Color.alpha; },
        set: function (a) { this.m_Color.alpha = a; },
        enumerable: true,
        configurable: true
    });
    /** Returns a copy of the world matrix */
    PSEntity.prototype.getWorldMatrix = function () { return new PSMatrix(this.m_WorldMatrix); };
    /** Override this method to change it's behaviour */
    /** virtual */ PSEntity.prototype.update = function (clock) { };
    /** Override this method to change the way is rendered */
    /** virtual */ PSEntity.prototype.render = function (support, gl) { };
    /** called when the object is going to be destroyed */
    /** virtual */ PSEntity.prototype.onDestroy = function () { };
    /** Add a child object */
    PSEntity.prototype.addChild = function (child) {
        this.addChildAt(child, this.m_Children.length);
    };
    /** Add the child at the specified position */
    PSEntity.prototype.addChildAt = function (child, index) {
        if (child == null) {
            return;
        }
        if (child.m_Parent == this) {
            return;
        }
        if (child.m_Parent != null) {
            child.m_Parent.removeChild(child);
        }
        child.m_Parent = this;
        index = Math.min(index, this.m_Children.length);
        this.m_Children.splice(index, 0, child);
        this.updateBounds(true);
    };
    /** Remove and returns the child reseting it's parent to null */
    PSEntity.prototype.removeChild = function (child) {
        if (child == null)
            return;
        var index = this.m_Children.indexOf(child);
        if (index != -1) {
            this.m_Children.splice(index, 1);
            child.m_Parent = null;
        }
        return child;
    };
    /** Returns the first child found with the specified name */
    PSEntity.prototype.getChildByName = function (childName, recursive) {
        if (recursive === void 0) { recursive = true; }
        for (var childIndex in this.m_Children) {
            var child = this.m_Children[childIndex];
            if (child != null && child.m_Name == childName)
                return child;
        }
        if (recursive) {
            for (var childIndex in this.m_Children) {
                var child = this.m_Children[childIndex];
                if (child != null) {
                    var entity = child.getChildByName(childName, recursive);
                    if (entity != null)
                        return entity;
                }
            }
        }
        return null;
    };
    /** Returns the child in the specified position or null if the  */
    PSEntity.prototype.getChildAt = function (index) {
        if (index >= this.m_Children.length)
            return null;
        return this.m_Children[index];
    };
    /** Returns the index of the specified child */
    PSEntity.prototype.getChildIndex = function (child) {
        return this.m_Children.indexOf(child);
    };
    /** Sets the specified index to the child */
    PSEntity.prototype.setChildIndex = function (child, index) {
        this.removeChild(child);
        this.addChildAt(child, index);
    };
    /** Remove this object from it's parent and destroys it */
    PSEntity.prototype.destroy = function () {
        if (this.m_Parent != null) {
            this.m_Parent.destroyChild(this);
        }
        this.onDestroy();
    };
    /** Remove and destroy all object's children */
    PSEntity.prototype.destroyAllChilds = function () {
        for (var childIndex in this.m_Children) {
            var child = this.m_Children[childIndex];
            child.m_Parent = null;
            this.addEntityToDestroy(child);
            child.onDestroy();
        }
        this.m_Children = [];
    };
    /** Updates the world matrix */
    PSEntity.prototype.forceMatrixUpdate = function () {
        this.m_Transform.getTransformationMatrix(this.m_WorldMatrix, false);
        if (this.m_Parent != null) {
            this.m_WorldMatrix.concat(this.m_Parent.m_WorldMatrix);
        }
    };
    /** Internally updates the object (do not manually call this method) */
    PSEntity.prototype.innerUpdate = function (time) {
        this.forceMatrixUpdate();
        var prevDeltaTime = time.timeScale;
        time.timeScale = this.m_TimeScale;
        this.update(time);
        for (var childIndex in this.m_Children) {
            var child = this.m_Children[childIndex];
            if (child != null)
                child.innerUpdate(time);
        }
        time.timeScale = prevDeltaTime;
    };
    /** Internally render the object (do not manually call this method) */
    PSEntity.prototype.innerRender = function (support, gl) {
        if (!this.m_Visible)
            return;
        //entity renders itself using with and height
        support.pushState(this.m_Transform.getTransformationMatrix(null, true), this.m_Color);
        this.render(support, gl);
        support.popState();
        support.pushState(this.m_Transform.getTransformationMatrix(null, false), this.m_Color);
        for (var childIndex in this.m_Children) {
            var child = this.m_Children[childIndex];
            if (child != null)
                child.innerRender(support, gl);
        }
        support.popState();
    };
    /** Add a child entity to destroy */
    PSEntity.prototype.addEntityToDestroy = function (obj) {
        if (obj != null)
            this.m_EntitiesToDestroy.push(obj);
    };
    /** Delete objects to be destroyed */
    PSEntity.prototype.deletePendentObjects = function () {
        while (this.m_EntitiesToDestroy.length > 0) {
            var child = this.m_EntitiesToDestroy.splice(0, 1)[0];
            child.deletePendentObjects();
        }
    };
    /** Update the bounds rectangle with the object and childs boundaries */
    PSEntity.prototype.updateBounds = function (updateChilds) {
        // v1 --- v2
        // |      |
        // v3 --- v4
        if (updateChilds === void 0) { updateChilds = false; }
        var pivot = PSPool.alloc(PSPoint).setFrom(this.m_Transform.pivot);
        pivot.x *= this.m_Transform.scale.x;
        pivot.y *= this.m_Transform.scale.y;
        var width = this.m_Transform.width * this.m_Transform.scale.x;
        var height = this.m_Transform.height * this.m_Transform.scale.y;
        var v1 = PSPool.alloc(PSPoint).setValues(-pivot.x * width, -pivot.y * height);
        var v2 = PSPool.alloc(PSPoint).setValues(width - (pivot.x * width), -pivot.y * height);
        var v3 = PSPool.alloc(PSPoint).setValues(-pivot.x * width, height - (pivot.y * height));
        var v4 = PSPool.alloc(PSPoint).setValues(width - (pivot.x * width), height - (pivot.y * height));
        v1 = this.m_WorldMatrix.transformPoint(v1, v1);
        v2 = this.m_WorldMatrix.transformPoint(v2, v2);
        v3 = this.m_WorldMatrix.transformPoint(v3, v3);
        v4 = this.m_WorldMatrix.transformPoint(v4, v4);
        var maxX, maxY, minX, minY;
        maxX = v1.x > v2.x ? v1.x : v2.x;
        maxX = v3.x > v4.x ? (v3.x > maxX ? v3.x : maxX) : (v4.x > maxX ? v4.x : maxX);
        maxY = v1.y > v2.y ? v1.y : v2.y;
        maxY = v3.y > v4.y ? (v3.y > maxY ? v3.y : maxY) : (v4.y > maxY ? v4.y : maxY);
        minX = v1.x < v2.x ? v1.x : v2.x;
        minX = v3.x < v4.x ? (v3.x < minX ? v3.x : minX) : (v4.x < minX ? v4.x : minX);
        minY = v1.y < v2.y ? v1.y : v2.y;
        minY = v3.y < v4.y ? (v3.y < minY ? v3.y : minY) : (v4.y < minY ? v4.y : minY);
        this.m_Bounds.width = maxX - minX;
        this.m_Bounds.height = maxY - minY;
        this.m_Bounds.x = minX;
        this.m_Bounds.y = minY;
        for (var childIndex in this.m_Children) {
            var child = this.m_Children[childIndex];
            if (child != null && child.m_Visible && child.m_Color.alpha > 0.0) {
                var childBounds = child.getBounds(updateChilds);
                this.m_Bounds.unite(childBounds);
            }
        }
        PSPool.free(pivot);
        PSPool.free(v1);
        PSPool.free(v2);
        PSPool.free(v3);
        PSPool.free(v4);
    };
    /** Returns a reference to the bounds rectnagle */
    PSEntity.prototype.getBounds = function (forceUpdate, updateChilds) {
        if (forceUpdate === void 0) { forceUpdate = true; }
        if (updateChilds === void 0) { updateChilds = true; }
        if (forceUpdate) {
            this.updateBounds(updateChilds);
        }
        return this.m_Bounds;
    };
    /** Returns true if the point is inside the boundaries of this object */
    PSEntity.prototype.hitTestPoint = function (p) {
        return this.m_Bounds.containsPoint(p);
    };
    /** Converts a point to global to local object space, returns a new point */
    PSEntity.prototype.globalToLocal = function (p, targetPoint) {
        this.forceMatrixUpdate();
        return this.m_WorldMatrix.inverse().transformPoint(p, targetPoint);
    };
    /** Converts a point from local space to global space, returns a new point */
    PSEntity.prototype.localToGlobal = function (p, targetPoint) {
        this.forceMatrixUpdate();
        return this.m_WorldMatrix.transformPoint(p, targetPoint);
    };
    /** Called on every object when a Touch event occurs (finger touch or mouse click) */
    PSEntity.prototype.onNativeEvent = function (event) {
        if (!event.captured && this.m_Visible) {
            this.updateBounds(true);
            var stageLoc = PSPool.alloc(PSPoint);
            if (this.hitTestPoint(stageLoc.setValues(event.stageX, event.stageY))) {
                var hasListener = this.hasEventListener(event.type);
                event.captured = this.m_CaptureInput && hasListener;
                if (event.type == PSTouchEvent.TOUCH_BEGIN) {
                    this.m_MouseDown = true;
                    this.m_MouseDownPoint.setValues(event.stageX, event.stageY);
                    if (hasListener) {
                        event.target = this;
                        this.dispatchEvent(event);
                    }
                }
                else if (event.type == PSTouchEvent.TOUCH_TAP) {
                    if (hasListener) {
                        this.m_MouseDown = false;
                        event.target = this;
                        this.dispatchEvent(event);
                    }
                }
                else if (event.type == PSTouchEvent.TOUCH_END && this.m_MouseDown) {
                    this.m_MouseDown = false;
                    if (hasListener) {
                        event.target = this;
                        this.dispatchEvent(event);
                    }
                }
                else if (event.type == PSTouchEvent.TOUCH_MOVE) {
                    if (!this.m_MouseDown) {
                        if (hasListener) {
                            var e = new PSTouchEvent(PSTouchEvent.TOUCH_OVER);
                            e.stageX = event.stageX;
                            e.stageY = event.stageY;
                            e.target = this;
                            this.dispatchEvent(e);
                        }
                    }
                    else if (hasListener) {
                        event.target = this;
                        this.dispatchEvent(event);
                    }
                }
            }
            else {
                if (this.m_MouseDown) {
                    this.m_MouseDown = false;
                    var e = new PSTouchEvent(PSTouchEvent.TOUCH_OUT);
                    e.stageX = event.stageX;
                    e.stageY = event.stageY;
                    e.target = this;
                    this.dispatchEvent(e);
                }
            }
            PSPool.free(stageLoc);
        }
    };
    /** Remove and destroy the child */
    PSEntity.prototype.destroyChild = function (child) {
        var index = this.m_Children.indexOf(child);
        if (index != -1) {
            this.m_Children.splice(index, 1);
        }
        child.m_Parent = null;
        this.addEntityToDestroy(child);
    };
    return PSEntity;
})(PSEventDispatcher);
;
var PSResourceManager = (function () {
    function PSResourceManager() {
        this.m_ResourcesToLoad = [];
        this.m_LoadedResources = [];
        this.m_ResourcesMap = {};
        this.m_Verbose = false;
    }
    Object.defineProperty(PSResourceManager, "instance", {
        get: function () {
            if (PSResourceManager.s_Instance == null)
                PSResourceManager.s_Instance = new PSResourceManager;
            return PSResourceManager.s_Instance;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSResourceManager.prototype, "verbose", {
        get: function () { return this.m_Verbose; },
        set: function (b) { this.m_Verbose = b; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSResourceManager.prototype, "resourceCountToLoad", {
        get: function () { return this.m_ResourcesToLoad.length; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSResourceManager.prototype, "loadingResources", {
        get: function () { return this.m_ResourcesToLoad.length > 0; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSResourceManager.prototype, "loadedPercentage", {
        get: function () {
            if (this.m_ResourcesToLoad.length > 0 || this.m_LoadedResources.length > 0) {
                return 1.0 - (this.m_ResourcesToLoad.length / (this.m_ResourcesToLoad.length + this.m_LoadedResources.length));
            }
            else {
                return 0.0;
            }
        },
        enumerable: true,
        configurable: true
    });
    PSResourceManager.prototype.loadResource = function (newRes, optCallback) {
        if (optCallback === void 0) { optCallback = PSResourceManager.onResourceLoaded; }
        if (newRes != null) {
            var asset = this.getResource(newRes.getResourceName());
            if (asset != null) {
                newRes = null;
                return asset;
            }
            else {
                this.m_ResourcesMap[newRes.getResourceName()] = newRes;
                this.m_ResourcesToLoad.push(newRes);
                newRes.load(optCallback);
                if (this.m_Verbose) {
                    console.log('PSResourceManager :: Loading ' + newRes.getResourceName() + '...');
                }
            }
        }
        return newRes;
    };
    PSResourceManager.prototype.getResource = function (name) {
        var asset = this.m_ResourcesMap[name];
        return asset;
    };
    PSResourceManager.onResourceLoaded = function (newRes) {
        PSResourceManager.instance.resourceLoaded(newRes);
    };
    PSResourceManager.onResourceUnLoaded = function (res) {
        PSResourceManager.instance.resourceUnLoaded(res);
    };
    PSResourceManager.prototype.resourceLoaded = function (newRes) {
        var assetIndex = this.m_ResourcesToLoad.indexOf(newRes);
        if (assetIndex != -1) {
            this.m_ResourcesToLoad.splice(assetIndex, 1);
        }
        this.m_LoadedResources.push(newRes);
        this.m_ResourcesMap[newRes.getResourceName()] = newRes;
        if (this.m_Verbose) {
            console.log('PSResourceManager :: Loaded ' + newRes.getResourceName());
        }
    };
    PSResourceManager.prototype.resourceUnLoaded = function (res) {
        var assetIndex = this.m_LoadedResources.indexOf(res);
        if (assetIndex != -1) {
            this.m_LoadedResources.splice(assetIndex, 1);
            this.m_ResourcesMap[res.getResourceName()] = null;
        }
    };
    PSResourceManager.prototype.updateLoads = function () {
        for (var resIndex in this.m_ResourcesToLoad) {
            var res = this.m_ResourcesToLoad[resIndex];
            res.load(res.getLoaderCallback());
        }
    };
    PSResourceManager.prototype.setResourceKey = function (key, res) {
        this.m_LoadedResources.push(res);
        this.m_ResourcesMap[key] = res;
    };
    PSResourceManager.s_Instance = null;
    return PSResourceManager;
})();
;
var PS_RESMNGR = PSResourceManager.instance;
/// <reference path="../system/ResourceManager.ts"/>
/// <reference path="../math/Matrix.ts"/>
//enum ETexFilterStyle
var ETexFilterStyle = {
    TS_STANDARD: 0,
    TS_PIXELART: 1
};
var PSTexture = (function () {
    function PSTexture(path, repeat) {
        if (repeat === void 0) { repeat = false; }
        this.m_Path = '';
        this.m_Loaded = false;
        this.m_Loading = false;
        this.m_Width = 0;
        this.m_Height = 0;
        this.m_POTWidth = 0;
        this.m_POTHeight = 0;
        this.m_Repeat = false;
        this.m_TextureMatrix = new PSMatrix();
        this.m_Path = path;
        this.m_Repeat = repeat;
    }
    Object.defineProperty(PSTexture.prototype, "textureId", {
        get: function () { return this.m_TextureId; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTexture.prototype, "width", {
        get: function () { return this.m_POTWidth; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTexture.prototype, "height", {
        get: function () { return this.m_POTHeight; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTexture.prototype, "sourceWidth", {
        get: function () { return this.m_Width; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTexture.prototype, "sourceHeight", {
        get: function () { return this.m_Height; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTexture.prototype, "textureMatrix", {
        get: function () { return this.m_TextureMatrix; },
        enumerable: true,
        configurable: true
    });
    PSTexture.prototype.load = function (callback) {
        if (this.m_Loading || this.m_Loaded)
            return;
        this.m_Loading = true;
        this.m_LoaderCallback = callback;
        this.m_ImageElement = new Image();
        this.m_ImageElement['m_TextureObject'] = this; //uugh!
        this.m_ImageElement.onload = function () {
            this['m_TextureObject'].innerCallback();
        };
        this.m_ImageElement.src = this.m_Path;
    };
    PSTexture.prototype.unload = function (callback) {
        this.m_LoaderCallback = callback;
    };
    PSTexture.prototype.isLoaded = function () {
        return this.m_Loaded;
    };
    PSTexture.prototype.getResourceName = function () {
        return this.m_Path;
    };
    PSTexture.prototype.getLoaderCallback = function () {
        return this.m_LoaderCallback;
    };
    PSTexture.prototype.innerCallback = function () {
        var image = this.m_ImageElement;
        this.m_POTWidth = this.m_Width = this.m_ImageElement.width;
        this.m_POTHeight = this.m_Height = this.m_ImageElement.height;
        if (!PSTexture.isPowerOfTwo(this.m_Width) || !PSTexture.isPowerOfTwo(this.m_Height)) {
            // Scale up the texture to the next highest power of two dimensions.
            var canvas = document.createElement('canvas');
            canvas.width = this.m_POTWidth = PSTexture.nextHighestPowerOfTwo(image.width);
            canvas.height = this.m_POTHeight = PSTexture.nextHighestPowerOfTwo(image.height);
            var ctx = canvas.getContext('2d');
            ctx.drawImage(image, 0, 0, image.width, image.height);
            //image = new Image('image/png');
            //image.src = canvas.toDataUrl();
            image = canvas;
        }
        this.createTexture(image, true);
        this.m_TextureMatrix = new PSMatrix();
        this.m_TextureMatrix.scale(this.m_Width / this.m_POTWidth, this.m_Height / this.m_POTHeight);
        this.m_Loaded = true;
        this.m_Loading = false;
        if (this.m_LoaderCallback != null) {
            this.m_LoaderCallback(this);
        }
    };
    PSTexture.prototype.createTexture = function (image, genmipmap) {
        if (genmipmap === void 0) { genmipmap = true; }
        var gl = PSGame.CURRENT_GAME.renderSupport.renderContext;
        this.m_TextureId = gl.createTexture();
        this.updateTexture(image, genmipmap);
    };
    PSTexture.prototype.updateTexture = function (image, genmipmap) {
        if (genmipmap === void 0) { genmipmap = true; }
        var gl = PSGame.CURRENT_GAME.renderSupport.renderContext;
        gl.bindTexture(gl.TEXTURE_2D, this.m_TextureId);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
        if (this.m_Repeat) {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
        }
        else {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        }
        if (PSTexture.TEXTURE_FILTER_STYLE == ETexFilterStyle.TS_STANDARD) {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        }
        else if (PSTexture.TEXTURE_FILTER_STYLE == ETexFilterStyle.TS_PIXELART) {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        }
        if (genmipmap) {
            gl.generateMipmap(gl.TEXTURE_2D);
        }
        gl.bindTexture(gl.TEXTURE_2D, null);
    };
    PSTexture.isPowerOfTwo = function (x) {
        return (x & (x - 1)) == 0;
    };
    PSTexture.nextHighestPowerOfTwo = function (x) {
        --x;
        for (var i = 1; i < 32; i <<= 1) {
            x = x | x >> i;
        }
        return x + 1;
    };
    PSTexture.getTextureResource = function (resource) {
        var res = PSResourceManager.instance.getResource(resource);
        if (res == null) {
            res = PSResourceManager.instance.loadResource(new PSTexture(resource));
        }
        return res;
    };
    PSTexture.TEXTURE_FILTER_STYLE = ETexFilterStyle.TS_STANDARD;
    return PSTexture;
})();
/// <reference path="./Entity.ts"/>
/// <reference path="../graphics/Texture.ts"/>
var PSSprite = (function (_super) {
    __extends(PSSprite, _super);
    function PSSprite(p) {
        _super.call(this);
        this.m_TextureTransform = new PSMatrix();
        if (p != null) {
            this.setTexture(p);
        }
        else {
            this.m_Texture = PSBuiltInGfx.PS_DEFAULT_TEXTURE;
        }
        this.m_IncludeWidthHeight = true;
        this.setTextureTransform(0.0, 0.0, 1.0, 1.0, 0.0);
    }
    PSSprite.prototype.setTexture = function (resource) {
        if (typeof resource == 'string') {
            this.m_Texture = PSResourceManager.instance.getResource(resource);
            if (this.m_Texture == null) {
                this.m_Texture = PSResourceManager.instance.loadResource(new PSTexture(resource));
            }
        }
        else if (resource instanceof PSTexture) {
            this.m_Texture = resource;
        }
        if (this.m_Texture.isLoaded()) {
            this.setScaleFromTexture();
        }
    };
    PSSprite.prototype.setTextureTransform = function (panX, panY, scaleX, scaleY, rotation) {
        this.m_TextureTransform.identity();
        this.m_TextureTransform.translate(panX, panY);
        this.m_TextureTransform.scale(scaleX, scaleY);
        this.m_TextureTransform.rotate(rotation);
    };
    ;
    PSSprite.prototype.render = function (support, gl) {
        var material = PSBuiltInGfx.PS_DEFAULT_MATERIAL;
        var meshVAO = PSBuiltInGfx.PS_DEFAULT_QUAD_VAO;
        var tempTexMatrix = PSPool.alloc(PSMatrix);
        material.uSampler = this.m_Texture;
        material.uVPMatrix = support.m_CurrentVPMatrix;
        material.uModelMatrix = support.m_CurrentModelMatrix;
        material.uColor = support.m_CurrentColor;
        tempTexMatrix.concat(this.m_Texture.textureMatrix);
        tempTexMatrix.concat(this.m_TextureTransform);
        material.uTMatrix = tempTexMatrix;
        material.apply(support);
        gl['ext'].vao.bindVertexArrayOES(meshVAO);
        gl.drawArrays(gl.TRIANGLES, 0, 6);
        gl['ext'].vao.bindVertexArrayOES(null);
        PSPool.free(tempTexMatrix);
    };
    PSSprite.prototype.setScaleFromTexture = function () {
        this.width = this.m_Texture.sourceWidth;
        this.height = this.m_Texture.sourceHeight;
    };
    return PSSprite;
})(PSEntity);
;
/// <reference path="./Sprite.ts"/>
var PSTextSprite = (function (_super) {
    __extends(PSTextSprite, _super);
    function PSTextSprite(text, fontSize, fontFamily, forcedWidth, forcedHeight) {
        if (text === void 0) { text = ''; }
        if (fontSize === void 0) { fontSize = 12; }
        if (fontFamily === void 0) { fontFamily = 'Helvetica'; }
        _super.call(this);
        this.m_FontSize = fontSize;
        this.m_FontFamily = fontFamily;
        this.m_InnerCanvas = document.createElement('canvas');
        this.m_InnerContext = this.m_InnerCanvas.getContext('2d');
        this.m_Texture = new PSTexture('PSTextSprite-InnerTexture');
        this.m_Texture.createTexture(this.m_InnerCanvas, false);
        this.setText(text, forcedWidth, forcedHeight);
        this.m_MaxWidth = -1;
    }
    PSTextSprite.prototype.setText = function (text, forcedWidth, forcedHeight) {
        this.m_Text = text;
        this.m_MaxWidth = forcedWidth || -1;
        this.width = forcedWidth || 0;
        this.height = forcedHeight || this.m_FontSize + 4;
        this.compose();
    };
    PSTextSprite.prototype.compose = function () {
        this.m_InnerContext.textBaseline = 'top';
        this.m_InnerContext.font = (this.m_FontSize + 'px ' + this.m_FontFamily);
        if (this.width == 0) {
            this.width = this.m_InnerContext.measureText(this.m_Text).width;
        }
        if (!PSTexture.isPowerOfTwo(this.width) || !PSTexture.isPowerOfTwo(this.height)) {
            //this.width = PSTexture.nextHighestPowerOfTwo(this.width);
            this.height = PSTexture.nextHighestPowerOfTwo(this.height);
        }
        this.m_InnerCanvas.width = this.width;
        this.m_InnerCanvas.height = this.height;
        this.m_InnerContext.fillStyle = this.m_Color.getRGBHexString();
        this.m_InnerContext.textBaseline = 'top';
        this.m_InnerContext.font = (this.m_FontSize + 'px ' + this.m_FontFamily);
        this.m_InnerContext.fillText(this.m_Text, 0, 0, (this.m_MaxWidth < 1) ? this.width : this.m_MaxWidth);
        this.m_Texture.updateTexture(this.m_InnerCanvas, false);
    };
    return PSTextSprite;
})(PSSprite);
var PSMaterial = (function () {
    function PSMaterial(name, program) {
        //resource
        this.m_Path = '';
        this.m_Loaded = false;
        this.m_Loading = false;
        this.m_Path = name;
        this.m_ShaderProgram = program;
    }
    Object.defineProperty(PSMaterial, "CURRENT_MATERIAL", {
        //current material
        get: function () { return PSMaterial.s_CurrentMaterial; },
        enumerable: true,
        configurable: true
    });
    PSMaterial.prototype.releasePrevious = function (support) {
        if (PSMaterial.s_CurrentMaterial != null && PSMaterial.s_CurrentMaterial != this) {
            PSMaterial.s_CurrentMaterial.release(support);
        }
        PSMaterial.s_CurrentMaterial = this;
    };
    /** Do not call super.apply() or super.release() methods, overwrite them */
    //pure virtual methods (abstract methos not supported in typescript, runtime check is required)
    PSMaterial.prototype.apply = function (support) {
        throw 'Implementation of apply() method required for PSMaterial derived class ' +
            this['__proto__'].constructor.name +
            '. Do not call super.apply().';
    };
    PSMaterial.prototype.release = function (support) {
        throw 'Implementation of release() method required for PSMaterial derived class ' +
            this['__proto__'].constructor.name +
            '. Do not call super.release().';
    };
    PSMaterial.prototype.loadUniforms = function (gl) {
    };
    PSMaterial.prototype.load = function (callback) {
        if (this.m_Loaded)
            return;
        this.m_Loading = true;
        this.m_LoaderCallback = callback;
        if (this.m_ShaderProgram.isLoaded()) {
            this.loadUniforms(PSGame.CURRENT_GAME.renderSupport.renderContext);
            this.m_Loaded = true;
            if (this.m_LoaderCallback != null) {
                this.m_LoaderCallback(this);
            }
        }
    };
    PSMaterial.prototype.unload = function (callback) {
        this.m_LoaderCallback = callback;
    };
    PSMaterial.prototype.isLoaded = function () {
        return this.m_Loaded;
    };
    PSMaterial.prototype.getResourceName = function () {
        return this.m_Path;
    };
    PSMaterial.prototype.getLoaderCallback = function () {
        return this.m_LoaderCallback;
    };
    return PSMaterial;
})();
;
/// <reference path="./Material.ts"/>
var PSBuiltInGfx = (function () {
    function PSBuiltInGfx() {
    }
    PSBuiltInGfx.load = function () {
        var gl = PSGame.CURRENT_GAME.renderSupport.renderContext;
        //setup aux 8x8 texture
        var aux2DCanvas = document.createElement('canvas');
        var aux2DContext = aux2DCanvas.getContext('2d');
        aux2DCanvas.width = aux2DCanvas.height = 8;
        aux2DContext.fillStyle = '#ffffff';
        aux2DContext.fillRect(0, 0, 8, 8);
        //'BuiltIn8x8WhiteTexture'
        var aux8x8Texture = new PSTexture(aux2DCanvas.toDataURL());
        aux8x8Texture.load(function (res) {
            //hacking resource indexing
            PSResourceManager.instance.setResourceKey('BuiltIn8x8WhiteTexture', res);
        });
        PSBuiltInGfx.PS_DEFAULT_TEXTURE = aux8x8Texture;
        //setup standard shader program
        var stdVert = new PSShaderSource('BuiltInShaderSrc-Standard-Vert', EShaderSourceType.VERTEX_SHADER);
        var stdFrag = new PSShaderSource('BuiltInShaderSrc-Standard-Frag', EShaderSourceType.FRAGMENT_SHADER);
        var stdProg = new PSShaderProgram('BuiltInShaderProg-Standard');
        PSBuiltInGfx.PS_DEFAULT_SHADER = stdProg;
        stdVert.compileSource(PSBuiltInGfx.PS_DEFAULT_SHADER_VERT_SRC);
        stdFrag.compileSource(PSBuiltInGfx.PS_DEFAULT_SHADER_FRAG_SRC);
        stdProg.attachSource(stdVert);
        stdProg.attachSource(stdFrag);
        PSResourceManager.instance.loadResource(stdProg);
        //TODO move to mesh class
        //setup standard VAO
        var vao = gl['ext'].vao.createVertexArrayOES();
        PSBuiltInGfx.PS_DEFAULT_QUAD_VAO = vao;
        gl['ext'].vao.bindVertexArrayOES(vao);
        var vertices = new Float32Array([
            //	POS  	  	UV
            0.0, 0.0, 0.0, 0.0,
            1.0, 0.0, 1.0, 0.0,
            1.0, 1.0, 1.0, 1.0,
            0.0, 0.0, 0.0, 0.0,
            1.0, 1.0, 1.0, 1.0,
            0.0, 1.0, 0.0, 1.0
        ]);
        var vbo = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
        gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(PSBuiltInAttributes.POSITION.location, 2, gl.FLOAT, false, 16, 0);
        gl.vertexAttribPointer(PSBuiltInAttributes.TEXCOORD.location, 2, gl.FLOAT, false, 16, 8);
        gl.enableVertexAttribArray(PSBuiltInAttributes.POSITION.location);
        gl.enableVertexAttribArray(PSBuiltInAttributes.TEXCOORD.location);
        //TODO: disable other vertex attribues if needed
        gl['ext'].vao.bindVertexArrayOES(null);
        //setup standard material
        PSBuiltInGfx.PS_DEFAULT_MATERIAL = new PSBuiltInSpriteMaterial('BuiltInSpriteMaterial', PSBuiltInGfx.PS_DEFAULT_SHADER);
        //PSBuiltInGfx.PS_DEFAULT_MATERIAL.loadUniforms(gl);
        PSResourceManager.instance.loadResource(PSBuiltInGfx.PS_DEFAULT_MATERIAL);
        //GLOW Effect
        var glowVert = new PSShaderSource('BuiltInShaderSrc-Null-Vert', EShaderSourceType.VERTEX_SHADER);
        var glowFrag = new PSShaderSource('BuiltInShaderSrc-Glow-Frag', EShaderSourceType.FRAGMENT_SHADER);
        var glowProg = new PSShaderProgram('BuiltInShaderProg-Glow');
        PSBuiltInGfx.PS_GLOW_SHADER = glowProg;
        glowVert.compileSource(PSBuiltInGfx.PS_NULL_SHADER_VERT_SRC);
        glowFrag.compileSource(PSBuiltInGfx.PS_GLOW_SHADER_FRAG_SRC);
        glowProg.attachSource(glowVert);
        glowProg.attachSource(glowFrag);
        PSResourceManager.instance.loadResource(glowProg);
        PSBuiltInGfx.PS_GLOW_EFFECT = new PSBuiltInGlowEffect('BuiltInGlowEffect', PSBuiltInGfx.PS_GLOW_SHADER);
        PSResourceManager.instance.loadResource(PSBuiltInGfx.PS_GLOW_EFFECT);
    };
    PSBuiltInGfx.PS_DEFAULT_SHADER_VERT_SRC = '' +
        'attribute vec2 aVertexPosition;' +
        'attribute vec2 aTextureCoord;' +
        'uniform mat3 uMVPMatrix;' +
        'varying vec2 vTextureCoord;' +
        'void main(void){' +
        '	vTextureCoord = aTextureCoord;' +
        '	gl_Position = vec4((uMVPMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);' +
        '}' +
        '';
    PSBuiltInGfx.PS_DEFAULT_SHADER_FRAG_SRC = '' +
        'precision mediump float;' +
        'uniform sampler2D uSampler;' +
        'uniform mat3 uTMatrix;' +
        'uniform vec4 uColor;' +
        'varying vec2 vTextureCoord;' +
        'void main(void){' +
        '	vec4 color = texture2D(uSampler, (uTMatrix * vec3(vTextureCoord, 1.0)).xy);' +
        '	gl_FragColor = color * uColor;' +
        //'	gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);'+
        '}' +
        '';
    PSBuiltInGfx.PS_NULL_SHADER_VERT_SRC = '' +
        'precision mediump float;\n' +
        'attribute vec2 aVertexPosition;' +
        'attribute vec2 aTextureCoord;' +
        'uniform mat3 uMVPMatrix;' +
        'varying vec2 vTextureCoord;' +
        'void main(void){' +
        '	vTextureCoord = aTextureCoord;' +
        '	gl_Position = vec4((uMVPMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);' +
        '}' +
        '';
    //TODO: http://wp.applesandoranges.eu/?p=14
    PSBuiltInGfx.PS_GLOW_SHADER_FRAG_SRC = '' +
        'precision mediump float;\n' +
        'uniform sampler2D uSampler;\n' +
        'uniform vec2 uScreenSize;\n' +
        'uniform int uPass;\n' +
        'varying vec2 vTextureCoord;\n' +
        'void main(void){\n' +
        '	vec2 pixelSize = 1.1 / vec2(uScreenSize.x, uScreenSize.y);\n' +
        '	vec2 pos = gl_FragCoord.xy / (uScreenSize * 1.0);\n' +
        '	float values[25];\n' +
        '	values[0]=0.005;\n' +
        '	values[1]=0.01;\n' +
        '	values[2]=0.015;\n' +
        '	values[3]=0.02;\n' +
        '	values[4]=0.03;\n' +
        '	values[5]=0.04;\n' +
        '	values[6]=0.05;\n' +
        '	values[7]=0.06;\n' +
        '	values[8]=0.07;\n' +
        '	values[9]=0.08;\n' +
        '	values[10]=0.10;\n' +
        '	values[11]=0.11;\n' +
        '	values[12]=0.35;\n' +
        '	values[13]=0.11;\n' +
        '	values[14]=0.10;\n' +
        '	values[15]=0.08;\n' +
        '	values[16]=0.07;\n' +
        '	values[17]=0.06;\n' +
        '	values[18]=0.05;\n' +
        '	values[19]=0.04;\n' +
        '	values[20]=0.03;\n' +
        '	values[21]=0.02;\n' +
        '	values[22]=0.015;\n' +
        '	values[23]=0.01;\n' +
        '	values[24]=0.005;\n' +
        '	vec4 result;\n' +
        '	vec2 curSamplePos;\n' +
        '					  \n' +
        '	if(uPass == 0){\n' +
        '		curSamplePos=vec2(pos.x, pos.y - 11.0 * pixelSize.y);\n' +
        '		for(int i=0;i<25;i++)\n' +
        '		{\n' +
        '			vec4 tempCol = texture2D(uSampler, curSamplePos);' +
        '			result += tempCol * values[i];\n' +
        '			curSamplePos.y += pixelSize.y;\n' +
        '		}\n' +
        //'		result += texture2D(uSampler, pos);\n'+
        '	} else {' +
        '		curSamplePos = vec2(pos.x - 11.0 * pixelSize.x, pos.y);\n' +
        '		for(int i=0;i<25;i++)\n' +
        '		{\n' +
        '			vec4 tempCol = texture2D(uSampler, curSamplePos);' +
        '			result += tempCol * values[i];\n' +
        '			curSamplePos.x += pixelSize.x;\n' +
        '		}' +
        //'		result += texture2D(uSampler, pos);\n'+
        //'	} else if(uPass == 2){\n'+
        //'		result = texture2D(uSampler, pos);\n'+
        '	}\n' +
        '	gl_FragColor = vec4(result.xyz, 1.0);\n' +
        '}\n' +
        '';
    return PSBuiltInGfx;
})();
var PSBuiltInSpriteMaterial = (function (_super) {
    __extends(PSBuiltInSpriteMaterial, _super);
    function PSBuiltInSpriteMaterial(name, program) {
        _super.call(this, name, program);
        this.uModelMatrix = new PSMatrix();
        this.uVPMatrix = new PSMatrix();
        this.uTMatrix = new PSMatrix();
    }
    PSBuiltInSpriteMaterial.prototype.loadUniforms = function (gl) {
        var prog = this.m_ShaderProgram;
        this.m_uSamplerLocation = gl.getUniformLocation(prog.programId, 'uSampler');
        this.m_uMVPMatrixLocation = gl.getUniformLocation(prog.programId, 'uMVPMatrix');
        this.m_uColorLocation = gl.getUniformLocation(prog.programId, 'uColor');
        this.m_uTMatrixLocation = gl.getUniformLocation(prog.programId, 'uTMatrix');
    };
    PSBuiltInSpriteMaterial.prototype.apply = function (support) {
        this.releasePrevious(support);
        var gl = support.renderContext;
        var prog = this.m_ShaderProgram;
        prog.use(gl);
        var mvp = new PSMatrix(this.uModelMatrix);
        mvp.concat(this.uVPMatrix);
        gl.uniformMatrix3fv(this.m_uMVPMatrixLocation, false, mvp.arrayBuffer);
        gl.uniformMatrix3fv(this.m_uTMatrixLocation, false, this.uTMatrix.arrayBuffer);
        gl.uniform4fv(this.m_uColorLocation, this.uColor.arrayBuffer);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.uSampler.textureId);
        gl.uniform1i(this.m_uSamplerLocation, 0);
    };
    PSBuiltInSpriteMaterial.prototype.release = function (support) {
        var prog = this.m_ShaderProgram;
    };
    return PSBuiltInSpriteMaterial;
})(PSMaterial);
var PSBuiltInGlowEffect = (function (_super) {
    __extends(PSBuiltInGlowEffect, _super);
    function PSBuiltInGlowEffect(name, program) {
        _super.call(this, name, program);
        this.m_MVPMatrix = new PSMatrix();
        this.m_Pass = 0;
    }
    PSBuiltInGlowEffect.prototype.setColorTexture = function (texture) {
        this.m_ColorTexture = texture;
    };
    PSBuiltInGlowEffect.prototype.setMVPMatrix = function (mvp) {
        this.m_MVPMatrix = mvp;
    };
    PSBuiltInGlowEffect.prototype.setFBSize = function (width, height) {
        this.m_FBWidth = width;
        this.m_FBHeight = height;
    };
    PSBuiltInGlowEffect.prototype.setPass = function (pass) {
        this.m_Pass = pass;
    };
    PSBuiltInGlowEffect.prototype.loadUniforms = function (gl) {
        var prog = this.m_ShaderProgram;
        this.m_uSamplerLocation = gl.getUniformLocation(prog.programId, 'uSampler');
        this.m_uMVPMatrixLocation = gl.getUniformLocation(prog.programId, 'uMVPMatrix');
        this.m_uScreenSizeLocation = gl.getUniformLocation(prog.programId, 'uScreenSize');
        this.m_uPassLocation = gl.getUniformLocation(prog.programId, 'uPass');
    };
    PSBuiltInGlowEffect.prototype.apply = function (support) {
        this.releasePrevious(support);
        var gl = support.renderContext;
        var prog = this.m_ShaderProgram;
        prog.use(gl);
        gl.uniformMatrix3fv(this.m_uMVPMatrixLocation, false, this.m_MVPMatrix.arrayBuffer);
        gl.uniform2f(this.m_uScreenSizeLocation, this.m_FBWidth, this.m_FBHeight);
        gl.uniform1i(this.m_uPassLocation, this.m_Pass);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.m_ColorTexture);
        gl.uniform1i(this.m_uSamplerLocation, 0);
    };
    PSBuiltInGlowEffect.prototype.release = function (support) {
    };
    return PSBuiltInGlowEffect;
})(PSMaterial);
var PSPostProcessPipeline = (function () {
    function PSPostProcessPipeline(effect) {
        this.m_FrameBuffers = [];
        this.m_ColorTextures = [];
        this.m_Passes = 1;
        this.m_Effect = effect;
    }
    PSPostProcessPipeline.prototype.init = function (support, gl, screenW, screenH, passes) {
        if (passes === void 0) { passes = 1; }
        this.m_Passes = passes;
        this.m_FBWidth = screenW;
        this.m_FBHeight = screenH;
        if (!PSTexture.isPowerOfTwo(screenW) || !PSTexture.isPowerOfTwo(screenH)) {
            // Scale up the texture to the next highest power of two dimensions.
            this.m_FBWidth = PSTexture.nextHighestPowerOfTwo(screenW);
            this.m_FBHeight = PSTexture.nextHighestPowerOfTwo(screenH);
        }
        for (var i = 0; i < passes; i++) {
            this.createRenderTarget(gl, this.m_FBWidth, this.m_FBHeight, i);
        }
        this.m_FullScreenQuadVAO = PSBuiltInGfx.PS_DEFAULT_QUAD_VAO;
        this.m_MVP = new PSMatrix();
        this.m_MVP.scale(this.m_FBWidth, -this.m_FBHeight);
        this.m_MVP.translate(0, screenH); //0.587 * screenH
        this.m_MVP.concat(support.m_CurrentProjectionMatrix);
    };
    PSPostProcessPipeline.prototype.createRenderTarget = function (gl, width, height, index) {
        // Create a color texture
        var colorTexture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, colorTexture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
        // Create the depth texture
        /*
        var depthTexture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, depthTexture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT, screenW, screenH, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_SHORT, null);
        */
        var renderbuffer = gl.createRenderbuffer();
        gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
        gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, this.m_FBWidth, this.m_FBHeight);
        var framebuffer = gl.createFramebuffer();
        gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, colorTexture, 0);
        //gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, depthTexture, 0);
        gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);
        //set to default
        gl.bindTexture(gl.TEXTURE_2D, null);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        this.m_ColorTextures[index] = colorTexture;
        this.m_FrameBuffers[index] = framebuffer;
    };
    PSPostProcessPipeline.prototype.bind = function (support, gl) {
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.m_FrameBuffers[0]);
        gl.blendFuncSeparate(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA, gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
    };
    PSPostProcessPipeline.prototype.render = function (support, gl) {
        var meshVAO = PSBuiltInGfx.PS_DEFAULT_QUAD_VAO;
        gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
        this.m_Effect.setMVPMatrix(this.m_MVP);
        this.m_Effect.setFBSize(this.m_FBWidth, this.m_FBHeight);
        var nextFB = 1;
        var nextTexture = 0;
        for (var p = 0; p < this.m_Passes - 1; ++p) {
            gl.bindFramebuffer(gl.FRAMEBUFFER, this.m_FrameBuffers[nextFB]);
            this.m_Effect.setColorTexture(this.m_ColorTextures[nextTexture]);
            this.m_Effect.setPass(p);
            this.m_Effect.apply(support);
            gl['ext'].vao.bindVertexArrayOES(meshVAO);
            gl.drawArrays(gl.TRIANGLES, 0, 6);
            gl['ext'].vao.bindVertexArrayOES(null);
            nextFB++;
            nextTexture++;
        }
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        this.m_Effect.setColorTexture(this.m_ColorTextures[this.m_Passes - 1]);
        this.m_Effect.setPass(this.m_Passes - 1);
        this.m_Effect.apply(support);
        gl['ext'].vao.bindVertexArrayOES(meshVAO);
        gl.drawArrays(gl.TRIANGLES, 0, 6);
        gl['ext'].vao.bindVertexArrayOES(null);
    };
    return PSPostProcessPipeline;
})();
var PSContextState = (function () {
    function PSContextState(m, c) {
        this.matrix = new PSMatrix(m);
        this.color = new PSColor(c);
    }
    PSContextState.prototype.resetValues = function () {
        this.matrix.identity();
        this.color.setValues(1.0, 1.0, 1.0, 1.0);
    };
    return PSContextState;
})();
var PSRenderSupport = (function () {
    function PSRenderSupport() {
        this.m_ContextStack = [];
        this.m_CurrentModelMatrix = new PSMatrix();
        this.m_CurrentColor = new PSColor();
        this.m_ContextStack = [];
        this.m_CurrentViewMatrix = new PSMatrix();
        this.m_CurrentProjectionMatrix = new PSMatrix();
        this.m_CurrentVPMatrix = new PSMatrix();
        this.pushState(this.m_CurrentModelMatrix, this.m_CurrentColor);
    }
    Object.defineProperty(PSRenderSupport.prototype, "renderContext", {
        get: function () { return this.m_GLContext; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSRenderSupport.prototype, "viewMatrix", {
        set: function (vm) {
            this.m_CurrentViewMatrix = vm;
            this.updateVPMatrix();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSRenderSupport.prototype, "projectionMatrix", {
        set: function (pm) {
            this.m_CurrentProjectionMatrix = pm;
            this.updateVPMatrix();
        },
        enumerable: true,
        configurable: true
    });
    PSRenderSupport.prototype.updateVPMatrix = function () {
        this.m_CurrentVPMatrix.identity();
        this.m_CurrentVPMatrix.concat(this.m_CurrentViewMatrix);
        this.m_CurrentVPMatrix.concat(this.m_CurrentProjectionMatrix);
    };
    PSRenderSupport.prototype.setup = function (canvas) {
        this.m_GLContext = canvas.getContext('webgl', { alpha: false }) ||
            canvas.getContext('experimental-webgl', { alpha: false });
        if (this.m_GLContext == null) {
            console.error('Unnable to initalize WebGL context');
            return;
        }
        this.projectionMatrix = PSMatrix.make2DProjection(canvas.width, canvas.height);
        var gl = this.m_GLContext;
        gl['ext'] = {};
        gl['ext']['vao'] = null;
        var extContext = gl['rawgl'] || gl;
        var vao_ext = (extContext.getExtension('OES_vertex_array_object') ||
            extContext.getExtension('MOZ_OES_vertex_array_object') ||
            extContext.getExtension('WEBKIT_OES_vertex_array_object'));
        if (vao_ext != null) {
            gl['ext']['vao'] = vao_ext;
        }
        else {
            throw 'WEBGL_ERR: OES_vertex_array_object not supported!';
        }
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
        gl.enable(gl.BLEND);
    };
    PSRenderSupport.prototype.pushState = function (m, c) {
        //var state : PSContextState = new PSContextState(this.m_CurrentModelMatrix, this.m_CurrentColor); 
        var state = PSPool.alloc(PSContextState);
        state.color.setFrom(this.m_CurrentColor);
        state.matrix.setFrom(this.m_CurrentModelMatrix);
        this.m_ContextStack.push(state);
        var newMatrix = PSPool.alloc(PSMatrix).setFrom(m).concat(this.m_CurrentModelMatrix);
        this.m_CurrentModelMatrix.setFrom(newMatrix);
        PSPool.free(newMatrix);
        this.m_CurrentColor.multiply(c);
    };
    PSRenderSupport.prototype.popState = function () {
        var state = this.m_ContextStack.pop();
        this.m_CurrentModelMatrix.setFrom(state.matrix);
        this.m_CurrentColor.setFrom(state.color);
        PSPool.free(state);
        //delete state;
    };
    PSRenderSupport.prototype.restoreBlendFunc = function () {
        var gl = this.m_GLContext;
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
        gl.enable(gl.BLEND);
    };
    return PSRenderSupport;
})();
/// <reference path="../system/ResourceManager.ts"/>
var PSVertexAttribute = (function () {
    function PSVertexAttribute(n, l) {
        this.name = n;
        this.location = l;
    }
    return PSVertexAttribute;
})();
var PSBuiltInAttributes = (function () {
    function PSBuiltInAttributes() {
    }
    PSBuiltInAttributes.POSITION = new PSVertexAttribute('aVertexPosition', 0);
    PSBuiltInAttributes.TEXCOORD = new PSVertexAttribute('aTextureCoord', 1);
    return PSBuiltInAttributes;
})();
var PSShaderProgram = (function () {
    function PSShaderProgram(name) {
        this.m_Loading = false;
        this.m_Loaded = false;
        this.m_Path = '';
        this.m_LoaderCallback = null;
        this.m_ShaderSources = [];
        this.m_Path = name;
        this.m_Loaded = false;
    }
    Object.defineProperty(PSShaderProgram, "PROGRAM_IN_USE", {
        get: function () { return PSShaderProgram.s_ProgramInUse; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSShaderProgram.prototype, "programId", {
        get: function () { return this.m_ProgramId; },
        enumerable: true,
        configurable: true
    });
    PSShaderProgram.prototype.load = function (callback) {
        //console.log('shader program load called for '+this.m_Path);
        this.m_LoaderCallback = callback || this.m_LoaderCallback;
        if (this.m_Loaded)
            return;
        var sourcesLoaded = true;
        //will not link until all attached shader sources has been compiled
        for (var shaderIndex in this.m_ShaderSources) {
            var shader = this.m_ShaderSources[shaderIndex];
            if (!shader.isLoaded()) {
                sourcesLoaded = false;
                break;
            }
        }
        if (!sourcesLoaded)
            return;
        this.link();
        this.m_Loaded = true;
        if (this.m_LoaderCallback != null)
            this.m_LoaderCallback(this);
    };
    PSShaderProgram.prototype.unload = function (callback) {
        this.m_LoaderCallback = callback;
    };
    PSShaderProgram.prototype.isLoaded = function () {
        return this.m_Loaded;
    };
    PSShaderProgram.prototype.getResourceName = function () {
        return this.m_Path;
    };
    PSShaderProgram.prototype.getLoaderCallback = function () {
        return this.m_LoaderCallback;
    };
    PSShaderProgram.prototype.attachSource = function (shader) {
        if (this.m_ShaderSources.indexOf(shader) == -1) {
            this.m_ShaderSources.push(shader);
        }
    };
    PSShaderProgram.prototype.link = function () {
        var gl = PSGame.CURRENT_GAME.renderSupport.renderContext;
        this.m_ProgramId = gl.createProgram();
        for (var shaderIndex in this.m_ShaderSources) {
            var shader = this.m_ShaderSources[shaderIndex];
            gl.attachShader(this.m_ProgramId, shader.sourceId);
        }
        gl.linkProgram(this.m_ProgramId);
        if (!gl.getProgramParameter(this.m_ProgramId, gl.LINK_STATUS)) {
            throw 'ERROR: Unable to link program ' + this.m_Path;
        }
        this.use(gl);
        //all shaders can implement those vertex attributes
        gl.bindAttribLocation(this.m_ProgramId, PSBuiltInAttributes.POSITION.location, PSBuiltInAttributes.POSITION.name);
        gl.bindAttribLocation(this.m_ProgramId, PSBuiltInAttributes.TEXCOORD.location, PSBuiltInAttributes.TEXCOORD.name);
        PSShaderProgram.s_ProgramInUse = null;
    };
    PSShaderProgram.prototype.use = function (gl) {
        if (PSShaderProgram.s_ProgramInUse != this) {
            gl.useProgram(this.m_ProgramId);
            PSShaderProgram.s_ProgramInUse = this;
        }
    };
    return PSShaderProgram;
})();
/// <reference path="../system/ResourceManager.ts"/>
var EShaderSourceType;
(function (EShaderSourceType) {
    EShaderSourceType[EShaderSourceType["FRAGMENT_SHADER"] = 35632] = "FRAGMENT_SHADER";
    EShaderSourceType[EShaderSourceType["VERTEX_SHADER"] = 35633] = "VERTEX_SHADER";
})(EShaderSourceType || (EShaderSourceType = {}));
;
var PSShaderSource = (function () {
    function PSShaderSource(name, type) {
        this.m_Loaded = false;
        this.m_Loading = false;
        this.m_Path = '';
        this.m_Path = name;
        this.m_SourceType = type;
    }
    Object.defineProperty(PSShaderSource.prototype, "sourceId", {
        get: function () { return this.m_SourceId; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSShaderSource.prototype, "sourceType", {
        get: function () { return this.m_SourceType; },
        enumerable: true,
        configurable: true
    });
    PSShaderSource.prototype.isLoaded = function () {
        return this.m_Loaded;
    };
    PSShaderSource.prototype.getResourceName = function () {
        return this.m_Path;
    };
    PSShaderSource.prototype.innerCallback = function (data) {
        this.compileSource(data);
        if (this.m_LoaderCallback != null) {
            this.m_LoaderCallback(this);
        }
    };
    PSShaderSource.prototype.load = function (callback) {
        if (this.m_Loaded || this.m_Loading)
            return;
        this.m_Loading = true;
        this.m_LoaderCallback = callback;
        var shaderFileRequest = new XMLHttpRequest();
        shaderFileRequest['m_ShaderSource'] = this;
        shaderFileRequest.open("GET", this.m_Path, true);
        shaderFileRequest.responseType = "text";
        shaderFileRequest.onload = function () {
            this['m_ShaderSource'].innerCallback(this.response);
        };
        shaderFileRequest.send();
    };
    PSShaderSource.prototype.unload = function (callback) {
        this.m_LoaderCallback = callback;
    };
    PSShaderSource.prototype.getLoaderCallback = function () {
        return this.m_LoaderCallback;
    };
    PSShaderSource.prototype.compileSource = function (source) {
        var gl = PSGame.CURRENT_GAME.renderSupport.renderContext;
        this.m_SourceId = gl.createShader(this.m_SourceType);
        gl.shaderSource(this.m_SourceId, source);
        gl.compileShader(this.m_SourceId);
        if (!gl.getShaderParameter(this.m_SourceId, gl.COMPILE_STATUS)) {
            throw 'PSERROR: ' + gl.getShaderInfoLog(this.m_SourceId);
        }
        this.m_Loaded = true;
    };
    return PSShaderSource;
})();
;
/*
class StaticMesh implements IResource
{
    load(callback : IPSLoadCallback) : void
    {
        if(this.m_Loaded || this.m_Loading)
            return;

        this.m_Loading = true;

        // Static mesh loading from file not implemented yet
    }

    innerCallback(data : string) : void
    {
        this.compileSource(data);

        if(this.m_LoaderCallback != null)
        {
            this.m_LoaderCallback(this);
        }
    }

    isLoaded() : boolean
    {
        return this.m_Loaded;
    }

    getResourceName() : string
    {
        return this.m_Path;
    }

    unload(callback : IPSLoadCallback) : void
    {
        
    }
    
    getLoaderCallback() : IPSLoadCallback
    {
        return this.m_LoaderCallback;
    }


    private m_VAO : any; //** WebGLVertexArrayObjectOES
    private m_VBO : WebGLBuffer;
    private m_Loaded : boolean = false;
    private m_Loading : boolean = false;
    private m_Path : string = '';
    private m_LoaderCallback : IPSLoadCallback;
}
*/
/// <reference path="../display/Entity.ts"/>
var PSAlphaTo = (function (_super) {
    __extends(PSAlphaTo, _super);
    function PSAlphaTo(targetValue, inTime, easeFunc) {
        if (easeFunc === void 0) { easeFunc = PSEaseFuncions.lineal; }
        _super.call(this);
        this.m_InitialValue = 0;
        this.m_DestroyOnFinish = true;
        this.m_InnerTimer = new PSTimer();
        this.addChild(this.m_InnerTimer);
        this.m_EaseFunction = easeFunc;
        this.m_InTime = inTime;
        this.m_TargetValue = targetValue;
    }
    Object.defineProperty(PSAlphaTo.prototype, "estroyOnFinish", {
        get: function () { return this.m_DestroyOnFinish; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSAlphaTo.prototype, "destroyOnFinish", {
        set: function (d) { this.m_DestroyOnFinish = d; },
        enumerable: true,
        configurable: true
    });
    PSAlphaTo.prototype.update = function (clock) {
        if (!this.m_InnerTimer.started) {
            this.m_InitialValue = this.m_Parent.alpha;
            this.m_InnerTimer.start(this.m_InTime);
        }
        if (!this.m_InnerTimer.finished) {
            var delta = this.m_InnerTimer.getDelta(this.m_EaseFunction);
            var deltaValue = (this.m_InitialValue * (1.0 - delta)) + (this.m_TargetValue * delta);
            this.m_Parent.alpha = deltaValue;
        }
        else {
            this.m_Parent.alpha = this.m_TargetValue;
            if (this.m_DestroyOnFinish) {
                this.destroy();
            }
        }
    };
    PSAlphaTo.prototype.resetValues = function (targetValue, inTime, easeFunc) {
        if (easeFunc === void 0) { easeFunc = PSEaseFuncions.lineal; }
        this.m_InnerTimer.resetValues();
        this.m_EaseFunction = easeFunc;
        this.m_InTime = inTime;
        this.m_TargetValue = targetValue;
    };
    return PSAlphaTo;
})(PSEntity);
/// <reference path="../display/Entity.ts"/>
var PSColorTo = (function (_super) {
    __extends(PSColorTo, _super);
    function PSColorTo() {
        _super.apply(this, arguments);
    }
    return PSColorTo;
})(PSEntity);
/// <reference path="../display/Entity.ts"/>
var PSDelay = (function (_super) {
    __extends(PSDelay, _super);
    function PSDelay(objectToAdd, inTime) {
        _super.call(this);
        this.m_InnerTimer = new PSTimer();
        this.addChild(this.m_InnerTimer);
        this.m_InTime = inTime;
        this.m_ObjectToAdd = objectToAdd;
    }
    PSDelay.prototype.update = function (clock) {
        if (!this.m_InnerTimer.started) {
            this.m_InnerTimer.start(this.m_InTime);
        }
        if (this.m_InnerTimer.finished) {
            this.m_Parent.addChild(this.m_ObjectToAdd);
            this.destroy();
        }
    };
    return PSDelay;
})(PSEntity);
var PSEaseFuncions = (function () {
    function PSEaseFuncions() {
    }
    PSEaseFuncions.lineal = function (total, part) {
        return part / total;
    };
    return PSEaseFuncions;
})();
/// <reference path="../display/Entity.ts"/>
var PSScaleTo = (function (_super) {
    __extends(PSScaleTo, _super);
    function PSScaleTo(targetValue, inTime, easeFunc) {
        if (easeFunc === void 0) { easeFunc = PSEaseFuncions.lineal; }
        _super.call(this);
        this.m_InitialValue = null;
        this.m_DestroyOnFinish = true;
        this.m_InnerTimer = new PSTimer();
        this.addChild(this.m_InnerTimer);
        this.m_EaseFunction = easeFunc;
        this.m_InTime = inTime;
        this.m_TargetValue = PSPool.alloc(PSPoint);
        if (targetValue instanceof PSPoint) {
            this.m_TargetValue.setFrom(targetValue);
        }
        else {
            this.m_TargetValue.x = targetValue;
            this.m_TargetValue.y = targetValue;
        }
    }
    Object.defineProperty(PSScaleTo.prototype, "estroyOnFinish", {
        get: function () { return this.m_DestroyOnFinish; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSScaleTo.prototype, "destroyOnFinish", {
        set: function (d) { this.m_DestroyOnFinish = d; },
        enumerable: true,
        configurable: true
    });
    PSScaleTo.prototype.update = function (clock) {
        if (!this.m_InnerTimer.started) {
            this.m_InitialValue = PSPool.alloc(PSPoint).setValues(this.m_Parent.scaleX, this.m_Parent.scaleY);
            this.m_InnerTimer.start(this.m_InTime);
        }
        if (!this.m_InnerTimer.finished) {
            var delta = this.m_InnerTimer.getDelta(this.m_EaseFunction);
            var deltaValueX = (this.m_InitialValue.x * (1.0 - delta)) + (this.m_TargetValue.x * delta);
            var deltaValueY = (this.m_InitialValue.y * (1.0 - delta)) + (this.m_TargetValue.y * delta);
            this.m_Parent.scaleX = deltaValueX;
            this.m_Parent.scaleY = deltaValueY;
        }
        else {
            this.m_Parent.scaleX = this.m_TargetValue.x;
            this.m_Parent.scaleY = this.m_TargetValue.y;
            PSPool.free(this.m_InitialValue);
            if (this.m_DestroyOnFinish) {
                this.destroy();
            }
        }
    };
    PSScaleTo.prototype.resetValues = function (targetValue, inTime, easeFunc) {
        if (easeFunc === void 0) { easeFunc = PSEaseFuncions.lineal; }
        this.m_InnerTimer.resetValues();
        this.m_EaseFunction = easeFunc;
        this.m_InTime = inTime;
        if (targetValue instanceof PSPoint) {
            this.m_TargetValue.setFrom(targetValue);
        }
        else {
            this.m_TargetValue.x = targetValue;
            this.m_TargetValue.y = targetValue;
        }
    };
    return PSScaleTo;
})(PSEntity);
/// <reference path="./Point.ts"/>
/// <reference path="./Rectangle.ts"/>
/// <reference path="../system/Memory.ts"/>
var PSCircle = (function () {
    function PSCircle(_locationX, _locationY, _radius) {
        this.radius = 0;
        this.locationX = 0;
        this.locationY = 0;
        this.radius = _radius;
        this.locationX = _locationX;
        this.locationY = _locationY;
    }
    PSCircle.prototype.intersectsPoint = function (other) {
        var thisPoint = PSPool.alloc(PSPoint);
        thisPoint.setValues(this.locationX, this.locationY);
        var res = thisPoint.distanceTo(other) <= this.radius;
        PSPool.free(thisPoint);
        return res;
    };
    PSCircle.prototype.intersectsCircle = function (other) {
        var thisPoint = PSPool.alloc(PSPoint);
        thisPoint.setValues(this.locationX, this.locationY);
        var otherPoint = PSPool.alloc(PSPoint);
        otherPoint.setValues(other.locationX, other.locationY);
        PSPool.free(otherPoint);
        PSPool.free(thisPoint);
        return thisPoint.distanceTo(otherPoint) <= (this.radius + other.radius);
    };
    PSCircle.prototype.intersectsRectangle = function (other) {
        var res = false;
        var thisPoint = PSPool.alloc(PSPoint);
        thisPoint.setValues(this.locationX, this.locationY);
        var otherPoint = PSPool.alloc(PSPoint);
        otherPoint.setValues(other.x, other.y);
        res = res || this.intersectsPoint(otherPoint);
        otherPoint.setValues(other.x + other.width, other.y);
        res = res || this.intersectsPoint(otherPoint);
        otherPoint.setValues(other.x, other.y + other.height);
        res = res || this.intersectsPoint(otherPoint);
        otherPoint.setValues(other.x + other.width, other.y + other.height);
        res = res || this.intersectsPoint(otherPoint);
        res = res || other.containsPoint(thisPoint);
        PSPool.free(otherPoint);
        PSPool.free(thisPoint);
        return res;
    };
    PSCircle.prototype.updateLocation = function (_locationX, _locationY) {
        this.locationX = _locationX;
        this.locationY = _locationY;
    };
    return PSCircle;
})();
var TileMapCell = (function () {
    function TileMapCell() {
    }
    return TileMapCell;
})();
var TileMap = (function (_super) {
    __extends(TileMap, _super);
    function TileMap(p) {
        _super.call(this);
        this.m_Descriptor = null;
        this.m_Layers = null;
        this.m_MapWidth = 0;
        this.m_MapHeight = 0;
        this.m_TileWidth = 0;
        this.m_TileHeight = 0;
        this.m_Parsed = false;
        this.m_Parsing = false;
        if (p != null) {
            this.setTileMapDescriptor(p);
        }
    }
    Object.defineProperty(TileMap.prototype, "mapWidth", {
        get: function () { return this.m_MapWidth * this.m_TileWidth; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TileMap.prototype, "mapHeight", {
        get: function () { return this.m_MapHeight * this.m_TileHeight; },
        enumerable: true,
        configurable: true
    });
    TileMap.prototype.setTileMapDescriptor = function (resource) {
        this.m_Parsed = false;
        if (typeof resource == 'string') {
            this.m_Descriptor = PSResourceManager.instance.getResource(resource);
            if (this.m_Descriptor == null) {
                this.m_Descriptor = PSResourceManager.instance.loadResource(new TileMapDescriptor(resource));
            }
        }
        else if (resource instanceof TileMapDescriptor) {
            this.m_Descriptor = resource;
        }
    };
    TileMap.prototype.setup = function () {
        //setup the tilemap if we need
        if (!this.m_Parsing && !this.m_Parsed && this.m_Descriptor != null && this.m_Descriptor.isLoaded()) {
            this.m_Parsing = true;
            var info = this.m_Descriptor.info;
            this.m_MapWidth = info.width;
            this.m_MapHeight = info.height;
            this.m_TileWidth = info.tilewidth;
            this.m_TileHeight = info.tileheight;
            this.m_Layers = new Array();
            var totalLayers = info.layers.length;
            for (var layerIndex = 0; layerIndex < totalLayers; ++layerIndex) {
                var layerInfo = info.layers[layerIndex];
                if (!layerInfo.properties.tileset) {
                    console.error('Error parsing layer "' + layerInfo.name + '" :: POOLLOS TileMap implementation ' +
                        'require a custom layer attribute called "tileset" ' +
                        'specifiying the name of the tileset that is being used for this layer. ' +
                        'Only one tileset per layer is supported.');
                    return;
                }
                var newLayer = new TileMapLayer();
                newLayer.mapWidth = layerInfo.width;
                newLayer.mapHeight = layerInfo.height;
                newLayer.type = layerInfo.type;
                newLayer.name = layerInfo.name;
                newLayer.tileset = this.m_Descriptor.getTileSet(layerInfo.properties.tileset);
                for (var prop in layerInfo.properties) {
                    newLayer.extraProperties[prop] = layerInfo.properties[prop];
                }
                newLayer.tileWidth = info.tilewidth;
                newLayer.tileHeight = info.tileheight;
                if (newLayer.type == 'tilelayer') {
                    newLayer.cells = new Array(newLayer.mapWidth * newLayer.mapHeight);
                    var totalCells = newLayer.cells.length;
                    for (var cellIndex = 0; cellIndex < totalCells; ++cellIndex) {
                        var newCell = new TileMapCell();
                        newCell.id = layerInfo.data[cellIndex];
                        newLayer.cells[cellIndex] = newCell;
                    }
                    this.m_Layers[layerIndex] = newLayer;
                    this.addChild(newLayer);
                    var generatedMap = this.m_Descriptor.getGeneratedTexture(newLayer.name);
                    if (generatedMap != null && !TileMap.FORCE_CUSTOM_MESH) {
                        console.log('Using pregenerated image for ' + newLayer.name) + ' layer';
                        newLayer.texture = generatedMap;
                        newLayer.width = generatedMap.width;
                        newLayer.height = generatedMap.height;
                        newLayer.setupGenerated();
                    }
                    else {
                        console.log('Using custom mesh for ' + this.name);
                        newLayer.texture = newLayer.tileset.texture;
                        newLayer.createVAO();
                    }
                }
            }
            this.m_Parsing = false;
            this.m_Parsed = true;
        }
    };
    TileMap.prototype.update = function (clock) {
        //will setup the tile map info
        this.setup();
    };
    TileMap.FORCE_CUSTOM_MESH = false;
    return TileMap;
})(PSEntity);
var TileMapDescriptor = (function () {
    function TileMapDescriptor(path) {
        this.m_Path = '';
        this.m_Loaded = false;
        this.m_DescriptorLoaded = false;
        this.m_Loading = false;
        this.m_TileMapInfo = null;
        this.m_TileSets = {};
        this.m_GeneratedLayers = {};
        this.m_Path = path;
    }
    Object.defineProperty(TileMapDescriptor.prototype, "info", {
        get: function () { return this.m_TileMapInfo; },
        enumerable: true,
        configurable: true
    });
    TileMapDescriptor.prototype.load = function (callback) {
        if (this.m_Loaded)
            return;
        if (this.m_Loading) {
            if (this.m_DescriptorLoaded) {
                var loaded = true;
                for (var tileMapName in this.m_TileSets) {
                    loaded = loaded && this.m_TileSets[tileMapName].isLoaded();
                    if (!loaded) {
                        return;
                    }
                }
                for (var generatedLayer in this.m_GeneratedLayers) {
                    loaded = loaded && this.m_GeneratedLayers[generatedLayer].isLoaded();
                    if (!loaded) {
                        return;
                    }
                }
                this.m_Loaded = loaded;
                if (this.m_Loaded) {
                    if (this.m_LoaderCallback != null) {
                        this.m_LoaderCallback(this);
                    }
                }
            }
            return;
        }
        this.m_Loading = true;
        this.m_LoaderCallback = callback;
        this.m_TileMapInfo = null;
        this.m_TileSets = {};
        var jsonFileRequest = new XMLHttpRequest();
        jsonFileRequest['m_JSONAsset'] = this;
        jsonFileRequest.open("GET", this.m_Path, true);
        jsonFileRequest.responseType = "json";
        jsonFileRequest.onload = function () {
            this['m_JSONAsset'].innerCallback(this.response);
        };
        jsonFileRequest.send();
    };
    TileMapDescriptor.prototype.innerCallback = function (data) {
        //TODO parse json
        this.m_TileMapInfo = (typeof data == 'string') ? JSON.parse(data) : data;
        //TODO load texture map
        var assetDirectory = this.m_Path.replace(/[^\/]*$/, '');
        //PSTexture.getTextureResource(path);
        var tilesets = this.m_TileMapInfo.tilesets;
        for (var i = 0; i < tilesets.length; ++i) {
            var tileset = tilesets[i];
            var newTileset = new TileSetDescriptor(PSTexture.getTextureResource(assetDirectory + tileset.image));
            newTileset.tileWidth = tileset.tilewidth;
            newTileset.tileHeight = tileset.tileheight;
            newTileset.margin = tileset.margin;
            newTileset.spacing = tileset.spacing;
            this.m_TileSets[tileset.name] = newTileset;
        }
        var properties = this.m_TileMapInfo.properties;
        for (var property in properties) {
            if (property.indexOf('generated_') == 0) {
                var texName = properties[property];
                var layerName = property.replace('generated_', '');
                this.m_GeneratedLayers[layerName] = PSTexture.getTextureResource(assetDirectory + texName);
            }
        }
        this.m_DescriptorLoaded = true;
    };
    TileMapDescriptor.prototype.unload = function (callback) {
        this.m_LoaderCallback = callback;
    };
    TileMapDescriptor.prototype.isLoaded = function () {
        return this.m_Loaded;
    };
    TileMapDescriptor.prototype.getResourceName = function () {
        return this.m_Path;
    };
    TileMapDescriptor.prototype.getLoaderCallback = function () {
        return this.m_LoaderCallback;
    };
    TileMapDescriptor.prototype.getTileSet = function (name) {
        return this.m_TileSets[name];
    };
    TileMapDescriptor.prototype.getGeneratedTexture = function (layerName) {
        return this.m_GeneratedLayers[layerName];
    };
    TileMapDescriptor.getTileMapResource = function (resource) {
        var res = PSResourceManager.instance.getResource(resource);
        if (res == null) {
            res = PSResourceManager.instance.loadResource(new TileMapDescriptor(resource));
        }
        return res;
    };
    return TileMapDescriptor;
})();
var TileMapLayer = (function (_super) {
    __extends(TileMapLayer, _super);
    function TileMapLayer() {
        _super.call(this);
        this.extraProperties = {};
    }
    TileMapLayer.prototype.createVAO = function () {
        var gl = PSGame.CURRENT_GAME.renderSupport.renderContext;
        this.m_TotalNumElements = this.mapWidth * this.mapHeight * 6;
        this.m_VertexData = new Float32Array(this.m_TotalNumElements * 4); // 4 x GL_FLOAT per element
        var output = {}; //store the quad coords output
        for (var cellIndex = 0; cellIndex < this.cells.length; ++cellIndex) {
            var cell = this.cells[cellIndex];
            this.tileset.getQuadCoords(cell.id, output);
            var startY = Math.floor(cellIndex / this.mapWidth);
            var startX = cellIndex - (startY * this.mapWidth);
            startX *= this.tileWidth;
            startY *= this.tileHeight;
            var data = new Float32Array([
                //	POS  	  						UV
                startX, startY, output.upperLeft.x, output.upperLeft.y,
                startX + this.tileWidth, startY, output.upperRight.x, output.upperRight.y,
                startX + this.tileWidth, startY + this.tileHeight, output.bottomRight.x, output.bottomRight.y,
                startX, startY, output.upperLeft.x, output.upperLeft.y,
                startX + this.tileWidth, startY + this.tileHeight, output.bottomRight.x, output.bottomRight.y,
                startX, startY + this.tileHeight, output.bottomLeft.x, output.bottomLeft.y
            ]);
            var arrayStart = cellIndex * data.length; //TODO calc array start
            for (var dataIndex = 0; dataIndex < data.length; ++dataIndex) {
                this.m_VertexData[arrayStart + dataIndex] = data[dataIndex];
            }
        }
        var vao = gl['ext'].vao.createVertexArrayOES();
        gl['ext'].vao.bindVertexArrayOES(vao);
        var vbo = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
        gl.bufferData(gl.ARRAY_BUFFER, this.m_VertexData, gl.STATIC_DRAW);
        gl.vertexAttribPointer(PSBuiltInAttributes.POSITION.location, 2, gl.FLOAT, false, 16, 0);
        gl.vertexAttribPointer(PSBuiltInAttributes.TEXCOORD.location, 2, gl.FLOAT, false, 16, 8);
        gl.enableVertexAttribArray(PSBuiltInAttributes.POSITION.location);
        gl.enableVertexAttribArray(PSBuiltInAttributes.TEXCOORD.location);
        //TODO: disable other vertex attribues if needed
        gl['ext'].vao.bindVertexArrayOES(null);
        this.vao = vao;
    };
    TileMapLayer.prototype.setupGenerated = function () {
        this.vao = PSBuiltInGfx.PS_DEFAULT_QUAD_VAO;
        this.m_TotalNumElements = 6;
    };
    TileMapLayer.prototype.render = function (support, gl) {
        var material = PSBuiltInGfx.PS_DEFAULT_MATERIAL;
        var meshVAO = this.vao;
        material.uSampler = this.texture;
        material.uVPMatrix = support.m_CurrentVPMatrix;
        material.uModelMatrix = support.m_CurrentModelMatrix;
        material.uColor = support.m_CurrentColor;
        material.uTMatrix = this.texture.textureMatrix;
        material.apply(support);
        gl['ext'].vao.bindVertexArrayOES(meshVAO);
        gl.drawArrays(gl.TRIANGLES, 0, this.m_TotalNumElements);
        gl['ext'].vao.bindVertexArrayOES(null);
    };
    return TileMapLayer;
})(PSEntity);
var TileSetDescriptor = (function () {
    function TileSetDescriptor(texture) {
        this.m_Texture = texture;
    }
    Object.defineProperty(TileSetDescriptor.prototype, "tilesPerRow", {
        get: function () {
            return Math.floor((this.m_Texture.sourceWidth - (this.margin * 2)) / this.tileWidth);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TileSetDescriptor.prototype, "texture", {
        get: function () {
            return this.m_Texture;
        },
        enumerable: true,
        configurable: true
    });
    TileSetDescriptor.prototype.getCoords = function (index) {
        var tilesPerRow = this.tilesPerRow;
        var r = Math.floor((index - 1) / tilesPerRow);
        var c = (index - 1) - (r * tilesPerRow);
        return { column: c, row: r };
    };
    TileSetDescriptor.prototype.getTexelCoords = function (index) {
        var mapCoords = this.getCoords(index);
        var mapCoordX = this.margin + (mapCoords.column * (this.tileWidth + this.spacing));
        var mapCoordY = this.margin + (mapCoords.row * (this.tileHeight + this.spacing));
        return { x: mapCoordX, y: mapCoordY };
    };
    TileSetDescriptor.prototype.getQuadCoords = function (index, output) {
        output = output || {};
        var texelCoords = this.getTexelCoords(index);
        var addedMargin = 0.0;
        var leftX = (texelCoords.x + addedMargin) / this.m_Texture.sourceWidth;
        var rightX = (texelCoords.x + this.tileWidth - addedMargin) / this.m_Texture.sourceWidth;
        var upperY = (texelCoords.y + addedMargin) / this.m_Texture.sourceHeight;
        var bottomY = (texelCoords.y + this.tileHeight - addedMargin) / this.m_Texture.sourceHeight;
        output.upperLeft = {};
        output.upperRight = {};
        output.bottomLeft = {};
        output.bottomRight = {};
        output.upperLeft.x = leftX;
        output.upperLeft.y = upperY;
        output.upperRight.x = rightX;
        output.upperRight.y = upperY;
        output.bottomLeft.x = leftX;
        output.bottomLeft.y = bottomY;
        output.bottomRight.x = rightX;
        output.bottomRight.y = bottomY;
        return output;
    };
    TileSetDescriptor.prototype.isLoaded = function () {
        return this.m_Texture.isLoaded();
    };
    return TileSetDescriptor;
})();
var PSClock = (function () {
    function PSClock() {
        this.m_RealDeltaTime = 0.0;
        this.m_TimeScale = 1.0;
        this.m_LastFrameTimestamp = 0.0;
        this.m_StartFrameTimestamp = 0.0;
        this.m_FPS = 0.0;
        this.m_RealFPS = 0.0;
    }
    Object.defineProperty(PSClock, "instance", {
        get: function () {
            if (PSClock.s_Instance == null)
                PSClock.s_Instance = new PSClock;
            return PSClock.s_Instance;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSClock.prototype, "time", {
        get: function () { return new Date().getTime() / 1000.0; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSClock.prototype, "timeInMiliseconds", {
        get: function () { return new Date().getTime(); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSClock.prototype, "deltaTime", {
        get: function () { return this.m_RealDeltaTime * this.m_TimeScale; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSClock.prototype, "timeScale", {
        get: function () { return this.m_TimeScale; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSClock.prototype, "fps", {
        get: function () { return this.m_FPS; },
        enumerable: true,
        configurable: true
    });
    //get realfps() : number { return this.m_RealFPS; }
    PSClock.prototype.init = function () {
        this.m_LastFrameTimestamp = new Date().getTime();
    };
    PSClock.prototype.startFrame = function () {
        this.m_StartFrameTimestamp = new Date().getTime();
        this.m_RealDeltaTime = (this.m_StartFrameTimestamp - this.m_LastFrameTimestamp) / 1000.0;
        this.m_RealFPS = 1.0 / this.m_RealDeltaTime;
        this.m_RealDeltaTime = Math.min(this.m_RealDeltaTime, 0.033);
        //this.m_RealDeltaTime = Math.max(this.m_RealDeltaTime, 0.016);
        this.m_FPS = 1.0 / this.m_RealDeltaTime;
    };
    PSClock.prototype.endFrame = function () {
        this.m_LastFrameTimestamp = this.m_StartFrameTimestamp;
    };
    PSClock.s_Instance = null;
    return PSClock;
})();
/// <reference path="../graphics/RenderSupport.ts"/>
/// <reference path="../display/Entity.ts"/>
/// <reference path="../system/ResourceManager.ts"/>
/** START requestAnimationFrame SETUP */
if (window.requestAnimationFrame == null) {
    if (window['mozRequestAnimationFrame'] != null) {
        /** Firefox */
        window.requestAnimationFrame = window['mozRequestAnimationFrame'];
    }
    else if (window['msRequestAnimationFrame'] != null) {
        /** Firefox */
        window.requestAnimationFrame = window['msRequestAnimationFrame'];
    }
    else if (window['webkitRequestAnimationFrame'] != null) {
        /** Firefox */
        window.requestAnimationFrame = window['webkitRequestAnimationFrame'];
    }
}
/** END requestAnimationFrame SETUP */
var PSGame = (function () {
    function PSGame() {
        this.m_TempViewMatrix = new PSMatrix();
        this.m_CurrentTouchEvent = new PSTouchEvent('');
        this.m_PostProcessPipeline = null;
        PSGame.CURRENT_GAME = this;
        this.m_Clock = new PSClock();
        this.m_ClearColor = new PSColor(0.0, 0.0, 0.0, 1.0);
        this.m_ResourceManager = PSResourceManager.instance;
        this.m_Input = PSInput.instance;
    }
    PSGame.LOOP_FNC = function () {
        PSGame.CURRENT_GAME.requestFrame();
        window.requestAnimationFrame(PSGame.LOOP_FNC);
    };
    Object.defineProperty(PSGame.prototype, "stage", {
        get: function () { return this.m_Stage; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSGame.prototype, "currentCamera", {
        get: function () { return this.m_CurrentCamera; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSGame.prototype, "canvas", {
        get: function () { return this.m_Canvas; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSGame.prototype, "renderSupport", {
        get: function () { return this.m_RenderSupport; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSGame.prototype, "clock", {
        get: function () { return this.m_Clock; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSGame.prototype, "input", {
        get: function () { return this.m_Input; },
        enumerable: true,
        configurable: true
    });
    /** virtual */
    PSGame.prototype.init = function () {
        this.setupCanvas('DefaultCanvas');
    };
    /** virtual */
    PSGame.prototype.loadResources = function (manager) { };
    /** virtual */
    PSGame.prototype.setupScene = function () { };
    /** virtual */
    PSGame.prototype.update = function (clock) {
        this.m_Input.updateInput(this.m_Stage, this.m_CurrentTouchEvent);
        this.m_ResourceManager.updateLoads();
        this.m_Stage.innerUpdate(clock);
    };
    /** virtual */
    PSGame.prototype.render = function (support, gl) {
        if (this.m_PostProcessPipeline != null) {
            this.m_PostProcessPipeline.bind(support, gl);
        }
        gl.viewport(0, 0, this.m_Canvas.width, this.m_Canvas.height);
        gl.clearColor(this.m_ClearColor.red, this.m_ClearColor.green, this.m_ClearColor.blue, this.m_ClearColor.alpha);
        gl.clear(gl.COLOR_BUFFER_BIT);
        this.m_CurrentCamera.getTransformationMatrix(this.m_TempViewMatrix);
        support.viewMatrix = this.m_TempViewMatrix.invert();
        this.m_Stage.innerRender(support, gl);
        if (this.m_PostProcessPipeline != null) {
            //gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
            //gl.viewport(0, 0, this.m_PostProcessPipeline.m_FBWidth, this.m_PostProcessPipeline.m_FBHeight);
            this.m_PostProcessPipeline.render(support, gl);
        }
    };
    PSGame.prototype.setupCanvas = function (canvasId, w, h) {
        if (canvasId === void 0) { canvasId = ''; }
        if (w === void 0) { w = 0; }
        if (h === void 0) { h = 0; }
        if (canvasId == '' || document.getElementById(canvasId) == null) {
            var canvasDiv = document.createElement('div');
            canvasDiv.style.textAlign = 'center';
            canvasDiv.id = canvasId + 'Div';
            this.m_Canvas = document.createElement('canvas');
            this.m_Canvas.width = (w > 0) ? w : 800;
            this.m_Canvas.height = (h > 0) ? h : 600;
            this.m_Canvas.id = canvasId;
            canvasDiv.appendChild(this.m_Canvas);
            document.body.appendChild(canvasDiv);
        }
        else {
            this.m_Canvas = document.getElementById(canvasId);
            this.m_Canvas.width = (w > 0) ? w : this.m_Canvas.width;
            this.m_Canvas.height = (h > 0) ? h : this.m_Canvas.height;
        }
        this.m_RenderSupport = new PSRenderSupport();
        this.m_RenderSupport.setup(this.m_Canvas);
        PSBuiltInGfx.load();
        this.m_Input.init();
    };
    PSGame.prototype.requestFrame = function () {
        this.m_Clock.startFrame();
        this.update(this.m_Clock);
        this.render(this.m_RenderSupport, this.m_RenderSupport.m_GLContext);
        this.m_Clock.endFrame();
    };
    PSGame.prototype.run = function () {
        PSPool.createPool(PSPoint);
        PSPool.createPool(PSMatrix);
        PSPool.createPool(PSContextState, 30);
        this.init();
        this.m_Stage = new PSEntity();
        this.m_CurrentCamera = new PSCamera();
        this.loadResources(PSResourceManager.instance);
        PSGame.runOnceLoaded(this);
        //this.setupScene();
        //this.startGameLoop();
    };
    PSGame.runOnceLoaded = function (_game) {
        if (PSResourceManager.instance.loadingResources) {
            setTimeout(PSGame.runOnceLoaded, 300, _game);
        }
        else {
            _game.setupScene();
            _game.startGameLoop();
        }
    };
    PSGame.prototype.startGameLoop = function () {
        this.m_Clock.init();
        PSGame.LOOP_FNC();
    };
    PSGame.CURRENT_GAME = null;
    return PSGame;
})();
;
var PSKeyMap = (function () {
    function PSKeyMap() {
    }
    PSKeyMap.TAB = 9;
    PSKeyMap.ENTER = 13;
    PSKeyMap.SHIFT = 16;
    PSKeyMap.CTRL = 17;
    PSKeyMap.ALT = 18;
    PSKeyMap.SPACE = 32;
    PSKeyMap.ARROW_LEFT = 37;
    PSKeyMap.ARROW_UP = 38;
    PSKeyMap.ARROW_RIGHT = 39;
    PSKeyMap.ARROW_DOWN = 40;
    PSKeyMap.DOT = 46;
    PSKeyMap.ZERO = 48;
    PSKeyMap.ONE = 49;
    PSKeyMap.TWO = 50;
    PSKeyMap.THREE = 51;
    PSKeyMap.FOUR = 52;
    PSKeyMap.FIVE = 53;
    PSKeyMap.SIX = 54;
    PSKeyMap.SEVEN = 55;
    PSKeyMap.EIGHT = 56;
    PSKeyMap.NINE = 57;
    PSKeyMap.A = 65;
    PSKeyMap.B = 66;
    PSKeyMap.C = 67;
    PSKeyMap.D = 68;
    PSKeyMap.E = 69;
    PSKeyMap.F = 70;
    PSKeyMap.G = 71;
    PSKeyMap.H = 72;
    PSKeyMap.I = 73;
    PSKeyMap.J = 74;
    PSKeyMap.K = 75;
    PSKeyMap.L = 76;
    PSKeyMap.M = 77;
    PSKeyMap.N = 78;
    PSKeyMap.O = 79;
    PSKeyMap.P = 80;
    PSKeyMap.Q = 81;
    PSKeyMap.R = 82;
    PSKeyMap.S = 83;
    PSKeyMap.T = 84;
    PSKeyMap.U = 85;
    PSKeyMap.V = 86;
    PSKeyMap.W = 87;
    PSKeyMap.X = 88;
    PSKeyMap.Y = 89;
    PSKeyMap.Z = 90;
    return PSKeyMap;
})();
var PSInput = (function () {
    function PSInput() {
        this.m_MobileDevice = false;
        this.m_PressedKeys = new Array(256);
        this.m_JustReleasedKeys = new Array(256);
        this.m_JustPressedKeys = new Array(256);
        //frame to frame state
        this.m_EventMouseLocation = new PSPoint(-50, -50);
        this.m_MouseLocation = new PSPoint(-50, -50);
        this.m_MousePreviousLocation = new PSPoint(-50, -50);
        this.m_MouseDown = false;
        this.m_MousePreviousDown = false;
        //browser events state
        this.m_EventMouseDown = false;
        this.m_HaveFocus = true;
    }
    Object.defineProperty(PSInput, "instance", {
        get: function () {
            if (PSInput.s_Instance == null)
                PSInput.s_Instance = new PSInput;
            return PSInput.s_Instance;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput, "mouseX", {
        get: function () { return PSInput.s_Instance.m_MouseLocation.x; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput, "mouseY", {
        get: function () { return PSInput.s_Instance.m_MouseLocation.y; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput, "mousePreviousX", {
        get: function () { return PSInput.s_Instance.m_MousePreviousLocation.x; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput, "mousePreviousY", {
        get: function () { return PSInput.s_Instance.m_MousePreviousLocation.y; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput, "mouseDown", {
        get: function () { return PSInput.s_Instance.m_MouseDown; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput, "mousePreviousDown", {
        get: function () { return PSInput.s_Instance.m_MousePreviousDown; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput, "mouseClick", {
        get: function () { return PSInput.s_Instance.m_MouseDown && !PSInput.s_Instance.m_MousePreviousDown; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput, "mouseRelease", {
        get: function () { return !PSInput.s_Instance.m_MouseDown && PSInput.s_Instance.m_MousePreviousDown; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput, "mouseMoving", {
        get: function () {
            return PSInput.s_Instance.m_MouseLocation.x != PSInput.s_Instance.m_MousePreviousLocation.x ||
                PSInput.s_Instance.m_MouseLocation.y != PSInput.s_Instance.m_MousePreviousLocation.y;
        },
        enumerable: true,
        configurable: true
    });
    PSInput.isKeyPressed = function (keycode) {
        return PSInput.s_Instance.m_PressedKeys[keycode];
    };
    PSInput.isKeyJustPressed = function (keycode) {
        return PSInput.s_Instance.m_JustPressedKeys[keycode] > 0;
    };
    PSInput.isKeyJustReleased = function (keycode) {
        return PSInput.s_Instance.m_JustReleasedKeys[keycode] > 0;
    };
    PSInput.isMobileDevice = function () {
        return PSInput.s_Instance.m_MobileDevice;
    };
    Object.defineProperty(PSInput.prototype, "mouseX", {
        get: function () { return this.m_MouseLocation.x; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput.prototype, "mouseY", {
        get: function () { return this.m_MouseLocation.y; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput.prototype, "mousePreviousX", {
        get: function () { return this.m_MousePreviousLocation.x; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput.prototype, "mousePreviousY", {
        get: function () { return this.m_MousePreviousLocation.y; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput.prototype, "mouseDown", {
        get: function () { return this.m_MouseDown; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput.prototype, "mousePreviousDown", {
        get: function () { return this.m_MousePreviousDown; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput.prototype, "mouseClicked", {
        get: function () { return this.m_MouseDown && !this.m_MousePreviousDown; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput.prototype, "mouseReleased", {
        get: function () { return !this.m_MouseDown && this.m_MousePreviousDown; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSInput.prototype, "mouseMoving", {
        get: function () {
            return this.m_MouseLocation.x != this.m_MousePreviousLocation.x ||
                this.m_MouseLocation.y != this.m_MousePreviousLocation.y;
        },
        enumerable: true,
        configurable: true
    });
    PSInput.prototype.isKeyPressed = function (keycode) {
        return this.m_PressedKeys[keycode];
    };
    PSInput.prototype.isMobileDevice = function () {
        return this.m_MobileDevice;
    };
    PSInput.prototype.init = function () {
        var game = PSGame.CURRENT_GAME;
        var canvas = game.canvas;
        for (var i = 0; i < 256; ++i) {
            this.m_PressedKeys[i] = false;
            this.m_JustReleasedKeys[i] = 0;
            this.m_JustPressedKeys[i] = 0;
        }
        //check mobile devices
        if ((navigator.userAgent.match(/Android/i)) ||
            (navigator.userAgent.match(/iPhone/i)) ||
            (navigator.userAgent.match(/iPod/i)) ||
            (navigator.userAgent.match(/iPad/i))) {
            this.m_MobileDevice = true;
            canvas.style['webkitTapHighlightColor'] = 'rgba(0,0,0,0)';
        }
        if (!this.m_MobileDevice) {
            if (window.navigator['msPointerEnabled']) {
                window.addEventListener('MSPointerDown', PSInput.windowMouseEventHandler, true);
            }
            else {
                window.addEventListener('mousedown', PSInput.windowMouseEventHandler, true);
            }
        }
        window.addEventListener('keydown', PSInput.handleEventCaller, true);
        window.addEventListener('keyup', PSInput.handleEventCaller, true);
        if (window.navigator['msPointerEnabled']) {
            canvas.addEventListener("MSPointerDown", PSInput.handleEventCaller, true);
            canvas.addEventListener("MSPointerUp", PSInput.handleEventCaller, true);
            canvas.addEventListener("MSPointerMove", PSInput.handleEventCaller, true);
        }
        else {
            canvas.addEventListener('mousedown', PSInput.handleEventCaller, true);
            canvas.addEventListener('mouseup', PSInput.handleEventCaller, true);
            canvas.addEventListener('mousemove', PSInput.handleEventCaller, true);
        }
        canvas.addEventListener("touchstart", PSInput.handleEventCaller, false);
        canvas.addEventListener("touchend", PSInput.handleEventCaller, false);
        canvas.addEventListener("touchcancel", PSInput.handleEventCaller, false);
        canvas.addEventListener("touchleave", PSInput.handleEventCaller, false);
        canvas.addEventListener("touchmove", PSInput.handleEventCaller, false);
    };
    PSInput.handleEventCaller = function (e) {
        e = e || window.event;
        return PSInput.s_Instance.handleEvent(e);
    };
    PSInput.windowMouseEventHandler = function (e) {
        e = e || window.event;
        var input = PSInput.s_Instance;
        if (((typeof e['toElement'] != 'undefined' && e['toElement'] != PSGame.CURRENT_GAME.canvas) ||
            (typeof e['target'] != 'undefined' && e['target'] != PSGame.CURRENT_GAME.canvas)) &&
            input.m_HaveFocus) {
            input.onFocusOut();
            input.m_HaveFocus = false;
        }
        else {
            input.m_HaveFocus = true;
        }
    };
    PSInput.prototype.updateInput = function (stage, touchEvent) {
        var touchEventType = null;
        if (this.mouseClicked) {
            touchEventType = PSTouchEvent.TOUCH_BEGIN;
        }
        else if (this.mouseReleased) {
            touchEventType = PSTouchEvent.TOUCH_END;
        }
        else if (this.mouseMoving) {
            touchEventType = PSTouchEvent.TOUCH_MOVE;
        }
        if (touchEventType != null) {
            touchEvent.resetValues(touchEventType);
            var stagePoint = PSPool.alloc(PSPoint);
            this.getTransformedMousePoint(stagePoint);
            touchEvent.stageX = stagePoint.x;
            touchEvent.stageY = stagePoint.y;
            touchEvent.localX = this.m_MouseLocation.x;
            touchEvent.localY = this.m_MouseLocation.y;
            touchEvent.displacementX = this.m_MouseLocation.x - this.m_MousePreviousLocation.x;
            touchEvent.displacementY = this.m_MouseLocation.y - this.m_MousePreviousLocation.y;
            //TODO: velocity and other things
            this.updateTraversal(stage, touchEvent);
            PSPool.free(stagePoint);
        }
        for (var i = 0; i < 256; ++i) {
            this.m_JustReleasedKeys[i] -= (this.m_JustReleasedKeys[i] > 0) ? 1 : 0;
            this.m_JustPressedKeys[i] -= (this.m_JustPressedKeys[i] > 0) ? 1 : 0;
        }
        this.m_MousePreviousLocation.setFrom(this.m_MouseLocation);
        this.m_MouseLocation.setFrom(this.m_EventMouseLocation);
        this.m_MousePreviousDown = this.m_MouseDown;
        this.m_MouseDown = this.m_EventMouseDown;
    };
    PSInput.prototype.updateTraversal = function (entity, touchEvent) {
        if (entity.visible && entity.m_Color.alpha > 0.0) {
            var numChildren = entity.numChildren;
            for (var i = numChildren - 1; i >= 0; --i) {
                var child = entity.getChildAt(i);
                if (child != null) {
                    this.updateTraversal(child, touchEvent);
                }
            }
            if (entity.captureInput && !touchEvent.captured) {
                entity.onNativeEvent(touchEvent);
            }
        }
    };
    PSInput.prototype.handleEvent = function (e) {
        e = e || window.event;
        var r = true;
        switch (e.type) {
            case 'mousedown':
            case 'touchstart':
            case "MSPointerDown":
                r = this.onMouseDown(e);
                break;
            case 'mouseup':
            case 'touchend':
            case 'touchcancel':
            case "MSPointerUp":
                r = this.onMouseUp(e);
                break;
            case 'keydown':
                r = this.onKeyDown(e);
                break;
            case 'keyup':
                r = this.onKeyUp(e);
                break;
            case 'mousemove':
            case 'touchmove':
            case "MSPointerMove":
                r = this.onMouseMove(e);
                break;
        }
        return r;
    };
    PSInput.prototype.getTouch = function (touchIndex, pEvent) {
        if (pEvent && pEvent.targetTouches)
            return pEvent.targetTouches[touchIndex] || null;
        /*if(window.event && window.event.targetTouches)
            return window.event.targetTouches[touchIndex] || false;*/
    };
    PSInput.prototype.onMouseDown = function (pe) {
        var event = this.m_MobileDevice ? this.getTouch(0, pe) : pe;
        if (event == null)
            return false;
        this.m_EventMouseLocation.x = event['offsetX'] || event['pageX'] - PSGame.CURRENT_GAME.canvas.offsetLeft;
        this.m_EventMouseLocation.y = event['offsetY'] || event['pageY'] - PSGame.CURRENT_GAME.canvas.offsetTop;
        this.onFocusIn();
        this.m_EventMouseDown = true;
        pe.preventDefault();
        pe.stopPropagation();
        //PSGame.CURRENT_GAME.canvas.addEventListener("touchmove", ss2d.Input.handleEventCaller, false);
        return false;
    };
    PSInput.prototype.onMouseUp = function (pe) {
        //var event = this.mMobileDevice ? this.getTouch(0, pe) : pe;
        //if((this.mMobileDevice && event) || !this.mMobileDevice){
        this.m_EventMouseDown = false;
        pe.preventDefault();
        pe.stopPropagation();
        //PSGame.CURRENT_GAME.canvas.removeEventListener("touchmove", ss2d.Input.handleEventCaller, false);
        //}
        return false;
    };
    PSInput.prototype.onKeyDown = function (event) {
        if (this.m_HaveFocus) {
            var keyCode = parseInt(event.keyCode);
            if (!this.m_PressedKeys[keyCode])
                this.m_JustPressedKeys[keyCode] = 2;
            this.m_PressedKeys[keyCode] = true;
            if (keyCode == 37 || keyCode == 38 || keyCode == 39 || keyCode == 40 || keyCode == 32) {
                event.preventDefault();
            }
            return false;
        }
        return true;
    };
    PSInput.prototype.onKeyUp = function (event) {
        this.m_PressedKeys[parseInt(event.keyCode)] = false;
        this.m_JustReleasedKeys[parseInt(event.keyCode)] = 2;
        return !this.m_HaveFocus;
    };
    PSInput.prototype.onMouseMove = function (pe) {
        var event = this.m_MobileDevice ? this.getTouch(0, pe) : pe;
        if (event) {
            this.m_EventMouseLocation.x = event['offsetX'] || event['pageX'] - PSGame.CURRENT_GAME.canvas.offsetLeft;
            this.m_EventMouseLocation.y = event['offsetY'] || event['pageY'] - PSGame.CURRENT_GAME.canvas.offsetTop;
        }
        return false;
    };
    PSInput.prototype.getTransformedMousePoint = function (targetPoint) {
        targetPoint = targetPoint || new PSPoint();
        var tempMatrix = PSPool.alloc(PSMatrix);
        PSGame.CURRENT_GAME.currentCamera.getTransformationMatrix(tempMatrix).invert().transformPoint(this.m_MouseLocation, targetPoint);
        PSPool.free(tempMatrix);
        return targetPoint;
    };
    PSInput.prototype.onFocusIn = function () {
        this.m_HaveFocus = true;
    };
    PSInput.prototype.onFocusOut = function () {
        this.m_HaveFocus = false;
        this.m_EventMouseDown = false;
        //if(this.mCleanKeysOnFocusOut)
        //{
        for (var i = 0; i < 256; ++i) {
            this.m_PressedKeys[i] = false;
            this.m_JustReleasedKeys[i] = 0;
            this.m_JustPressedKeys[i] = 0;
        }
        //}
    };
    PSInput.s_Instance = null;
    return PSInput;
})();
var PSScreen = (function () {
    function PSScreen() {
    }
    Object.defineProperty(PSScreen, "width", {
        get: function () { return PSGame.CURRENT_GAME.canvas.width; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSScreen, "height", {
        get: function () { return PSGame.CURRENT_GAME.canvas.height; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSScreen, "center", {
        get: function () {
            return new PSPoint(PSGame.CURRENT_GAME.canvas.width * 0.5, PSGame.CURRENT_GAME.canvas.height * 0.5);
        },
        enumerable: true,
        configurable: true
    });
    return PSScreen;
})();
/// <reference path="../display/Entity.ts"/>
var PSTimer = (function (_super) {
    __extends(PSTimer, _super);
    function PSTimer() {
        _super.apply(this, arguments);
        this.m_Duration = 0;
        this.m_TimeSinceStart = 0;
        this.m_Started = false;
    }
    Object.defineProperty(PSTimer.prototype, "duration", {
        get: function () { return this.m_Duration; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTimer.prototype, "timeSinceStart", {
        get: function () { return this.m_TimeSinceStart; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTimer.prototype, "started", {
        get: function () { return this.m_Started; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PSTimer.prototype, "finished", {
        get: function () { return this.m_Started && (this.m_TimeSinceStart >= this.m_Duration); },
        enumerable: true,
        configurable: true
    });
    PSTimer.prototype.start = function (duration) {
        this.m_Duration = duration;
    };
    PSTimer.prototype.resetValues = function () {
        this.m_Duration = 0;
        this.m_TimeSinceStart = 0;
        this.m_Started = false;
    };
    PSTimer.prototype.getDelta = function (easeFunc) {
        if (easeFunc === void 0) { easeFunc = PSEaseFuncions.lineal; }
        if (this.m_Started && this.m_Duration > 0) {
            return easeFunc(this.m_Duration, this.m_TimeSinceStart);
        }
        else {
            return 0;
        }
    };
    PSTimer.prototype.update = function (clock) {
        if (this.m_Started == false && this.m_Duration > 0) {
            this.m_Started = true;
        }
        if (this.m_Started) {
            if (this.m_TimeSinceStart < this.m_Duration) {
                this.m_TimeSinceStart += clock.deltaTime;
            }
            if (this.m_TimeSinceStart > this.m_Duration) {
                this.m_TimeSinceStart = this.m_Duration;
            }
        }
    };
    return PSTimer;
})(PSEntity);
/// <reference  path="../src/display/Camera.ts" />
/// <reference  path="../src/display/Entity.ts" />
/// <reference  path="../src/display/Event.ts" />
/// <reference  path="../src/display/EventDispatcher.ts" />
/// <reference  path="../src/display/EventListener.ts" />
/// <reference  path="../src/display/Sprite.ts" />
/// <reference  path="../src/display/TextSprite.ts" />
/// <reference  path="../src/display/TouchEvent.ts" />
/// <reference  path="../src/graphics/BuiltInGfx.ts" />
/// <reference  path="../src/graphics/Material.ts" />
/// <reference  path="../src/graphics/PostProcessPipeline.ts" />
/// <reference  path="../src/graphics/RenderSupport.ts" />
/// <reference  path="../src/graphics/ShaderProgram.ts" />
/// <reference  path="../src/graphics/ShaderSource.ts" />
/// <reference  path="../src/graphics/StaticMesh.ts" />
/// <reference  path="../src/graphics/Texture.ts" />
/// <reference  path="../src/interpolators/AlphaTo.ts" />
/// <reference  path="../src/interpolators/ColorTo.ts" />
/// <reference  path="../src/interpolators/Delay.ts" />
/// <reference  path="../src/interpolators/EaseFunctions.ts" />
/// <reference  path="../src/interpolators/ScaleTo.ts" />
/// <reference  path="../src/math/Color.ts" />
/// <reference  path="../src/math/Matrix.ts" />
/// <reference  path="../src/math/Point.ts" />
/// <reference  path="../src/math/Rectangle.ts" />
/// <reference  path="../src/math/Circle.ts" />
/// <reference  path="../src/math/Transform.ts" />
/// <reference  path="../src/plugins/Tiled/TileMap.ts" />
/// <reference  path="../src/plugins/Tiled/TileMapDescriptor.ts" />
/// <reference  path="../src/plugins/Tiled/TileMapLayer.ts" />
/// <reference  path="../src/plugins/Tiled/TileSetDescriptor.ts" />
/// <reference  path="../src/shared/stdlib.ts" />
/// <reference  path="../src/system/Clock.ts" />
/// <reference  path="../src/system/Game.ts" />
/// <reference  path="../src/system/Input.ts" />
/// <reference  path="../src/system/Memory.ts" />
/// <reference  path="../src/system/ResourceManager.ts" />
/// <reference  path="../src/system/Screen.ts" />
/// <reference  path="../src/system/Timer.ts" />
/// <reference path="../../../src/Poollos.ts"/>
var Monster = (function (_super) {
    __extends(Monster, _super);
    function Monster() {
        _super.call(this);
        this.m_Speed = new PSPoint(0, 0);
        this.m_MoveUpKeyCode = PSKeyMap.W;
        this.m_MoveDownKeyCode = PSKeyMap.S;
        this.m_MoveLeftKeyCode = PSKeyMap.A;
        this.m_MoveRightKeyCode = PSKeyMap.D;
        var sprite = new PSSprite(PSResourceManager.instance.getResource('./assets/monster.png'));
        //sprite.color.setValues(4.0, 4.0, 4.0, 1.0);
        //sprite.width = 64;
        //sprite.height = 64;
        this.addChild(sprite);
        this.m_Sprite = sprite;
        this.m_Speed.x = 0;
        this.m_Speed.y = 0;
        this.updateBounds(true);
        this.m_LimitTop = 50;
        this.m_LimitBottom = PSScreen.height - 50;
        this.m_LimitLeft = 50;
        this.m_LimitRight = PSScreen.width - 50;
        this.m_CaptureInput = true;
        this.addEventListener(PSTouchEvent.TOUCH_BEGIN, this);
        this.addEventListener(PSTouchEvent.TOUCH_END, this);
        this.transform.scale = 1;
        this.pivotX = this.m_Sprite.width * 0.5;
        this.pivotY = this.m_Sprite.height * 0.5;
        this.m_CircleCollider = new PSCircle(this.x, this.y, 60);
    }
    Monster.prototype.updateSpeed = function (clock) {
        var verticalPressed = false;
        var horizontalPressed = false;
        if (PSInput.isKeyPressed(this.m_MoveUpKeyCode)) {
            if (this.m_Speed.y > 0)
                this.m_Speed.y = 0;
            this.m_Speed.y += (this.m_Speed.y > -500) ? -3000 * clock.deltaTime : 0;
            verticalPressed = true;
        }
        if (PSInput.isKeyPressed(this.m_MoveDownKeyCode)) {
            if (this.m_Speed.y < 0)
                this.m_Speed.y = 0;
            this.m_Speed.y += (this.m_Speed.y < 500) ? 3000 * clock.deltaTime : 0;
            verticalPressed = true;
        }
        if (!verticalPressed) {
            if (Math.abs(this.m_Speed.y) <= 10)
                this.m_Speed.y = 0;
            if (this.m_Speed.y < 0) {
                this.m_Speed.y += 3000 * clock.deltaTime;
                if (this.m_Speed.y > 0) {
                    this.m_Speed.y = 0;
                }
            }
            if (this.m_Speed.y > 0) {
                this.m_Speed.y -= 3000 * clock.deltaTime;
                if (this.m_Speed.y < 0) {
                    this.m_Speed.y = 0;
                }
            }
        }
        if (PSInput.isKeyPressed(this.m_MoveLeftKeyCode)) {
            if (this.m_Speed.x > 0)
                this.m_Speed.x = 0;
            this.m_Speed.x += (this.m_Speed.x > -500) ? -3000 * clock.deltaTime : 0;
            horizontalPressed = true;
        }
        if (PSInput.isKeyPressed(this.m_MoveRightKeyCode)) {
            if (this.m_Speed.x < 0)
                this.m_Speed.x = 0;
            this.m_Speed.x += (this.m_Speed.x < 500) ? 3000 * clock.deltaTime : 0;
            horizontalPressed = true;
        }
        if (!horizontalPressed) {
            if (Math.abs(this.m_Speed.x) <= 10)
                this.m_Speed.x = 0;
            if (this.m_Speed.x < 0) {
                this.m_Speed.x += 3000 * clock.deltaTime;
                if (this.m_Speed.x > 0) {
                    this.m_Speed.x = 0;
                }
            }
            if (this.m_Speed.x > 0) {
                this.m_Speed.x -= 3000 * clock.deltaTime;
                if (this.m_Speed.x < 0) {
                    this.m_Speed.x = 0;
                }
            }
        }
    };
    Monster.prototype.updateLocation = function (clock) {
        this.y += this.m_Speed.y * clock.deltaTime;
        this.x += this.m_Speed.x * clock.deltaTime;
        if (this.y < this.m_LimitTop)
            this.y = this.m_LimitTop;
        if (this.y > this.m_LimitBottom)
            this.y = this.m_LimitBottom;
        if (this.x < this.m_LimitLeft)
            this.x = this.m_LimitLeft;
        if (this.x > this.m_LimitRight)
            this.x = this.m_LimitRight;
        this.m_CircleCollider.updateLocation(this.x, this.y);
    };
    Monster.prototype.update = function (clock) {
        this.updateSpeed(clock);
        this.updateLocation(clock);
    };
    Monster.prototype.onEvent = function (type, event) {
    };
    return Monster;
})(PSEntity);
var Globals;
(function (Globals) {
    Globals.SCROLL_SPEED = 50;
})(Globals || (Globals = {}));
;
/// <reference path="../../../src/Poollos.ts"/>
var Bullet = (function (_super) {
    __extends(Bullet, _super);
    function Bullet(texturePath, _locX, _locY, _lifetime, _dirX, _dirY) {
        _super.call(this, texturePath);
        this.x = _locX;
        this.y = _locY;
        this.lifetime = _lifetime;
        this.directionX = _dirX;
        this.directionY = _dirY;
    }
    Bullet.prototype.setupCollider = function (radius) {
        this.circleCollider = new PSCircle(this.x, this.y, radius);
    };
    Bullet.prototype.update = function (clock) {
        _super.prototype.update.call(this, clock);
        this.circleCollider.updateLocation(this.x, this.y);
    };
    return Bullet;
})(PSSprite);
;
var SmallBullet = (function (_super) {
    __extends(SmallBullet, _super);
    function SmallBullet(_locX, _locY, _lifetime, _dirX, _dirY) {
        _super.call(this, './assets/smallbullet.png', _locX, _locY, _lifetime, _dirX, _dirY);
        this.setupCollider(this.m_Texture.width);
    }
    return SmallBullet;
})(Bullet);
;
var ProjectileGenerator = (function (_super) {
    __extends(ProjectileGenerator, _super);
    function ProjectileGenerator() {
        _super.apply(this, arguments);
    }
    return ProjectileGenerator;
})(PSEntity);
;
/// <reference path="../../../src/Poollos.ts"/>
/// <reference path="./Monster.ts"/>
/// <reference path="./Globals.ts"/>
/// <reference path="./ProjectileGenerator.ts"/>
var Game = (function (_super) {
    __extends(Game, _super);
    function Game() {
        _super.apply(this, arguments);
        this.m_PointsCount = 0;
        this.m_SkyRepeats = 1;
        this.m_SkyPan = 0;
    }
    Game.prototype.init = function () {
        this.setupCanvas('PongCanvas');
        //this.m_PostProcessPipeline = new PSPostProcessPipeline(PSBuiltInGfx.PS_GLOW_EFFECT);
        //this.m_PostProcessPipeline.init(this.m_RenderSupport, this.m_RenderSupport.m_GLContext, PSScreen.width, PSScreen.height, 2);
    };
    Game.prototype.loadResources = function (manager) {
        PSTexture.TEXTURE_FILTER_STYLE = ETexFilterStyle.TS_PIXELART;
        manager.verbose = true;
        var textures = [];
        textures.push(['./assets/monster.png', false]);
        textures.push(['./assets/smallbullet.png', false]);
        textures.push(['./assets/mediumbullet.png', false]);
        textures.push(['./assets/skyline.png', true]);
        for (var i = 0; i < textures.length; i++) {
            manager.loadResource(new PSTexture(textures[i][0], textures[i][1]));
        }
    };
    Game.prototype.setupScene = function () {
        this.m_ClearColor.setValues(0.6, 0.6, 0.9, 1.0);
        this.m_Skyline = new PSSprite('./assets/skyline.png');
        this.m_Skyline.x = 0;
        this.m_Skyline.y = PSScreen.height - this.m_Skyline.height;
        this.m_SkyRepeats = PSScreen.width / this.m_Skyline.width;
        this.m_Skyline.width = PSScreen.width;
        this.m_Skyline.setTextureTransform(this.m_SkyPan, 0, this.m_SkyRepeats, 1, 0);
        this.stage.addChild(this.m_Skyline);
        this.m_Monster = new Monster();
        this.m_Monster.x = 80;
        this.m_Monster.y = 80;
        this.stage.addChild(this.m_Monster);
        this.m_PointsText = new PSTextSprite('0', 42);
        this.m_PointsText.color.setValues(1.0, 1.0, 1.0, 1.0);
        this.m_PointsText.compose();
        this.m_PointsText.pivotX = 0.5;
        this.m_PointsText.x = PSScreen.width * 0.5;
        this.m_PointsText.y = 20;
        this.stage.addChild(this.m_PointsText);
    };
    Game.prototype.update = function (clock) {
        _super.prototype.update.call(this, clock);
        this.m_PointsCount += clock.deltaTime * 8;
        this.m_PointsText.setText(Math.floor(this.m_PointsCount) + '');
        this.m_SkyPan += clock.deltaTime * 0.08;
        this.m_Skyline.setTextureTransform(this.m_SkyPan, 0, this.m_SkyRepeats, 1, 0);
    };
    Game.prototype.render = function (support, gl) {
        _super.prototype.render.call(this, support, gl);
    };
    Game.prototype.onEvent = function (type, event) {
        //console.log('Event received ', type, event);
    };
    return Game;
})(PSGame);
;
