var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CTFGame = (function (_super) {
    __extends(CTFGame, _super);
    function CTFGame() {
        _super.apply(this, arguments);
    }
    CTFGame.prototype.init = function () {
        this.setupCanvas('CTFCanvas', 1024, 640);
    };

    CTFGame.prototype.loadResources = function (manager) {
        manager.verbose = true;
        this.m_TileMapAsset = TileMapDescriptor.getTileMapResource('assets/ctf_desert_map_small.json');
    };

    CTFGame.prototype.setupScene = function () {
        //loading scene
        this.m_ClearColor.setValues(1.0, 1.0, 0.2, 1.0);
        this.m_SceneSetupDone = false;
    };

    CTFGame.prototype.setupMainScene = function () {
        //real main scene
        this.m_Scenario = new PSEntity();
        this.m_TileMap = new CTFTileMap(this.m_TileMapAsset);
        this.m_Scenario.addChild(this.m_TileMap);
        this.m_SceneSetupDone = true;
        var spritetest = new PSSprite('assets/tmw_desert_spacing.png');

        //this.m_Scenario.addChild(spritetest);
        this.stage.addChild(this.m_Scenario);

        var eventReceiver = new PSEntity();
        this.stage.addChild(eventReceiver);
        eventReceiver.addEventListener(PSTouchEvent.TOUCH_BEGIN, this);
        eventReceiver.addEventListener(PSTouchEvent.TOUCH_MOVE, this);
        eventReceiver.addEventListener(PSTouchEvent.TOUCH_END, this);
        eventReceiver.width = PSScreen.width;
        eventReceiver.height = PSScreen.height;
        eventReceiver.captureInput = true;

        this.m_SceneSetupDone = true;
    };

    CTFGame.prototype.update = function (clock) {
        _super.prototype.update.call(this, clock);

        if (!this.m_SceneSetupDone && PSResourceManager.instance.resourceCountToLoad == 0) {
            this.setupMainScene();
            return;
        }

        if (PSInput.isKeyPressed(PSKeyMap.A)) {
            this.m_Scenario.x += 200 * clock.deltaTime;
        }

        if (PSInput.isKeyPressed(PSKeyMap.D)) {
            this.m_Scenario.x -= 200 * clock.deltaTime;
        }

        if (PSInput.isKeyPressed(PSKeyMap.W)) {
            this.m_Scenario.y += 200 * clock.deltaTime;
        }

        if (PSInput.isKeyPressed(PSKeyMap.S)) {
            this.m_Scenario.y -= 200 * clock.deltaTime;
        }

        if (PSInput.isKeyJustPressed(PSKeyMap.R)) {
            console.log('Zoom in');
            if (this.m_Scenario.transform.scaleX < 2) {
                this.m_Scenario.transform.scaleX += 0.25;
                this.m_Scenario.transform.scaleY += 0.25;
            }
        }

        if (PSInput.isKeyJustPressed(PSKeyMap.F)) {
            console.log('Zoom out');
            if (this.m_Scenario.transform.scaleX > 0.5) {
                this.m_Scenario.transform.scaleX -= 0.25;
                this.m_Scenario.transform.scaleY -= 0.25;
            }
        }

        if (this.m_Scenario != null) {
            this.checkScenarioLocation();
        }
    };

    CTFGame.prototype.onEvent = function (type, event) {
        //console.log('Event received ', type, event);
        if (type == PSTouchEvent.TOUCH_BEGIN) {
        }

        if (type == PSTouchEvent.TOUCH_MOVE) {
            var touchEvent = event;
            this.m_Scenario.x += touchEvent.displacementX;
            this.m_Scenario.y += touchEvent.displacementY;

            this.checkScenarioLocation();
        }

        if (type == PSTouchEvent.TOUCH_END) {
        }
    };

    CTFGame.prototype.checkScenarioLocation = function () {
        if (this.m_Scenario.x < (-this.m_TileMap.mapWidth * this.m_Scenario.transform.scaleX) + PSScreen.width)
            this.m_Scenario.x = (-this.m_TileMap.mapWidth * this.m_Scenario.transform.scaleX) + PSScreen.width;

        if (this.m_Scenario.y < (-this.m_TileMap.mapHeight * this.m_Scenario.transform.scaleY) + PSScreen.height)
            this.m_Scenario.y = (-this.m_TileMap.mapHeight * this.m_Scenario.transform.scaleY) + PSScreen.height;

        if (this.m_Scenario.x > 0)
            this.m_Scenario.x = 0;

        if (this.m_Scenario.y > 0)
            this.m_Scenario.y = 0;
    };

    CTFGame.prototype.render = function (support, gl) {
        _super.prototype.render.call(this, support, gl);
    };
    return CTFGame;
})(PSGame);
;
var CTFTileMap = (function (_super) {
    __extends(CTFTileMap, _super);
    function CTFTileMap() {
        _super.apply(this, arguments);
    }
    CTFTileMap.prototype.setup = function () {
        _super.prototype.setup.call(this);

        if (this.m_Parsed && !this.m_CTFParsed) {
            this.m_Layers[0];

            //walkable 9, 33, 29
            //walkable extra cost: 3,4,11,12,5,6,7,13,14,15,21,22,23,46,47
            this.m_MainLayer = this.m_Layers[0];
            var walkableCells = this.m_MainLayer.extraProperties['walkable'].split(',');
            var walkableCellsPlus = this.m_MainLayer.extraProperties['walkable_extra_cost'].split(',');
            this.m_CTFParsed = true;
        }
    };

    CTFTileMap.prototype.getCellCostByIndex = function (cellIndex) {
        return 1;
    };

    CTFTileMap.prototype.getCellCostById = function (id) {
        return 1;
    };
    return CTFTileMap;
})(TileMap);
