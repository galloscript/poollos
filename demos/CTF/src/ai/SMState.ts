
class SMState
{
	constructor(ownerMachine : StateMachine)
	{
		this.m_OwnerMachine = ownerMachine;
	}

	public update(time : PSClock) : void
	{
		for(var transitionIndex in this.m_Transitions)
		{
			var transition = this.m_Transitions[transitionIndex];
			if(transition.evaluate(time : PSClock))
			{
				this.m_OwnerMachine.changeState(transition);
			}
		}
	}

	public addTransition(transition : StateMachineTransitions) : void
	{
		if(m_Transitions.indexOf(transition) == -1)
		{
			m_Transitions.push(transition);
		}
	}

	private m_Transitions : Array<StateMachineTransitions>;
	private m_OwnerMachine : StateMachine;

}