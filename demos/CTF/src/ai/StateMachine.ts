
class StateMachine extends PSEntity
{

	public addState(state : SMState) : void
	{
		if(m_States.indexOf(state) == -1)
		{
			m_States.push(state);
		}
	}

	private m_States : Array<SMState> = [];

};