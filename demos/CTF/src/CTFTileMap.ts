
class CTFTileMap extends TileMap
{


	setup() : void
	{
		super.setup();

		if(this.m_Parsed && !this.m_CTFParsed)
		{
			this.m_Layers[0];

			//walkable 9, 33, 29
			//walkable extra cost: 3,4,11,12,5,6,7,13,14,15,21,22,23,46,47

			this.m_MainLayer = this.m_Layers[0];
			var walkableCells = this.m_MainLayer.extraProperties['walkable'].split(',');
			var walkableCellsPlus = this.m_MainLayer.extraProperties['walkable_extra_cost'].split(',');
			this.m_CTFParsed = true;

		}
	}

	getCellCostByIndex(cellIndex : number) : number
	{
		return 1;
	}

	getCellCostById(id : number) : number
	{
		return 1;
	}

	public m_WalkableCells : Array<number>
	public m_WalkableExtraCostCells : Array<number>
	private m_CTFParsed : boolean;
	private m_MainLayer : TileMapLayer;
}