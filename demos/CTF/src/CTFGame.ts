/// <reference path="../../../src/Poollos.ts"/>
/// <reference path="./CTFTileMap.ts"/>

class CTFGame extends PSGame
{
	init() : void
	{
		this.setupCanvas('CTFCanvas', 1024, 640);
	}

	loadResources(manager : PSResourceManager) : void
	{
		manager.verbose = true;
		this.m_TileMapAsset = TileMapDescriptor.getTileMapResource('assets/ctf_desert_map_small.json');
	}

	setupScene() : void
	{
		//loading scene
		this.m_ClearColor.setValues(1.0, 1.0, 0.2, 1.0);
		this.m_SceneSetupDone = false;
		
	}	

	setupMainScene() : void
	{
		//real main scene
		this.m_Scenario = new PSEntity();
		this.m_TileMap = new CTFTileMap(this.m_TileMapAsset);
		this.m_Scenario.addChild(this.m_TileMap);
		this.m_SceneSetupDone = true;
		var spritetest = new PSSprite('assets/tmw_desert_spacing.png');
		//this.m_Scenario.addChild(spritetest);
		this.stage.addChild(this.m_Scenario);

		var eventReceiver = new PSEntity();
		this.stage.addChild(eventReceiver);
		eventReceiver.addEventListener(PSTouchEvent.TOUCH_BEGIN, this);
		eventReceiver.addEventListener(PSTouchEvent.TOUCH_MOVE, this);
		eventReceiver.addEventListener(PSTouchEvent.TOUCH_END, this);
		eventReceiver.width = PSScreen.width;
		eventReceiver.height = PSScreen.height;
		eventReceiver.captureInput = true;

		this.m_SceneSetupDone = true;
	}	

	update(clock : PSClock) : void
	{
		super.update(clock);


		if(!this.m_SceneSetupDone && PSResourceManager.instance.resourceCountToLoad == 0)
		{
			this.setupMainScene();
			return;
		}

		if(PSInput.isKeyPressed(PSKeyMap.A))
		{
			this.m_Scenario.x += 200 * clock.deltaTime;
		}

		if(PSInput.isKeyPressed(PSKeyMap.D))
		{
			this.m_Scenario.x -= 200 * clock.deltaTime;
		}

		if(PSInput.isKeyPressed(PSKeyMap.W))
		{
			this.m_Scenario.y += 200 * clock.deltaTime;
		}

		if(PSInput.isKeyPressed(PSKeyMap.S))
		{
			this.m_Scenario.y -= 200 * clock.deltaTime;
		}

		if(PSInput.isKeyJustPressed(PSKeyMap.R))
		{
			console.log('Zoom in');
			if(this.m_Scenario.transform.scaleX < 2)
			{
				this.m_Scenario.transform.scaleX += 0.25;
				this.m_Scenario.transform.scaleY += 0.25;
			}
		}

		if(PSInput.isKeyJustPressed(PSKeyMap.F))
		{
			console.log('Zoom out');
			if(this.m_Scenario.transform.scaleX > 0.5)
			{
				this.m_Scenario.transform.scaleX -= 0.25;
				this.m_Scenario.transform.scaleY -= 0.25;
			}
		}

		if(this.m_Scenario != null)
		{
			this.checkScenarioLocation();
		}
	}

	onEvent(type : string, event : PSEvent) : void
	{
		//console.log('Event received ', type, event);

		if(type == PSTouchEvent.TOUCH_BEGIN)
		{
			
		}

		if(type == PSTouchEvent.TOUCH_MOVE)
		{
			var touchEvent : any = event;
			this.m_Scenario.x += touchEvent.displacementX;
			this.m_Scenario.y += touchEvent.displacementY;

			this.checkScenarioLocation();

		}

		if(type == PSTouchEvent.TOUCH_END)
		{
			
		}
	}

	checkScenarioLocation() : void
	{
		if(this.m_Scenario.x < (-this.m_TileMap.mapWidth * this.m_Scenario.transform.scaleX) + PSScreen.width)
			this.m_Scenario.x =  (-this.m_TileMap.mapWidth * this.m_Scenario.transform.scaleX) + PSScreen.width;

		if(this.m_Scenario.y < (-this.m_TileMap.mapHeight * this.m_Scenario.transform.scaleY) + PSScreen.height)
			this.m_Scenario.y =  (-this.m_TileMap.mapHeight * this.m_Scenario.transform.scaleY) + PSScreen.height;

		if(this.m_Scenario.x > 0)
			this.m_Scenario.x = 0;

		if(this.m_Scenario.y > 0)
			this.m_Scenario.y =  0;
	}

	render(support : PSRenderSupport, gl : WebGLRenderingContext) : void
	{
		super.render(support, gl);
	}


	public m_TileMapAsset : TileMapDescriptor;
	public m_TileMap : TileMap;
	public m_Scenario : PSEntity;
	private m_SceneSetupDone : boolean;

};