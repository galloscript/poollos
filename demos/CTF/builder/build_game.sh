#!/bin/bash

find ../src -type f \( -name "*.ts" \) > args.txt
tsc -t ES5 --out ../game.js ../../../lib/poollos.d.ts @args.txt