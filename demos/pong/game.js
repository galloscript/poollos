var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var BallEmitter = (function (_super) {
    __extends(BallEmitter, _super);
    function BallEmitter(ball) {
        _super.call(this);
        this.m_Scaler = null;
        this.m_Fader = null;

        this.m_Ball = ball;

        this.width = 15;
        this.height = 15;
        this.pivotX = 0.5;
        this.pivotY = 0.5;
        this.scaleX = 0.0;
        this.scaleY = 0.0;
        this.color.setValues(0.0, 2.0, 0.0, 0.0);
        this.alpha = 0.0;
        this.m_Fader = new PSAlphaTo(0.0, 0.15 + (Math.random() * 0.4));

        this.m_Index = BallEmitter.s_ParticleCount;

        BallEmitter.s_ParticleCount += 1;
    }
    BallEmitter.prototype.update = function (clock) {
        if (this.alpha <= 0.0) {
            this.removeChild(this.m_Scaler);
            this.removeChild(this.m_Fader);
            this.scaleX = 1.0;
            this.scaleY = 1.0;
            this.alpha = 1.0;
            this.x = this.m_Ball.x;
            this.y = this.m_Ball.y;

            //this.m_Scaler = new PSScaleTo(0.0, 0.6);
            this.m_Fader.resetValues(0.0, 0.15 + (Math.random() * 0.4));
            this.addChild(this.m_Scaler);
            this.addChild(this.m_Fader);
        }
    };

    BallEmitter.s_ParticleCount = 0;
    return BallEmitter;
})(PSSprite);

var PongBall = (function (_super) {
    __extends(PongBall, _super);
    function PongBall() {
        _super.call(this);
        this.m_EmitterIinitialized = false;

        var sprite = new PSSprite();
        sprite.color.setValues(0.0, 2.0, 0.0, 1.0);
        sprite.width = 15;
        sprite.height = 15;
        this.addChild(sprite);
        this.m_Sprite = sprite;
        this.m_Sprite.pivotX = this.m_Sprite.pivotY = 0.5;

        this.m_Velocity = new PSPoint(110, 0);

        this.addEventListener(PSTouchEvent.TOUCH_BEGIN, this);
        this.addEventListener(PSTouchEvent.TOUCH_END, this);
        this.captureInput = true;
    }
    PongBall.prototype.update = function (clock) {
        this.x += this.m_Velocity.x * clock.deltaTime;
        this.y += this.m_Velocity.y * clock.deltaTime;

        //check ball out
        if (this.x < 20) {
            this.trigger(PongBall.OUT_LEFT_EVENT);
        }

        if (this.x > PSScreen.width - 15 - 20) {
            this.trigger(PongBall.OUT_RIGHT_EVENT);
        }
    };

    PongBall.prototype.setVelocity = function (vel) {
        this.m_Velocity.setFrom(vel);
    };

    PongBall.prototype.onEvent = function (type, event) {
        if (type == PSTouchEvent.TOUCH_BEGIN) {
            if (!this.m_EmitterIinitialized) {
                this.m_EmitterIinitialized = true;
                for (var i = 0; i < 200; i++) {
                    this.m_Parent.addChild(new PSDelay(new BallEmitter(this), i / 200));
                }
            }
        }
    };
    PongBall.OUT_LEFT_EVENT = 'PongBallOutLeft';
    PongBall.OUT_RIGHT_EVENT = 'PongBallOutRigth';
    return PongBall;
})(PSEntity);
var PongGame = (function (_super) {
    __extends(PongGame, _super);
    function PongGame() {
        _super.apply(this, arguments);
        this.m_TempVelocity = new PSPoint();
        this.m_LeftPlayerPoints = 0;
        this.m_RightPlayerPoints = 0;
        this.m_PWasPressed = false;
    }
    PongGame.prototype.init = function () {
        this.setupCanvas('PongCanvas');
        this.m_PostProEffect = this.m_PostProcessPipeline = new PSPostProcessPipeline(PSBuiltInGfx.PS_GLOW_EFFECT);
        this.m_PostProcessPipeline.init(this.m_RenderSupport, this.m_RenderSupport.m_GLContext, PSScreen.width, PSScreen.height, 2);
    };

    PongGame.prototype.loadResources = function (manager) {
    };

    PongGame.prototype.setupScene = function () {
        this.m_ClearColor.setValues(0.0, 0.0, 0.0, 1.0);

        this.m_PlayerLeft = new PongPlayer(PSKeyMap.SHIFT, PSKeyMap.CTRL);
        this.m_PlayerLeft.x = 20;
        this.m_PlayerLeft.y = 40;
        this.stage.addChild(this.m_PlayerLeft);

        this.m_PlayerRight = new PongPlayer(PSKeyMap.ARROW_UP, PSKeyMap.ARROW_DOWN);
        this.m_PlayerRight.x = PSScreen.width - this.m_PlayerRight.getBounds().width - 20;
        this.m_PlayerRight.y = 40;
        this.stage.addChild(this.m_PlayerRight);

        this.m_Ball = new PongBall();
        this.m_Ball.x = PSScreen.width * 0.5;
        this.m_Ball.y = PSScreen.height * 0.5;
        this.m_Ball.addEventListener(PongBall.OUT_LEFT_EVENT, this);
        this.m_Ball.addEventListener(PongBall.OUT_RIGHT_EVENT, this);
        this.stage.addChild(this.m_Ball);

        this.m_PointsText = new PSTextSprite('0 - 0', 42);
        this.m_PointsText.color.setValues(1.0, 1.0, 1.0, 1.0);
        this.m_PointsText.compose();
        this.m_PointsText.pivotX = 0.5;
        this.m_PointsText.x = PSScreen.width * 0.5;
        this.m_PointsText.y = 20;
        this.stage.addChild(this.m_PointsText);
    };

    PongGame.prototype.update = function (clock) {
        _super.prototype.update.call(this, clock);

        this.checkBallState();

        if (this.m_Input.isKeyPressed(PSKeyMap.P) && !this.m_PWasPressed) {
            if (this.m_PostProcessPipeline == null) {
                this.m_PostProcessPipeline = this.m_PostProEffect;
            } else {
                this.m_PostProcessPipeline = null;
                this.m_RenderSupport.restoreBlendFunc();
            }
        }

        this.m_PWasPressed = this.m_Input.isKeyPressed(PSKeyMap.P);
    };

    PongGame.prototype.checkBallState = function () {
        var ballBounds = this.m_Ball.getBounds();
        var leftPlayerBounds = this.m_PlayerLeft.getBounds();
        var rightPlayerBounds = this.m_PlayerRight.getBounds();

        this.m_TempVelocity.setFrom(this.m_Ball.m_Velocity);

        if (ballBounds.intersectsWidth(leftPlayerBounds)) {
            this.m_TempVelocity.x = 300;
            this.m_TempVelocity.y = this.m_Ball.m_Velocity.y + this.m_PlayerLeft.m_Speed * Math.min(0.2, 0.07 + Math.random());
        }

        if (ballBounds.intersectsWidth(rightPlayerBounds)) {
            this.m_TempVelocity.x = -300;
            this.m_TempVelocity.y = this.m_Ball.m_Velocity.y + this.m_PlayerRight.m_Speed * Math.min(0.2, 0.07 + Math.random());
            this.m_Ball.setVelocity(this.m_TempVelocity);
        }

        if (this.m_Ball.y < 0 || this.m_Ball.y > PSScreen.height - ballBounds.height) {
            this.m_TempVelocity.x = this.m_Ball.m_Velocity.x;
            this.m_TempVelocity.y = this.m_Ball.m_Velocity.y * -1;

            if (this.m_Ball.y < 0) {
                this.m_Ball.y = 1;
            } else {
                this.m_Ball.y = PSScreen.height - ballBounds.height - 1;
            }
        }

        this.m_Ball.setVelocity(this.m_TempVelocity);
    };

    PongGame.prototype.render = function (support, gl) {
        _super.prototype.render.call(this, support, gl);
    };

    PongGame.prototype.onEvent = function (type, event) {
        //console.log('Event received ', type, event);
        if (type == PongBall.OUT_LEFT_EVENT || type == PongBall.OUT_RIGHT_EVENT) {
            this.m_Ball.x = PSScreen.width * 0.5;
            this.m_Ball.y = PSScreen.height * 0.5;
            this.m_TempVelocity.x = 200;
            if (type == PongBall.OUT_RIGHT_EVENT) {
                this.m_TempVelocity.x = -200;
                this.m_LeftPlayerPoints += 1;
            } else {
                this.m_RightPlayerPoints += 1;
            }

            this.m_TempVelocity.y = 0;

            this.m_Ball.setVelocity(this.m_TempVelocity);

            this.m_PointsText.setText(this.m_LeftPlayerPoints + ' - ' + this.m_RightPlayerPoints);
        }
    };
    return PongGame;
})(PSGame);
;
var PongPlayer = (function (_super) {
    __extends(PongPlayer, _super);
    function PongPlayer(upKey, downKey) {
        _super.call(this);
        this.m_MoveUpKeyCode = upKey;
        this.m_MoveDownKeyCode = downKey;

        var sprite = new PSSprite();
        sprite.color.setValues(4.0, 4.0, 4.0, 1.0);
        sprite.width = 20;
        sprite.height = 120;
        this.addChild(sprite);
        this.m_Sprite = sprite;

        this.m_Speed = 500;

        this.updateBounds(true);

        this.m_LimitTop = 20;
        this.m_LimitBottom = PSScreen.height - this.getBounds().height - 20;

        this.m_CaptureInput = true;
        this.addEventListener(PSTouchEvent.TOUCH_BEGIN, this);
        this.addEventListener(PSTouchEvent.TOUCH_END, this);
    }
    PongPlayer.prototype.update = function (clock) {
        if (PSInput.isKeyPressed(this.m_MoveUpKeyCode) && !PSInput.isKeyPressed(this.m_MoveDownKeyCode)) {
            this.m_Speed = -500;
        } else if (PSInput.isKeyPressed(this.m_MoveDownKeyCode)) {
            this.m_Speed = 500;
        } else {
            this.m_Speed = 0;
        }

        this.y += this.m_Speed * clock.deltaTime;

        if (this.y < this.m_LimitTop)
            this.y = this.m_LimitTop;

        if (this.y > this.m_LimitBottom)
            this.y = this.m_LimitBottom;
    };

    PongPlayer.prototype.onEvent = function (type, event) {
        if (type == PSTouchEvent.TOUCH_BEGIN) {
            this.color.red = Math.random();
            this.color.blue = Math.random();
        }
    };
    return PongPlayer;
})(PSEntity);
