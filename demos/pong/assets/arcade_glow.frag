precision mediump float;
uniform sampler2D uSampler;
uniform vec2 uScreenSize;
varying vec2 vTextureCoord;


void main(void){
	vec2 pixelSize = vec2(1.0 / uScreenSize.x, 1.0 / uScreenSize.y);
	vec2 pos=gl_FragCoord.xy*pixelSize;

	float values[9];
	values[0]=0.05;
	values[1]=0.09;
	values[2]=0.11;
	values[3]=0.15;
	values[4]=0.2;
	values[5]=0.15;
	values[6]=0.11;
	values[7]=0.09;
	values[8]=0.05;

	vec4 result;

	vec2 curSamplePos=vec2(pos.x, pos.y - 4.0 * pixelSize.y);
	for(int i=0;i<9;i++)
	{
		result += texture2D(texture, curSamplePos) * values[i];
		curSamplePos.y += pixelSize.y;
	}

	gl_FragColor = vec4(result.xyz, 1.0);
}