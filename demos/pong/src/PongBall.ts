
class BallEmitter extends PSSprite
{
	constructor(ball : PongBall)
	{
		super();

		this.m_Ball = ball;

		this.width = 15;
		this.height = 15;
		this.pivotX = 0.5;
		this.pivotY = 0.5;
		this.scaleX = 0.0;
		this.scaleY = 0.0;
		this.color.setValues(0.0, 2.0, 0.0, 0.0);
		this.alpha = 0.0;
		this.m_Fader = new PSAlphaTo(0.0, 0.15 + (Math.random() * 0.4));

		this.m_Index = BallEmitter.s_ParticleCount;

		BallEmitter.s_ParticleCount += 1;
	}

	update(clock : PSClock) : void
	{
		if(this.alpha <= 0.0)
		{
			this.removeChild(this.m_Scaler);
			this.removeChild(this.m_Fader);
			this.scaleX = 1.0;
			this.scaleY = 1.0;
			this.alpha = 1.0;
			this.x = this.m_Ball.x;
			this.y = this.m_Ball.y;
			//this.m_Scaler = new PSScaleTo(0.0, 0.6);
			this.m_Fader.resetValues(0.0, 0.15 + (Math.random() * 0.4));
			this.addChild(this.m_Scaler);
			this.addChild(this.m_Fader);
		}
	}

	m_Ball : PongBall;
	m_Scaler : PSScaleTo = null;
	m_Fader : PSAlphaTo = null;
	m_Index : number;
	static s_ParticleCount : number = 0;

}

class PongBall extends PSEntity
{
	
	static OUT_LEFT_EVENT : string = 'PongBallOutLeft';
	static OUT_RIGHT_EVENT : string = 'PongBallOutRigth';

	constructor()
	{
		super();

		var sprite = new PSSprite();
		sprite.color.setValues(0.0, 2.0, 0.0, 1.0);
		sprite.width = 15;
		sprite.height = 15;
		this.addChild(sprite);
		this.m_Sprite = sprite;
		this.m_Sprite.pivotX = this.m_Sprite.pivotY = 0.5;

		this.m_Velocity = new PSPoint(110, 0);

		this.addEventListener(PSTouchEvent.TOUCH_BEGIN, this);
		this.addEventListener(PSTouchEvent.TOUCH_END, this);
		this.captureInput = true;
	}


	update(clock : PSClock)
	{

		this.x += this.m_Velocity.x * clock.deltaTime;
		this.y += this.m_Velocity.y * clock.deltaTime;

		//check ball out
		if(this.x < 20)
		{
			this.trigger(PongBall.OUT_LEFT_EVENT);
		}

		if(this.x > PSScreen.width - 15 - 20)
		{
			this.trigger(PongBall.OUT_RIGHT_EVENT);
		}


	}

	setVelocity(vel : PSPoint) : void
	{
		this.m_Velocity.setFrom(vel);
	}


	onEvent(type : string, event : PSEvent) : void
	{
		if(type == PSTouchEvent.TOUCH_BEGIN)
		{
			if(!this.m_EmitterIinitialized)
			{
				this.m_EmitterIinitialized = true;
				for(var i = 0 ; i < 200; i++)
				{
					this.m_Parent.addChild(new PSDelay(new BallEmitter(this), i/200 ));
				}
			}
		}
	}

	m_Sprite : PSSprite;
	m_Velocity : PSPoint;

	m_EmitterIinitialized : boolean = false;
}