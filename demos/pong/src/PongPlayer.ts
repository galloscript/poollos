
class PongPlayer extends PSEntity
{
	constructor(upKey : number, downKey : number)
	{
		super();
		this.m_MoveUpKeyCode = upKey;
		this.m_MoveDownKeyCode = downKey;

		var sprite = new PSSprite();
		sprite.color.setValues(4.0, 4.0, 4.0, 1.0);
		sprite.width = 20;
		sprite.height = 120
		this.addChild(sprite);
		this.m_Sprite = sprite;

		this.m_Speed = 500;

		this.updateBounds(true);

		this.m_LimitTop = 20;
		this.m_LimitBottom = PSScreen.height - this.getBounds().height - 20;

		this.m_CaptureInput = true;
		this.addEventListener(PSTouchEvent.TOUCH_BEGIN, this);
		this.addEventListener(PSTouchEvent.TOUCH_END, this);
	}

	update(clock : PSClock) : void
	{
		if(PSInput.isKeyPressed(this.m_MoveUpKeyCode) && !PSInput.isKeyPressed(this.m_MoveDownKeyCode))
		{
			this.m_Speed = -500;
		}
		else if(PSInput.isKeyPressed(this.m_MoveDownKeyCode))
		{
			this.m_Speed = 500;
		}
		else
		{
			this.m_Speed = 0;
		}

		this.y += this.m_Speed * clock.deltaTime;

		if(this.y < this.m_LimitTop)
			this.y = this.m_LimitTop;

		if(this.y > this.m_LimitBottom)
			this.y = this.m_LimitBottom;
	}

	onEvent(type : string, event : PSEvent) : void
	{
		if(type == PSTouchEvent.TOUCH_BEGIN)
		{
			this.color.red = Math.random();
			this.color.blue = Math.random();
		}
	}


	m_MoveUpKeyCode : number;
	m_MoveDownKeyCode : number;
	m_Sprite : PSSprite;
	m_Speed : number;

	m_LimitTop : number;
	m_LimitBottom : number;
}