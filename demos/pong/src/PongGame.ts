
class PongGame extends PSGame
{
	init() : void
	{
		this.setupCanvas('PongCanvas');
		this.m_PostProEffect = this.m_PostProcessPipeline = new PSPostProcessPipeline(PSBuiltInGfx.PS_GLOW_EFFECT);
		this.m_PostProcessPipeline.init(this.m_RenderSupport, this.m_RenderSupport.m_GLContext, PSScreen.width, PSScreen.height, 2);
	}

	loadResources(manager : PSResourceManager) : void
	{
		
	}

	setupScene() : void
	{
		this.m_ClearColor.setValues(0.0, 0.0, 0.0, 1.0);

		this.m_PlayerLeft = new PongPlayer(PSKeyMap.SHIFT, PSKeyMap.CTRL);
		this.m_PlayerLeft.x = 20;
		this.m_PlayerLeft.y = 40;
		this.stage.addChild(this.m_PlayerLeft);

		this.m_PlayerRight = new PongPlayer(PSKeyMap.ARROW_UP, PSKeyMap.ARROW_DOWN);
		this.m_PlayerRight.x = PSScreen.width - this.m_PlayerRight.getBounds().width - 20;
		this.m_PlayerRight.y = 40;
		this.stage.addChild(this.m_PlayerRight);

		this.m_Ball = new PongBall();
		this.m_Ball.x = PSScreen.width * 0.5;
		this.m_Ball.y = PSScreen.height * 0.5;
		this.m_Ball.addEventListener(PongBall.OUT_LEFT_EVENT, this);
		this.m_Ball.addEventListener(PongBall.OUT_RIGHT_EVENT, this);
		this.stage.addChild(this.m_Ball);

		this.m_PointsText = new PSTextSprite('0 - 0', 42);
		this.m_PointsText.color.setValues(1.0, 1.0, 1.0, 1.0);
		this.m_PointsText.compose();
		this.m_PointsText.pivotX = 0.5;
		this.m_PointsText.x = PSScreen.width * 0.5;
		this.m_PointsText.y = 20;
		this.stage.addChild(this.m_PointsText);

	}	

	update(clock : PSClock) : void
	{
		super.update(clock);
		
		this.checkBallState();

		if(this.m_Input.isKeyPressed(PSKeyMap.P) && !this.m_PWasPressed)
		{
			if(this.m_PostProcessPipeline == null)
			{
				this.m_PostProcessPipeline = this.m_PostProEffect;
			}
			else
			{
				this.m_PostProcessPipeline = null;
				this.m_RenderSupport.restoreBlendFunc();
			}
		}

		this.m_PWasPressed = this.m_Input.isKeyPressed(PSKeyMap.P);
	}

	checkBallState() : void
	{
		var ballBounds = this.m_Ball.getBounds();
		var leftPlayerBounds = this.m_PlayerLeft.getBounds();
		var rightPlayerBounds = this.m_PlayerRight.getBounds();

		this.m_TempVelocity.setFrom(this.m_Ball.m_Velocity);

		if(ballBounds.intersectsWidth(leftPlayerBounds))
		{
			this.m_TempVelocity.x = 300;
			this.m_TempVelocity.y = this.m_Ball.m_Velocity.y + this.m_PlayerLeft.m_Speed * Math.min(0.2, 0.07 + Math.random());
			
		}

		if(ballBounds.intersectsWidth(rightPlayerBounds))
		{
			this.m_TempVelocity.x = -300;
			this.m_TempVelocity.y = this.m_Ball.m_Velocity.y + this.m_PlayerRight.m_Speed * Math.min(0.2, 0.07 + Math.random());
			this.m_Ball.setVelocity(this.m_TempVelocity);
		}

		if(this.m_Ball.y < 0 || this.m_Ball.y > PSScreen.height - ballBounds.height)
		{
			this.m_TempVelocity.x = this.m_Ball.m_Velocity.x
			this.m_TempVelocity.y = this.m_Ball.m_Velocity.y * -1;

			if(this.m_Ball.y < 0)
			{
				this.m_Ball.y = 1;
			}
			else
			{
				this.m_Ball.y = PSScreen.height - ballBounds.height - 1;
			}
		}

		this.m_Ball.setVelocity(this.m_TempVelocity);
	}

	render(support : PSRenderSupport, gl : WebGLRenderingContext) : void
	{
		super.render(support, gl);
	}

	onEvent(type : string, event : PSEvent) : void
	{
		//console.log('Event received ', type, event);

		if(type == PongBall.OUT_LEFT_EVENT || type == PongBall.OUT_RIGHT_EVENT)
		{
			this.m_Ball.x = PSScreen.width * 0.5;
			this.m_Ball.y = PSScreen.height * 0.5;
			this.m_TempVelocity.x = 200;
			if(type == PongBall.OUT_RIGHT_EVENT)
			{
				this.m_TempVelocity.x = -200;
				this.m_LeftPlayerPoints += 1;
			}
			else
			{
				this.m_RightPlayerPoints += 1;
			}

			this.m_TempVelocity.y = 0;

			this.m_Ball.setVelocity(this.m_TempVelocity);

			this.m_PointsText.setText(this.m_LeftPlayerPoints+' - '+this.m_RightPlayerPoints);
		}
	}

	m_PlayerLeft : PongPlayer;
	m_PlayerRight : PongPlayer;
	m_Ball : PongBall;
	m_PointsText : PSTextSprite;
	m_TempVelocity : PSPoint = new PSPoint();

	m_LeftPlayerPoints : number = 0;
	m_RightPlayerPoints : number = 0;

	m_PostProEffect : PSPostProcessPipeline;
	m_PWasPressed : boolean = false;

};