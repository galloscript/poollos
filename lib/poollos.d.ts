/** math/Point.ts */
declare class PSPoint implements PSPoolObject {
    /** public */
    constructor(a?: any, b?: any);
    x: number;
    y: number;
    length: number;
    setValues(x: number, y: number): PSPoint;
    setFrom(other: PSPoint): PSPoint;
    normalize(): PSPoint;
    multiply(p2: PSPoint): PSPoint;
    multiply(p2: number): PSPoint;
    add(p2: PSPoint): PSPoint;
    subtract(p2: PSPoint): PSPoint;
    resetValues(): void;
    distanceTo(other: PSPoint): number;
    /** static */
    static X_AXIS: PSPoint;
    static Y_AXIS: PSPoint;
    static ZERO: PSPoint;
    static ONE: PSPoint;
    static add(p1: PSPoint, p2: PSPoint): PSPoint;
    static subtract(p1: PSPoint, p2: PSPoint): PSPoint;
    static multiply(p1: PSPoint, p2: PSPoint): PSPoint;
    static multiply(p1: PSPoint, p2: number): PSPoint;
    static dot(p1: PSPoint, p2: PSPoint): number;
    static angle(p1: PSPoint, p2: PSPoint): number;
    static distanceBetween(p1: PSPoint, p2: PSPoint): number;
    /** private */
    private m_X;
    private m_Y;
    static POOL: any[];
}
interface PSPoolObject {
    resetValues(): void;
}
declare class PSPool {
    static createPool(classType: any, initialSize?: number): void;
    static alloc(classType: any): any;
    static free<T extends PSPoolObject>(obj: T): void;
    static poolFor(classType: any): PSPool;
    constructor(classType: any, initialSize: number);
    private fill();
    private getAvailableElement();
    private freeElement<T>(element);
    private m_ClassType;
    private m_FreeElements;
    private m_UsedElements;
    private m_InitialSize;
    private static s_PoolMap;
}
/** math/Matrix.ts */
declare class PSMatrix implements PSPoolObject {
    /** public */
    constructor(m?: PSMatrix);
    setFrom(other: PSMatrix): PSMatrix;
    setValues(otherArray: any): PSMatrix;
    identity(): PSMatrix;
    determinant: number;
    arrayBuffer: Float32Array;
    concat(otherMatrix: PSMatrix): PSMatrix;
    translate(x: number, y: number): PSMatrix;
    rotate(angle: number): PSMatrix;
    scale(sx: number, sy: number): PSMatrix;
    transformPoint(point: PSPoint, targetPoint?: PSPoint): PSPoint;
    invert(): PSMatrix;
    inverse(): PSMatrix;
    resetValues(): void;
    static make2DProjection(width: number, height: number, target?: PSMatrix): PSMatrix;
    /** private */
    private m_M;
}
/** math/Transform.ts */
declare class PSTransform {
    constructor();
    pivotX: number;
    pivotY: number;
    x: number;
    y: number;
    scaleX: number;
    scaleY: number;
    rotation: number;
    width: number;
    height: number;
    pivot: PSPoint;
    location: PSPoint;
    scale: any;
    getTransformationMatrix(targetMatrix?: PSMatrix, includeWH?: boolean): PSMatrix;
    m_Pivot: PSPoint;
    m_Location: PSPoint;
    m_Scale: PSPoint;
    m_Rotation: number;
    m_Width: number;
    m_Height: number;
}
declare class PSCamera extends PSTransform {
}
/** display/Event.ts */
declare class PSEvent {
    constructor(type: string);
    type: string;
    captured: boolean;
    target: PSEntity;
    toString(): string;
    resetValues(type: string): void;
    private m_Type;
    private m_Captured;
    private m_Target;
}
/** display/EventListener.ts */
declare class PSEventListener {
    constructor();
    onEvent(type: string, event: PSEvent): void;
}
/** stdlib.ts */
interface PSDictionary<T> {
    [index: string]: T;
}
/** display/EventDispatcher.ts */
declare class PSEventDispatcher extends PSEventListener {
    constructor();
    addEventListener(type: string, listener: PSEventListener): void;
    dispatchEvent(event: PSEvent): void;
    trigger(type: string): void;
    hasEventListener(type: string): boolean;
    removeEventListener(type: string, listener: PSEventListener): void;
    clearEventListeners(): void;
    private m_Listeners;
}
declare class PSColor {
    static WHITE: PSColor;
    /** public */
    constructor(r?: any, g?: number, b?: number, a?: number);
    red: number;
    green: number;
    blue: number;
    alpha: number;
    arrayBuffer: Float32Array;
    setFrom(other: PSColor): PSColor;
    resetValues(): void;
    setValues(r: number, g: number, b: number, a?: number): PSColor;
    multiply(other: PSColor): PSColor;
    divide(other: PSColor): PSColor;
    getRGBHexString(): string;
    static multiply(other: PSColor, result?: PSColor): PSColor;
    static divide(other: PSColor, result?: PSColor): PSColor;
    private m_ColorVector;
}
declare class PSRectangle {
    constructor(x?: number, y?: number, w?: number, h?: number);
    x: number;
    y: number;
    width: number;
    height: number;
    clone(): PSRectangle;
    rectUnion(other: PSRectangle): PSRectangle;
    unite(other: PSRectangle): void;
    containsPoint(point: PSPoint): boolean;
    intersectsWidth(toIntersect: PSRectangle): boolean;
    intersectionResult(toIntersect: PSRectangle): PSRectangle;
    toString(): string;
    private m_X;
    private m_Y;
    private m_Width;
    private m_Height;
}
/** display/TouchEvent.ts */
declare class PSTouchEvent extends PSEvent {
    /** static */
    static TOUCH_BEGIN: string;
    static TOUCH_END: string;
    static TOUCH_MOVE: string;
    static TOUCH_OUT: string;
    static TOUCH_OVER: string;
    static TOUCH_TAP: string;
    static TOUCH_PINCH: string;
    /** public */
    constructor(type: string);
    localX: number;
    localY: number;
    stageX: number;
    stageY: number;
    displacementX: number;
    displacementY: number;
    duration: number;
    velocity: number;
    factor: number;
    toString(): string;
    resetValues(type: string): void;
    /** private */
    private m_LocalX;
    private m_LocalY;
    private m_StageX;
    private m_StageY;
    private m_DisplacementX;
    private m_DisplacementY;
    private m_Duration;
    private m_Velocity;
    private m_Factor;
}
/** display/Entity.ts */
declare class PSEntity extends PSEventDispatcher {
    constructor();
    /** Public properties */
    transform: PSTransform;
    pivotX: number;
    pivotY: number;
    x: number;
    y: number;
    scaleX: number;
    scaleY: number;
    rotation: number;
    width: number;
    height: number;
    name: string;
    numChildren: number;
    timeScale: number;
    visible: boolean;
    captureInput: boolean;
    color: PSColor;
    parent: PSEntity;
    alpha: number;
    /** Returns a copy of the world matrix */
    getWorldMatrix(): PSMatrix;
    /** Override this method to change it's behaviour */
    /** virtual */ update(clock: PSClock): void;
    /** Override this method to change the way is rendered */
    /** virtual */ render(support: PSRenderSupport, gl: WebGLRenderingContext): void;
    /** called when the object is going to be destroyed */
    /** virtual */ onDestroy(): void;
    /** Add a child object */
    addChild(child: PSEntity): void;
    /** Add the child at the specified position */
    addChildAt(child: PSEntity, index: number): void;
    /** Remove and returns the child reseting it's parent to null */
    removeChild(child: PSEntity): PSEntity;
    /** Returns the first child found with the specified name */
    getChildByName(childName: string, recursive?: boolean): PSEntity;
    /** Returns the child in the specified position or null if the  */
    getChildAt(index: number): PSEntity;
    /** Returns the index of the specified child */
    getChildIndex(child: PSEntity): number;
    /** Sets the specified index to the child */
    setChildIndex(child: PSEntity, index: number): void;
    /** Remove this object from it's parent and destroys it */
    destroy(): void;
    /** Remove and destroy all object's children */
    destroyAllChilds(): void;
    /** Updates the world matrix */
    forceMatrixUpdate(): void;
    /** Internally updates the object (do not manually call this method) */
    innerUpdate(time: PSClock): void;
    /** Internally render the object (do not manually call this method) */
    innerRender(support: PSRenderSupport, gl: WebGLRenderingContext): void;
    /** Add a child entity to destroy */
    addEntityToDestroy(obj: PSEntity): void;
    /** Delete objects to be destroyed */
    deletePendentObjects(): void;
    /** Update the bounds rectangle with the object and childs boundaries */
    updateBounds(updateChilds?: boolean): void;
    /** Returns a reference to the bounds rectnagle */
    getBounds(forceUpdate?: boolean, updateChilds?: boolean): PSRectangle;
    /** Returns true if the point is inside the boundaries of this object */
    hitTestPoint(p: PSPoint): boolean;
    /** Converts a point to global to local object space, returns a new point */
    globalToLocal(p: PSPoint, targetPoint?: PSPoint): PSPoint;
    /** Converts a point from local space to global space, returns a new point */
    localToGlobal(p: PSPoint, targetPoint?: PSPoint): PSPoint;
    /** Called on every object when a Touch event occurs (finger touch or mouse click) */
    onNativeEvent(event: PSTouchEvent): void;
    /** Remove and destroy the child */
    private destroyChild(child);
    /** Properties */
    m_Transform: PSTransform;
    m_Parent: PSEntity;
    m_Children: Array<PSEntity>;
    m_TimeScale: number;
    m_Name: string;
    m_Visible: boolean;
    m_WorldMatrix: PSMatrix;
    m_Color: PSColor;
    m_Bounds: PSRectangle;
    m_ClipRectangle: PSRectangle;
    /** Inner deferred object destruction */
    private m_EntitiesToDestroy;
    private m_DirtyList;
    /** Touch events handling */
    m_MouseDown: boolean;
    m_MouseDownPoint: PSPoint;
    m_CaptureInput: boolean;
    m_IncludeWidthHeight: boolean;
}
interface IPSLoadCallback {
    (newRes: IPSResource): void;
}
interface IPSResource {
    load(callback: IPSLoadCallback): void;
    unload(callback: IPSLoadCallback): void;
    isLoaded(): boolean;
    getResourceName(): string;
    getLoaderCallback(): IPSLoadCallback;
}
interface IPSResourcesMap {
    [key: string]: IPSResource;
}
declare class PSResourceManager {
    static instance: PSResourceManager;
    constructor();
    verbose: boolean;
    resourceCountToLoad: number;
    loadingResources: boolean;
    loadedPercentage: number;
    loadResource(newRes: IPSResource, optCallback?: IPSLoadCallback): any;
    getResource(name: string): any;
    static onResourceLoaded(newRes: IPSResource): void;
    static onResourceUnLoaded(res: IPSResource): void;
    resourceLoaded(newRes: IPSResource): void;
    resourceUnLoaded(res: IPSResource): void;
    updateLoads(): void;
    setResourceKey(key: string, res: IPSResource): void;
    private m_ResourcesToLoad;
    private m_LoadedResources;
    private m_ResourcesMap;
    private m_Verbose;
    private static s_Instance;
}
declare var PS_RESMNGR: PSResourceManager;
declare var ETexFilterStyle: {
    TS_STANDARD: number;
    TS_PIXELART: number;
};
declare class PSTexture implements IPSResource {
    static TEXTURE_FILTER_STYLE: number;
    constructor(path: string, repeat?: boolean);
    textureId: WebGLTexture;
    width: number;
    height: number;
    sourceWidth: number;
    sourceHeight: number;
    textureMatrix: PSMatrix;
    load(callback: IPSLoadCallback): void;
    unload(callback: IPSLoadCallback): void;
    isLoaded(): boolean;
    getResourceName(): string;
    getLoaderCallback(): IPSLoadCallback;
    innerCallback(): void;
    createTexture(image: any, genmipmap?: boolean): void;
    updateTexture(image: any, genmipmap?: boolean): void;
    static isPowerOfTwo(x: number): boolean;
    static nextHighestPowerOfTwo(x: number): number;
    static getTextureResource(resource: string): PSTexture;
    private m_Path;
    private m_Loaded;
    private m_Loading;
    private m_LoaderCallback;
    private m_TextureId;
    private m_ImageElement;
    private m_Width;
    private m_Height;
    private m_POTWidth;
    private m_POTHeight;
    private m_Repeat;
    private m_TextureMatrix;
}
declare class PSSprite extends PSEntity {
    constructor(p?: PSTexture);
    constructor(p?: string);
    setTexture(resource: PSTexture): void;
    setTexture(resource: string): void;
    setTextureTransform(panX: number, panY: number, scaleX: number, scaleY: number, rotation: number): void;
    render(support: PSRenderSupport, gl: WebGLRenderingContext): void;
    setScaleFromTexture(): void;
    m_Texture: PSTexture;
    private m_TextureTransform;
}
declare class PSTextSprite extends PSSprite {
    constructor(text?: string, fontSize?: number, fontFamily?: string, forcedWidth?: number, forcedHeight?: number);
    setText(text: string, forcedWidth?: number, forcedHeight?: number): void;
    compose(): void;
    private m_Text;
    private m_FontSize;
    private m_FontFamily;
    private m_InnerCanvas;
    private m_InnerContext;
    private m_MaxWidth;
}
declare class PSMaterial implements IPSResource {
    static CURRENT_MATERIAL: PSMaterial;
    constructor(name: string, program: PSShaderProgram);
    releasePrevious(support: PSRenderSupport): void;
    /** Do not call super.apply() or super.release() methods, overwrite them */
    apply(support: PSRenderSupport): void;
    release(support: PSRenderSupport): void;
    loadUniforms(gl: WebGLRenderingContext): void;
    load(callback: IPSLoadCallback): void;
    unload(callback: IPSLoadCallback): void;
    isLoaded(): boolean;
    getResourceName(): string;
    getLoaderCallback(): IPSLoadCallback;
    private m_Path;
    private m_Loaded;
    private m_Loading;
    private m_LoaderCallback;
    m_ShaderProgram: PSShaderProgram;
    private static s_CurrentMaterial;
}
declare class PSBuiltInGfx {
    static load(): void;
    static PS_DEFAULT_SHADER: PSShaderProgram;
    static PS_GLOW_SHADER: PSShaderProgram;
    /** WebGLVertexArrayObjectOES */
    static PS_DEFAULT_QUAD_VAO: any;
    /** WebGLBuffer */
    static PS_DEFAULT_QUAD_VBO: any;
    static PS_DEFAULT_MATERIAL: PSBuiltInSpriteMaterial;
    static PS_GLOW_EFFECT: PSBuiltInGlowEffect;
    static PS_DEFAULT_TEXTURE: PSTexture;
    static PS_DEFAULT_SHADER_VERT_SRC: string;
    static PS_DEFAULT_SHADER_FRAG_SRC: string;
    static PS_NULL_SHADER_VERT_SRC: string;
    static PS_GLOW_SHADER_FRAG_SRC: string;
}
declare class PSBuiltInSpriteMaterial extends PSMaterial {
    constructor(name: string, program: PSShaderProgram);
    loadUniforms(gl: WebGLRenderingContext): void;
    apply(support: PSRenderSupport): void;
    release(support: PSRenderSupport): void;
    uSampler: PSTexture;
    uModelMatrix: PSMatrix;
    uVPMatrix: PSMatrix;
    uColor: PSColor;
    uTMatrix: PSMatrix;
    private m_uSamplerLocation;
    private m_uMVPMatrixLocation;
    private m_uColorLocation;
    private m_uTMatrixLocation;
}
declare class PSBuiltInGlowEffect extends PSMaterial implements IPSPostProcessEffect {
    constructor(name: string, program: PSShaderProgram);
    setColorTexture(texture: WebGLTexture): void;
    setMVPMatrix(mvp: PSMatrix): void;
    setFBSize(width: number, height: number): void;
    setPass(pass: number): void;
    loadUniforms(gl: WebGLRenderingContext): void;
    apply(support: PSRenderSupport): void;
    release(support: PSRenderSupport): void;
    private m_MVPMatrix;
    private m_ColorTexture;
    private m_FBWidth;
    private m_FBHeight;
    private m_Pass;
    private m_uSamplerLocation;
    private m_uMVPMatrixLocation;
    private m_uScreenSizeLocation;
    private m_uPassLocation;
}
interface IPSPostProcessEffect extends PSMaterial {
    setColorTexture(texture: WebGLTexture): void;
    setMVPMatrix(mvp: PSMatrix): void;
    setFBSize(width: number, height: number): void;
    setPass(pass: number): void;
}
declare class PSPostProcessPipeline {
    constructor(effect: IPSPostProcessEffect);
    init(support: PSRenderSupport, gl: WebGLRenderingContext, screenW: number, screenH: number, passes?: number): void;
    createRenderTarget(gl: WebGLRenderingContext, width: number, height: number, index: number): void;
    bind(support: PSRenderSupport, gl: WebGLRenderingContext): void;
    render(support: PSRenderSupport, gl: WebGLRenderingContext): void;
    private m_FrameBuffers;
    private m_ColorTextures;
    private m_FullScreenQuadVAO;
    private m_Effect;
    m_FBWidth: number;
    m_FBHeight: number;
    private m_MVP;
    m_Passes: number;
}
declare class PSContextState implements PSPoolObject {
    constructor(m: PSMatrix, c: PSColor);
    resetValues(): void;
    matrix: PSMatrix;
    color: PSColor;
}
declare class PSRenderSupport {
    constructor();
    renderContext: WebGLRenderingContext;
    viewMatrix: PSMatrix;
    projectionMatrix: PSMatrix;
    private updateVPMatrix();
    setup(canvas: HTMLCanvasElement): void;
    pushState(m: PSMatrix, c: PSColor): void;
    popState(): void;
    restoreBlendFunc(): void;
    m_GLContext: WebGLRenderingContext;
    m_ContextStack: Array<PSContextState>;
    m_CurrentModelMatrix: PSMatrix;
    m_CurrentColor: PSColor;
    m_CurrentViewMatrix: PSMatrix;
    m_CurrentProjectionMatrix: PSMatrix;
    m_CurrentVPMatrix: PSMatrix;
}
declare class PSVertexAttribute {
    constructor(n: string, l: number);
    name: string;
    location: number;
}
declare class PSBuiltInAttributes {
    static POSITION: PSVertexAttribute;
    static TEXCOORD: PSVertexAttribute;
}
declare class PSShaderProgram implements IPSResource {
    static PROGRAM_IN_USE: PSShaderProgram;
    constructor(name: string);
    programId: WebGLProgram;
    load(callback: IPSLoadCallback): void;
    unload(callback: IPSLoadCallback): void;
    isLoaded(): boolean;
    getResourceName(): string;
    getLoaderCallback(): IPSLoadCallback;
    attachSource(shader: PSShaderSource): void;
    link(): void;
    use(gl: WebGLRenderingContext): void;
    private m_ProgramId;
    private m_Loading;
    private m_Loaded;
    private m_Path;
    private m_LoaderCallback;
    private m_ShaderSources;
    private static s_ProgramInUse;
}
declare enum EShaderSourceType {
    FRAGMENT_SHADER = 35632,
    VERTEX_SHADER = 35633,
}
declare class PSShaderSource implements IPSResource {
    constructor(name: string, type: EShaderSourceType);
    sourceId: WebGLShader;
    sourceType: EShaderSourceType;
    isLoaded(): boolean;
    getResourceName(): string;
    innerCallback(data: string): void;
    load(callback: IPSLoadCallback): void;
    unload(callback: IPSLoadCallback): void;
    getLoaderCallback(): IPSLoadCallback;
    compileSource(source: string): void;
    private m_SourceType;
    private m_SourceId;
    private m_Loaded;
    private m_Loading;
    private m_Path;
    private m_LoaderCallback;
}
declare class PSAlphaTo extends PSEntity {
    constructor(targetValue: number, inTime: number, easeFunc?: IPSEaseFunction);
    estroyOnFinish: boolean;
    destroyOnFinish: boolean;
    update(clock: PSClock): void;
    resetValues(targetValue: number, inTime: number, easeFunc?: IPSEaseFunction): void;
    private m_TargetValue;
    private m_InitialValue;
    private m_InTime;
    private m_InnerTimer;
    private m_EaseFunction;
    private m_DestroyOnFinish;
}
declare class PSColorTo extends PSEntity {
}
declare class PSDelay extends PSEntity {
    constructor(objectToAdd: PSEntity, inTime: number);
    update(clock: PSClock): void;
    private m_InTime;
    private m_InnerTimer;
    private m_ObjectToAdd;
}
interface IPSEaseFunction {
    (total: number, part: number): number;
}
declare class PSEaseFuncions {
    static lineal(total: number, part: number): number;
}
declare class PSScaleTo extends PSEntity {
    constructor(targetValue: number, inTime: number, easeFunc?: IPSEaseFunction);
    constructor(targetValue: PSPoint, inTime: number, easeFunc?: IPSEaseFunction);
    estroyOnFinish: boolean;
    destroyOnFinish: boolean;
    update(clock: PSClock): void;
    resetValues(targetValue: any, inTime: number, easeFunc?: IPSEaseFunction): void;
    private m_TargetValue;
    private m_InitialValue;
    private m_InTime;
    private m_InnerTimer;
    private m_EaseFunction;
    private m_DestroyOnFinish;
}
declare class PSCircle {
    constructor(_locationX: number, _locationY: number, _radius: number);
    intersectsPoint(other: PSPoint): boolean;
    intersectsCircle(other: PSCircle): boolean;
    intersectsRectangle(other: PSRectangle): boolean;
    radius: number;
    locationX: number;
    locationY: number;
}
declare class TileMapCell {
    id: number;
}
declare class TileMap extends PSEntity {
    static FORCE_CUSTOM_MESH: boolean;
    constructor(p?: TileMapDescriptor);
    constructor(p?: string);
    mapWidth: number;
    mapHeight: number;
    setTileMapDescriptor(resource: TileMapDescriptor): void;
    setTileMapDescriptor(resource: string): void;
    setup(): void;
    update(clock: PSClock): void;
    m_Descriptor: TileMapDescriptor;
    m_Layers: Array<TileMapLayer>;
    m_MapWidth: number;
    m_MapHeight: number;
    m_TileWidth: number;
    m_TileHeight: number;
    m_Parsed: boolean;
    m_Parsing: boolean;
}
declare class TileMapDescriptor implements IPSResource {
    constructor(path: string);
    info: any;
    load(callback: IPSLoadCallback): void;
    innerCallback(data: any): void;
    unload(callback: IPSLoadCallback): void;
    isLoaded(): boolean;
    getResourceName(): string;
    getLoaderCallback(): IPSLoadCallback;
    getTileSet(name: string): TileSetDescriptor;
    getGeneratedTexture(layerName: string): PSTexture;
    static getTileMapResource(resource: string): TileMapDescriptor;
    m_Path: string;
    m_Loaded: boolean;
    m_DescriptorLoaded: boolean;
    m_Loading: boolean;
    m_LoaderCallback: IPSLoadCallback;
    m_TileMapInfo: any;
    m_TileSets: {
        [index: string]: TileSetDescriptor;
    };
    m_GeneratedLayers: {
        [index: string]: PSTexture;
    };
}
declare class TileMapLayer extends PSEntity {
    cells: Array<TileMapCell>;
    type: string;
    mapWidth: number;
    mapHeight: number;
    visible: boolean;
    tileset: TileSetDescriptor;
    texture: PSTexture;
    vao: any;
    tileWidth: number;
    tileHeight: number;
    extraProperties: {
        [index: string]: string;
    };
    private m_TotalNumElements;
    private m_VertexData;
    constructor();
    createVAO(): void;
    setupGenerated(): void;
    render(support: PSRenderSupport, gl: WebGLRenderingContext): void;
}
declare class TileSetDescriptor {
    constructor(texture: PSTexture);
    tilesPerRow: number;
    texture: PSTexture;
    getCoords(index: number): any;
    getTexelCoords(index: number): any;
    getQuadCoords(index: number, output?: any): any;
    isLoaded(): boolean;
    m_Texture: PSTexture;
    tileWidth: number;
    tileHeight: number;
    margin: number;
    spacing: number;
}
declare class PSClock {
    static instance: PSClock;
    constructor();
    time: number;
    timeInMiliseconds: number;
    deltaTime: number;
    timeScale: number;
    fps: number;
    init(): void;
    startFrame(): void;
    endFrame(): void;
    private m_RealDeltaTime;
    private m_TimeScale;
    private m_LastFrameTimestamp;
    private m_StartFrameTimestamp;
    private m_FPS;
    private m_RealFPS;
    private static s_Instance;
}
/** END requestAnimationFrame SETUP */
declare class PSGame {
    static CURRENT_GAME: PSGame;
    static LOOP_FNC(): void;
    constructor();
    stage: PSEntity;
    currentCamera: PSCamera;
    canvas: HTMLCanvasElement;
    renderSupport: PSRenderSupport;
    clock: PSClock;
    input: PSInput;
    /** virtual */
    init(): void;
    /** virtual */
    loadResources(manager: PSResourceManager): void;
    /** virtual */
    setupScene(): void;
    /** virtual */
    update(clock: PSClock): void;
    /** virtual */
    render(support: PSRenderSupport, gl: WebGLRenderingContext): void;
    setupCanvas(canvasId?: string, w?: number, h?: number): void;
    requestFrame(): void;
    run(): void;
    private static runOnceLoaded(_game);
    startGameLoop(): void;
    m_ResourceManager: PSResourceManager;
    m_Stage: PSEntity;
    m_CurrentCamera: PSCamera;
    m_Canvas: HTMLCanvasElement;
    m_Input: PSInput;
    m_RenderSupport: PSRenderSupport;
    m_Clock: PSClock;
    m_ClearColor: PSColor;
    m_TempViewMatrix: PSMatrix;
    m_CurrentTouchEvent: PSTouchEvent;
    m_PostProcessPipeline: PSPostProcessPipeline;
}
declare class PSKeyMap {
    static TAB: number;
    static ENTER: number;
    static SHIFT: number;
    static CTRL: number;
    static ALT: number;
    static SPACE: number;
    static ARROW_LEFT: number;
    static ARROW_UP: number;
    static ARROW_RIGHT: number;
    static ARROW_DOWN: number;
    static DOT: number;
    static ZERO: number;
    static ONE: number;
    static TWO: number;
    static THREE: number;
    static FOUR: number;
    static FIVE: number;
    static SIX: number;
    static SEVEN: number;
    static EIGHT: number;
    static NINE: number;
    static A: number;
    static B: number;
    static C: number;
    static D: number;
    static E: number;
    static F: number;
    static G: number;
    static H: number;
    static I: number;
    static J: number;
    static K: number;
    static L: number;
    static M: number;
    static N: number;
    static O: number;
    static P: number;
    static Q: number;
    static R: number;
    static S: number;
    static T: number;
    static U: number;
    static V: number;
    static W: number;
    static X: number;
    static Y: number;
    static Z: number;
}
declare class PSInput {
    constructor();
    static instance: PSInput;
    static mouseX: number;
    static mouseY: number;
    static mousePreviousX: number;
    static mousePreviousY: number;
    static mouseDown: boolean;
    static mousePreviousDown: boolean;
    static mouseClick: boolean;
    static mouseRelease: boolean;
    static mouseMoving: boolean;
    static isKeyPressed(keycode: number): boolean;
    static isKeyJustPressed(keycode: number): boolean;
    static isKeyJustReleased(keycode: number): boolean;
    static isMobileDevice(): boolean;
    mouseX: number;
    mouseY: number;
    mousePreviousX: number;
    mousePreviousY: number;
    mouseDown: boolean;
    mousePreviousDown: boolean;
    mouseClicked: boolean;
    mouseReleased: boolean;
    mouseMoving: boolean;
    isKeyPressed(keycode: number): boolean;
    isMobileDevice(): boolean;
    init(): void;
    static handleEventCaller(e: Event): boolean;
    static windowMouseEventHandler(e: Event): void;
    updateInput(stage: PSEntity, touchEvent: PSTouchEvent): void;
    updateTraversal(entity: PSEntity, touchEvent: PSTouchEvent): void;
    handleEvent(e: Event): boolean;
    getTouch(touchIndex: number, pEvent: any): any;
    onMouseDown(pe: any): boolean;
    onMouseUp(pe: Event): boolean;
    onKeyDown(event: any): boolean;
    onKeyUp(event: any): boolean;
    onMouseMove(pe: Event): boolean;
    getTransformedMousePoint(targetPoint?: PSPoint): PSPoint;
    onFocusIn(): void;
    onFocusOut(): void;
    private m_MobileDevice;
    private m_PressedKeys;
    private m_JustReleasedKeys;
    private m_JustPressedKeys;
    private m_EventMouseLocation;
    private m_MouseLocation;
    private m_MousePreviousLocation;
    private m_MouseDown;
    private m_MousePreviousDown;
    private m_EventMouseDown;
    private m_HaveFocus;
    private static s_Instance;
}
declare class PSScreen {
    static width: number;
    static height: number;
    static center: PSPoint;
}
declare class PSTimer extends PSEntity {
    duration: number;
    timeSinceStart: number;
    started: boolean;
    finished: boolean;
    start(duration: number): void;
    resetValues(): void;
    getDelta(easeFunc?: IPSEaseFunction): number;
    update(clock: PSClock): void;
    private m_Duration;
    private m_TimeSinceStart;
    private m_Started;
}
