attribute vec2 aVertexPosition;
attribute vec2 aTextureCoord;
uniform mat3 uMVPMatrix;
varying vec2 vTextureCoord;
void main(void){
	vTextureCoord = aTextureCoord;
	gl_Position = vec4((uMVPMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);
}