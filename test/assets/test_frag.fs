precision mediump float;
uniform sampler2D uSampler;
uniform mat3 uTMatrix;
uniform vec4 uColor;
varying vec2 vTextureCoord;
void main(void){
	vec4 color = texture2D(uSampler, (uTMatrix * vec3(vTextureCoord, 1.0)).xy);
	gl_FragColor = color * uColor;
}