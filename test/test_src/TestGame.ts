class TestGame extends PSGame
{
	init() : void
	{
		this.setupCanvas('testCanvas');
		this.m_Timer = 0;
		this.m_PostProcessPipeline = new PSPostProcessPipeline(PSBuiltInGfx.PS_GLOW_EFFECT);
		this.m_PostProcessPipeline.init(this.m_RenderSupport, this.m_RenderSupport.m_GLContext, PSScreen.width, PSScreen.height, 2);
	}

	loadResources(manager : PSResourceManager) : void
	{
		manager.verbose = true;
		
		this.m_PolloTexture = manager.loadResource(new PSTexture('assets/pollo.png'));

		//var stdVert = new PSShaderSource('assets/test_vert.vs', EShaderSourceType.VERTEX_SHADER);
		//var stdFrag = new PSShaderSource('assets/test_frag.fs', EShaderSourceType.FRAGMENT_SHADER);
		//this.m_TestProgram = new PSShaderProgram('TestProgram');
		//this.m_TestProgram.attachSource(stdVert);
		//this.m_TestProgram.attachSource(stdFrag);
		//manager.loadResource(stdVert);
		//manager.loadResource(stdFrag);
		//manager.loadResource(this.m_TestProgram);
		
	}

	setupScene() : void
	{
		this.m_ClearColor.setValues(0.0, 0.0, 0.1, 1.0);
		this.m_Sprite = new PSSprite('assets/pollo.png');
		this.m_Stage.addChild(this.m_Sprite);
		this.m_Sprite.transform.width = 256;
		this.m_Sprite.transform.height = 256;
		//this.m_Sprite.transform.pivot = new PSPoint(0.5, 0.5);
		this.m_Sprite.transform.location.x = 320;
		this.m_Sprite.transform.location.y = 200;

		this.m_Sprite.transform.scale = 1;

		var quad = new PSSprite();
		quad.x = 50;
		quad.y = 0;
		quad.width = 40;
		quad.height = 40;
		quad.color.setValues(0.0, 1.0, 0.0, 0.3);
		quad.transform.pivot = new PSPoint(0.5, 0.5);
		quad.addChild(new PSScaleTo(3.0, 5.0))
		this.m_Sprite.addChild(quad);

		this.m_Quad = quad;



		var text = new PSTextSprite('Hello world :P');
		text.x = 20;
		text.y = 40;
		this.m_Text = text;
		this.m_Stage.addChild(this.m_Text);
	}

	update(clock : PSClock) : void
	{
		super.update(clock);
		this.m_Sprite.rotation += clock.deltaTime;
		//this.m_Quad.rotation -= 1.5* clock.deltaTime;
		this.m_Quad.rotation += Math.sin(clock.time)*10*clock.deltaTime;

		if(PSInput.isKeyPressed(PSKeyMap.D))
		{
			this.m_Sprite.x += 100 * clock.deltaTime;
		}

		if(PSInput.isKeyPressed(PSKeyMap.A))
		{
			this.m_Sprite.x -= 100 * clock.deltaTime;
		}
	}

	render(support : PSRenderSupport, gl : WebGLRenderingContext) : void
	{
		super.render(support, gl);
	}

	private m_PolloTexture : PSTexture;
	private m_TestProgram : PSShaderProgram;
	private m_Timer : number;

	private m_Sprite : PSSprite;
	private m_Quad : PSSprite;

	private m_Text : PSTextSprite;
};