#!/bin/bash

find ../test_src -type f \( -name "*.ts" \) > args.txt
tsc -t ES5 --out ../test.js ../../lib/poollos.d.ts @args.txt