#!/bin/bash

find ../src -type f \( -name "*.ts" \) > args.txt
tsc -d -t ES5 --out ../lib/poollos.js @args.txt