/// <reference path="../display/Entity.ts"/>

class PSScaleTo extends PSEntity
{
	constructor(targetValue : number, inTime : number, easeFunc? : IPSEaseFunction);
	constructor(targetValue : PSPoint, inTime : number, easeFunc? : IPSEaseFunction);
	constructor(targetValue : any, inTime : number, easeFunc : IPSEaseFunction=PSEaseFuncions.lineal)
	{
		super();

		this.m_InnerTimer = new PSTimer();
		this.addChild(this.m_InnerTimer);
		this.m_EaseFunction = easeFunc;

		this.m_InTime = inTime;

		this.m_TargetValue = PSPool.alloc(PSPoint);

		if(targetValue instanceof PSPoint)
		{
			this.m_TargetValue.setFrom(targetValue);
		}
		else
		{
			this.m_TargetValue.x = targetValue;
			this.m_TargetValue.y = targetValue;
		}
	}

	get estroyOnFinish() : boolean { return this.m_DestroyOnFinish; }
	set destroyOnFinish(d : boolean){ this.m_DestroyOnFinish = d; }

	update(clock : PSClock) : void
	{

		if(!this.m_InnerTimer.started)
		{
			this.m_InitialValue = PSPool.alloc(PSPoint).setValues(this.m_Parent.scaleX, this.m_Parent.scaleY);
			this.m_InnerTimer.start(this.m_InTime);
		}

		if(!this.m_InnerTimer.finished)
		{
			var delta = this.m_InnerTimer.getDelta(this.m_EaseFunction);
			var deltaValueX = (this.m_InitialValue.x * (1.0 - delta)) + (this.m_TargetValue.x * delta);
			var deltaValueY = (this.m_InitialValue.y * (1.0 - delta)) + (this.m_TargetValue.y * delta);

			this.m_Parent.scaleX = deltaValueX;
			this.m_Parent.scaleY = deltaValueY;
		}
		else
		{
			this.m_Parent.scaleX = this.m_TargetValue.x;
			this.m_Parent.scaleY = this.m_TargetValue.y;
			PSPool.free(this.m_InitialValue);
			if(this.m_DestroyOnFinish)
			{

				this.destroy();
			}
		}		
	}

	resetValues(targetValue : any, inTime : number, easeFunc : IPSEaseFunction=PSEaseFuncions.lineal) : void
	{
		this.m_InnerTimer.resetValues();
		this.m_EaseFunction = easeFunc;
		this.m_InTime = inTime;

		if(targetValue instanceof PSPoint)
		{
			this.m_TargetValue.setFrom(targetValue);
		}
		else
		{
			this.m_TargetValue.x = targetValue;
			this.m_TargetValue.y = targetValue;
		}
	}


	private m_TargetValue : PSPoint;
	private m_InitialValue : PSPoint = null;

	private m_InTime : number;
	private m_InnerTimer : PSTimer;
	private m_EaseFunction : IPSEaseFunction;

	private m_DestroyOnFinish : boolean = true;
}