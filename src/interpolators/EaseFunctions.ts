
interface IPSEaseFunction
{
	(total : number, part : number) : number;
}

class PSEaseFuncions
{
	static lineal(total : number, part : number) : number
	{
		return part / total;
	}
}