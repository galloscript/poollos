/// <reference path="../display/Entity.ts"/>

class PSAlphaTo extends PSEntity
{
	constructor(targetValue : number, inTime : number, easeFunc : IPSEaseFunction=PSEaseFuncions.lineal)
	{
		super();

		this.m_InnerTimer = new PSTimer();
		this.addChild(this.m_InnerTimer);
		this.m_EaseFunction = easeFunc;

		this.m_InTime = inTime;

		this.m_TargetValue = targetValue;

	}

	get estroyOnFinish() : boolean { return this.m_DestroyOnFinish; }
	set destroyOnFinish(d : boolean){ this.m_DestroyOnFinish = d; }

	update(clock : PSClock) : void
	{

		if(!this.m_InnerTimer.started)
		{
			this.m_InitialValue = this.m_Parent.alpha;
			this.m_InnerTimer.start(this.m_InTime);
		}

		if(!this.m_InnerTimer.finished)
		{
			var delta = this.m_InnerTimer.getDelta(this.m_EaseFunction);
			var deltaValue = (this.m_InitialValue * (1.0 - delta)) + (this.m_TargetValue * delta);

			this.m_Parent.alpha = deltaValue;
		}
		else
		{
			this.m_Parent.alpha = this.m_TargetValue;
			if(this.m_DestroyOnFinish)
			{
				this.destroy();
			}
		}
	}

	resetValues(targetValue : number, inTime : number, easeFunc : IPSEaseFunction=PSEaseFuncions.lineal) : void
	{
		this.m_InnerTimer.resetValues();

		this.m_EaseFunction = easeFunc;
		this.m_InTime = inTime;
		this.m_TargetValue = targetValue;
	}

	private m_TargetValue : number;
	private m_InitialValue : number = 0;

	private m_InTime : number;
	private m_InnerTimer : PSTimer;
	private m_EaseFunction : IPSEaseFunction;

	private m_DestroyOnFinish : boolean = true;
}