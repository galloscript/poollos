/// <reference path="../display/Entity.ts"/>

class PSDelay extends PSEntity
{
	constructor(objectToAdd : PSEntity, inTime : number)
	{
		super();

		this.m_InnerTimer = new PSTimer();
		this.addChild(this.m_InnerTimer);

		this.m_InTime = inTime;

		this.m_ObjectToAdd = objectToAdd;
	}

	update(clock : PSClock) : void
	{
		if(!this.m_InnerTimer.started)
		{
			this.m_InnerTimer.start(this.m_InTime);
		}

		if(this.m_InnerTimer.finished)
		{
			this.m_Parent.addChild(this.m_ObjectToAdd);
			this.destroy();
		}
	}

	private m_InTime : number;
	private m_InnerTimer : PSTimer;
	private m_ObjectToAdd : PSEntity;
}