
class TileMapCell
{
	public id : number;
}

class TileMap extends PSEntity
{
	public static FORCE_CUSTOM_MESH : boolean = false;

	constructor(p? : TileMapDescriptor);
	constructor(p? : string);
	constructor(p? : any)
	{
		super();

		if(p != null)
		{
			this.setTileMapDescriptor(p);
		}
	}

	get mapWidth() : number { return this.m_MapWidth * this.m_TileWidth; }
	get mapHeight() : number { return this.m_MapHeight * this.m_TileHeight; }

	setTileMapDescriptor(resource : TileMapDescriptor) : void;
	setTileMapDescriptor(resource : string) : void;
	setTileMapDescriptor(resource : any) : void
	{
		this.m_Parsed = false;

		if(typeof resource == 'string')
		{
			this.m_Descriptor = PSResourceManager.instance.getResource(resource);

			if(this.m_Descriptor == null)
			{
				this.m_Descriptor = PSResourceManager.instance.loadResource(new TileMapDescriptor(resource));
			}
		}
		else if(resource instanceof TileMapDescriptor)
		{
			this.m_Descriptor = resource;
		}
	}

	setup() : void
	{
		//setup the tilemap if we need
		if(!this.m_Parsing && !this.m_Parsed && this.m_Descriptor != null && this.m_Descriptor.isLoaded())
		{
			this.m_Parsing = true;
			var info : any = this.m_Descriptor.info;

			this.m_MapWidth = info.width;
			this.m_MapHeight = info.height;
			this.m_TileWidth = info.tilewidth;
			this.m_TileHeight = info.tileheight;
			
			this.m_Layers = new Array();
			var totalLayers = info.layers.length;

			for(var layerIndex = 0; layerIndex < totalLayers; ++layerIndex)
			{
				var layerInfo = info.layers[layerIndex];

				if(!layerInfo.properties.tileset)
				{
					console.error('Error parsing layer "'+ layerInfo.name+'" :: POOLLOS TileMap implementation '+
							  	  'require a custom layer attribute called "tileset" '+
								  'specifiying the name of the tileset that is being used for this layer. '+
								  'Only one tileset per layer is supported.');

					return;
				}

				var newLayer = new TileMapLayer();
				newLayer.mapWidth = layerInfo.width;
				newLayer.mapHeight = layerInfo.height;
				newLayer.type = layerInfo.type;
				newLayer.name = layerInfo.name;
				newLayer.tileset = this.m_Descriptor.getTileSet(layerInfo.properties.tileset);
				
				for(var prop in layerInfo.properties)
				{
					newLayer.extraProperties[prop] = layerInfo.properties[prop];
				}

				newLayer.tileWidth = info.tilewidth;
				newLayer.tileHeight = info.tileheight;

				if(newLayer.type == 'tilelayer')
				{
					newLayer.cells = new Array(newLayer.mapWidth * newLayer.mapHeight);
					var totalCells = newLayer.cells.length;
					
					for(var cellIndex = 0; cellIndex < totalCells; ++cellIndex)
					{
						var newCell = new TileMapCell();
						newCell.id = layerInfo.data[cellIndex];
						newLayer.cells[cellIndex] = newCell;
					}
					this.m_Layers[layerIndex] = newLayer;
					this.addChild(newLayer);	
					var generatedMap = this.m_Descriptor.getGeneratedTexture(newLayer.name);

					if(generatedMap != null && !TileMap.FORCE_CUSTOM_MESH)
					{
						console.log('Using pregenerated image for '+newLayer.name)+' layer';
						newLayer.texture =  generatedMap;
						newLayer.width = generatedMap.width;
						newLayer.height = generatedMap.height;
						newLayer.setupGenerated();
					}
					else
					{
						console.log('Using custom mesh for '+this.name);
						newLayer.texture = newLayer.tileset.texture;
						newLayer.createVAO();
					}
				}
			}

			this.m_Parsing = false;
			this.m_Parsed = true;
		}
	}

	update(clock : PSClock) : void
	{
		//will setup the tile map info
		this.setup();
	}
	
	public m_Descriptor : TileMapDescriptor = null;
	public m_Layers : Array<TileMapLayer> = null;

	public m_MapWidth : number = 0;
	public m_MapHeight : number = 0;
	public m_TileWidth : number = 0;
	public m_TileHeight : number = 0;

	public m_Parsed : boolean = false;
	public m_Parsing : boolean = false;
}
