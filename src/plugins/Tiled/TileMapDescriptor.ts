
class TileMapDescriptor implements IPSResource
{
	constructor(path : string)
	{
		this.m_Path = path;
	}

	get info() : any { return this.m_TileMapInfo; }

	load(callback : IPSLoadCallback) : void
	{
		if(this.m_Loaded)
			return;

		if(this.m_Loading)
		{
			if(this.m_DescriptorLoaded)
			{
				var loaded = true;
		
				for (var tileMapName in this.m_TileSets) 
				{
					loaded = loaded && this.m_TileSets[tileMapName].isLoaded();
					if(!loaded)
					{
						return;
					}
				}

				for (var generatedLayer in this.m_GeneratedLayers) 
				{
					loaded = loaded && this.m_GeneratedLayers[generatedLayer].isLoaded();
					if(!loaded)
					{
						return;
					}
				}

				this.m_Loaded = loaded;
				if(this.m_Loaded)
				{
					if(this.m_LoaderCallback != null)
				    {
						this.m_LoaderCallback(this);
					}
				}
			}

			return;
		}
			

		this.m_Loading = true;
		this.m_LoaderCallback = callback;

		this.m_TileMapInfo = null;
		this.m_TileSets = {}; 
		
		var jsonFileRequest = new XMLHttpRequest();
		jsonFileRequest['m_JSONAsset'] = this;
		jsonFileRequest.open("GET", this.m_Path, true);
		jsonFileRequest.responseType = "json";
		jsonFileRequest.onload = function() 
		{
			this['m_JSONAsset'].innerCallback(this.response);
		};
		
		jsonFileRequest.send();
	}

	innerCallback(data) : void
	{
		//TODO parse json

		this.m_TileMapInfo = (typeof data == 'string') ? JSON.parse(data) : data;

		//TODO load texture map

		var assetDirectory : string = this.m_Path.replace(/[^\/]*$/, '');

		//PSTexture.getTextureResource(path);
		var tilesets : Array<Object> = this.m_TileMapInfo.tilesets;
		for(var i = 0; i < tilesets.length; ++i)
		{
			var tileset : any = tilesets[i];
			
			var newTileset : TileSetDescriptor = new TileSetDescriptor(PSTexture.getTextureResource(assetDirectory+tileset.image));

			newTileset.tileWidth = tileset.tilewidth;
			newTileset.tileHeight = tileset.tileheight;
			newTileset.margin = tileset.margin;
			newTileset.spacing = tileset.spacing;

			this.m_TileSets[tileset.name] = newTileset;
		}

		var properties = this.m_TileMapInfo.properties;
		for(var property in properties)
		{
			if(property.indexOf('generated_') == 0)
			{
				var texName : string = properties[property];
				var layerName : string = property.replace('generated_','');
				this.m_GeneratedLayers[layerName] = PSTexture.getTextureResource(assetDirectory+texName);
			}
		}

	    this.m_DescriptorLoaded = true;
	}

	unload(callback : IPSLoadCallback) : void
	{
		this.m_LoaderCallback = callback;
	}

	isLoaded() : boolean
	{
		return this.m_Loaded;
	}

	getResourceName() : string
	{
		return this.m_Path;
	}

	getLoaderCallback() : IPSLoadCallback
	{
		return this.m_LoaderCallback;
	}

	getTileSet(name : string) : TileSetDescriptor
	{
		return this.m_TileSets[name];
	}

	getGeneratedTexture(layerName : string) : PSTexture
	{
		return this.m_GeneratedLayers[layerName];
	}

	static getTileMapResource(resource : string) : TileMapDescriptor
	{
		var res : any = PSResourceManager.instance.getResource(resource);

		if(res == null)
		{
			res = PSResourceManager.instance.loadResource(new TileMapDescriptor(resource));
		}

		return res;
	}

	public m_Path : string = '';
	public m_Loaded : boolean = false;
	public m_DescriptorLoaded : boolean = false;
	public m_Loading : boolean = false;
	public m_LoaderCallback : IPSLoadCallback;

	public m_TileMapInfo : any = null;
	public m_TileSets : { [index: string] : TileSetDescriptor }  = {};
	public m_GeneratedLayers : { [index: string] : PSTexture } = {};
}
