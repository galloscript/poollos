
class TileSetDescriptor
{
	constructor(texture : PSTexture)
	{
		this.m_Texture = texture;
	}

	get tilesPerRow() : number
	{
		return Math.floor( (this.m_Texture.sourceWidth - (this.margin * 2)) / this.tileWidth );
	}

	get texture() : PSTexture
	{
		return this.m_Texture;
	}

	public getCoords(index : number) : any
	{
		var tilesPerRow = this.tilesPerRow;
		var r = Math.floor((index - 1) / tilesPerRow);
		var c = (index - 1) - (r * tilesPerRow);

		return {column : c, row : r}
	}

	public getTexelCoords(index : number) : any
	{
		var mapCoords : any = this.getCoords(index);

		var mapCoordX = this.margin + (mapCoords.column * (this.tileWidth + this.spacing));
		var mapCoordY = this.margin + (mapCoords.row * (this.tileHeight + this.spacing));

		return {x : mapCoordX, y : mapCoordY};
	}

	public getQuadCoords(index : number, output? : any) : any
	{
		output = output || {};
		var texelCoords : any = this.getTexelCoords(index);

		var addedMargin = 0.0;
		var leftX = (texelCoords.x + addedMargin) / this.m_Texture.sourceWidth;
		var rightX = (texelCoords.x + this.tileWidth - addedMargin) / this.m_Texture.sourceWidth;
		var upperY = (texelCoords.y + addedMargin) / this.m_Texture.sourceHeight;
		var bottomY = (texelCoords.y + this.tileHeight - addedMargin) / this.m_Texture.sourceHeight;

		output.upperLeft = {};
		output.upperRight = {};
		output.bottomLeft = {};
		output.bottomRight = {};

		output.upperLeft.x = leftX;
		output.upperLeft.y = upperY;
		output.upperRight.x = rightX;
		output.upperRight.y = upperY;
		output.bottomLeft.x = leftX;
		output.bottomLeft.y = bottomY;
		output.bottomRight.x = rightX;
		output.bottomRight.y = bottomY;

		return output;
	}

	public isLoaded() : boolean
	{
		return this.m_Texture.isLoaded();
	}

	public m_Texture : PSTexture;

	public tileWidth : number;
	public tileHeight : number;
	public margin : number;
	public spacing : number;
}
