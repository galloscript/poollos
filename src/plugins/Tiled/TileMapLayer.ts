class TileMapLayer extends PSEntity
{
	public cells : Array<TileMapCell>;
	public type : string;
	public mapWidth : number;
	public mapHeight : number;
	public visible : boolean;
	public tileset : TileSetDescriptor;
	public texture : PSTexture;
	public vao : any;

	public tileWidth : number;
	public tileHeight : number;

	public extraProperties : { [index: string] : string } = {};

	private m_TotalNumElements : number;
	private m_VertexData : Float32Array;

	constructor()
	{
		super();
	}

	createVAO() : void
	{
		var gl = PSGame.CURRENT_GAME.renderSupport.renderContext;

		this.m_TotalNumElements = this.mapWidth * this.mapHeight * 6;
		this.m_VertexData = new Float32Array(this.m_TotalNumElements * 4); // 4 x GL_FLOAT per element

		var output : any = {}; //store the quad coords output

		for(var cellIndex = 0; cellIndex < this.cells.length; ++cellIndex)
		{
			var cell = this.cells[cellIndex];

			this.tileset.getQuadCoords(cell.id, output);

			var startY = Math.floor(cellIndex / this.mapWidth);
			var startX = cellIndex - (startY * this.mapWidth);

			startX *= this.tileWidth;
			startY *= this.tileHeight;

			var data = new Float32Array([
				//	POS  	  						UV
				startX, startY, 									output.upperLeft.x, output.upperLeft.y,
				startX + this.tileWidth, startY, 					output.upperRight.x, output.upperRight.y,
				startX + this.tileWidth, startY + this.tileHeight, 	output.bottomRight.x, output.bottomRight.y,
				startX, startY, 									output.upperLeft.x, output.upperLeft.y,
				startX + this.tileWidth, startY + this.tileHeight, 	output.bottomRight.x, output.bottomRight.y,
				startX, startY + this.tileHeight, 					output.bottomLeft.x, output.bottomLeft.y
			]);
			
			var arrayStart = cellIndex * data.length; //TODO calc array start
			for(var dataIndex = 0; dataIndex < data.length; ++dataIndex)
			{
				this.m_VertexData[arrayStart + dataIndex] = data[dataIndex];
			}
		}

		var vao = gl['ext'].vao.createVertexArrayOES();
		gl['ext'].vao.bindVertexArrayOES(vao);
		var vbo = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
		gl.bufferData(gl.ARRAY_BUFFER, this.m_VertexData, gl.STATIC_DRAW);

		gl.vertexAttribPointer(PSBuiltInAttributes.POSITION.location, 2, gl.FLOAT, false, 16, 0);
		gl.vertexAttribPointer(PSBuiltInAttributes.TEXCOORD.location, 2, gl.FLOAT, false, 16, 8);

		gl.enableVertexAttribArray(PSBuiltInAttributes.POSITION.location);
		gl.enableVertexAttribArray(PSBuiltInAttributes.TEXCOORD.location);

		//TODO: disable other vertex attribues if needed

		gl['ext'].vao.bindVertexArrayOES(null);

		this.vao = vao;
	}

	setupGenerated() : void
	{
		this.vao = PSBuiltInGfx.PS_DEFAULT_QUAD_VAO;
		this.m_TotalNumElements = 6;
	}

	render(support : PSRenderSupport, gl : WebGLRenderingContext) : void
	{
		var material : PSBuiltInSpriteMaterial = PSBuiltInGfx.PS_DEFAULT_MATERIAL;
		var meshVAO = this.vao;

		material.uSampler = this.texture;
		material.uVPMatrix = support.m_CurrentVPMatrix;
		material.uModelMatrix = support.m_CurrentModelMatrix;
		material.uColor = support.m_CurrentColor;
		material.uTMatrix = this.texture.textureMatrix; 

		material.apply(support);

		gl['ext'].vao.bindVertexArrayOES(meshVAO);
		gl.drawArrays(gl.TRIANGLES, 0, this.m_TotalNumElements);
		gl['ext'].vao.bindVertexArrayOES(null);
	}
}