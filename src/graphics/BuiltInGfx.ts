/// <reference path="./Material.ts"/>

class PSBuiltInGfx
{
	static load() : void 
	{
		var gl = PSGame.CURRENT_GAME.renderSupport.renderContext;

		//setup aux 8x8 texture
		var aux2DCanvas = document.createElement('canvas');
		var aux2DContext : CanvasRenderingContext2D = <CanvasRenderingContext2D>aux2DCanvas.getContext('2d');
		aux2DCanvas.width = aux2DCanvas.height = 8;
		aux2DContext.fillStyle = '#ffffff';
		aux2DContext.fillRect(0, 0, 8, 8);
		//'BuiltIn8x8WhiteTexture'
		var aux8x8Texture = new PSTexture(aux2DCanvas.toDataURL());
		aux8x8Texture.load(
			function(res : IPSResource)
			{
				//hacking resource indexing
				PSResourceManager.instance.setResourceKey('BuiltIn8x8WhiteTexture', res);
			});

		PSBuiltInGfx.PS_DEFAULT_TEXTURE = aux8x8Texture;

		//setup standard shader program
		var stdVert = new PSShaderSource('BuiltInShaderSrc-Standard-Vert', EShaderSourceType.VERTEX_SHADER);
		var stdFrag = new PSShaderSource('BuiltInShaderSrc-Standard-Frag', EShaderSourceType.FRAGMENT_SHADER);
		var stdProg = new PSShaderProgram('BuiltInShaderProg-Standard');
		PSBuiltInGfx.PS_DEFAULT_SHADER = stdProg;

		stdVert.compileSource(PSBuiltInGfx.PS_DEFAULT_SHADER_VERT_SRC);
		stdFrag.compileSource(PSBuiltInGfx.PS_DEFAULT_SHADER_FRAG_SRC);
		stdProg.attachSource(stdVert);
		stdProg.attachSource(stdFrag);	
		PSResourceManager.instance.loadResource(stdProg);

		//TODO move to mesh class
		//setup standard VAO
		var vao = gl['ext'].vao.createVertexArrayOES();
		PSBuiltInGfx.PS_DEFAULT_QUAD_VAO = vao;
		gl['ext'].vao.bindVertexArrayOES(vao);

		var vertices = new Float32Array([
		//	POS  	  	UV
			0.0, 0.0, 	0.0, 0.0,
			1.0, 0.0, 	1.0, 0.0,
			1.0, 1.0, 	1.0, 1.0,
			0.0, 0.0, 	0.0, 0.0,
			1.0, 1.0, 	1.0, 1.0,
			0.0, 1.0, 	0.0, 1.0
		]);

		var vbo = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

		gl.vertexAttribPointer(PSBuiltInAttributes.POSITION.location, 2, gl.FLOAT, false, 16, 0);
		gl.vertexAttribPointer(PSBuiltInAttributes.TEXCOORD.location, 2, gl.FLOAT, false, 16, 8);

		gl.enableVertexAttribArray(PSBuiltInAttributes.POSITION.location);
		gl.enableVertexAttribArray(PSBuiltInAttributes.TEXCOORD.location);

		//TODO: disable other vertex attribues if needed

		gl['ext'].vao.bindVertexArrayOES(null);

		//setup standard material

		PSBuiltInGfx.PS_DEFAULT_MATERIAL = new PSBuiltInSpriteMaterial('BuiltInSpriteMaterial', PSBuiltInGfx.PS_DEFAULT_SHADER);
		//PSBuiltInGfx.PS_DEFAULT_MATERIAL.loadUniforms(gl);
		PSResourceManager.instance.loadResource(PSBuiltInGfx.PS_DEFAULT_MATERIAL);


		//GLOW Effect
		var glowVert = new PSShaderSource('BuiltInShaderSrc-Null-Vert', EShaderSourceType.VERTEX_SHADER);
		var glowFrag = new PSShaderSource('BuiltInShaderSrc-Glow-Frag', EShaderSourceType.FRAGMENT_SHADER);
		var glowProg = new PSShaderProgram('BuiltInShaderProg-Glow');
		PSBuiltInGfx.PS_GLOW_SHADER = glowProg;

		glowVert.compileSource(PSBuiltInGfx.PS_NULL_SHADER_VERT_SRC);
		glowFrag.compileSource(PSBuiltInGfx.PS_GLOW_SHADER_FRAG_SRC);
		glowProg.attachSource(glowVert);
		glowProg.attachSource(glowFrag);	
		PSResourceManager.instance.loadResource(glowProg);


		PSBuiltInGfx.PS_GLOW_EFFECT = new PSBuiltInGlowEffect('BuiltInGlowEffect', PSBuiltInGfx.PS_GLOW_SHADER);
		PSResourceManager.instance.loadResource(PSBuiltInGfx.PS_GLOW_EFFECT);
	}

	public static PS_DEFAULT_SHADER : PSShaderProgram;
	public static PS_GLOW_SHADER : PSShaderProgram;

	 /** WebGLVertexArrayObjectOES */
	public static PS_DEFAULT_QUAD_VAO : any;

	/** WebGLBuffer */
	public static PS_DEFAULT_QUAD_VBO : any;

	public static PS_DEFAULT_MATERIAL : PSBuiltInSpriteMaterial;
	public static PS_GLOW_EFFECT : PSBuiltInGlowEffect;

	public static PS_DEFAULT_TEXTURE : PSTexture;

	public static PS_DEFAULT_SHADER_VERT_SRC = ''+
		'attribute vec2 aVertexPosition;'+
		'attribute vec2 aTextureCoord;'+
		'uniform mat3 uMVPMatrix;'+
		'varying vec2 vTextureCoord;'+
		'void main(void){'+
		'	vTextureCoord = aTextureCoord;'+
		'	gl_Position = vec4((uMVPMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);'+
		'}'+
	'';

	public static PS_DEFAULT_SHADER_FRAG_SRC = ''+
		'precision mediump float;'+
		'uniform sampler2D uSampler;'+
		'uniform mat3 uTMatrix;'+
		'uniform vec4 uColor;'+
		'varying vec2 vTextureCoord;'+
		'void main(void){'+
		'	vec4 color = texture2D(uSampler, (uTMatrix * vec3(vTextureCoord, 1.0)).xy);'+
		'	gl_FragColor = color * uColor;'+
		//'	gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);'+
		'}'+
	'';

	public static PS_NULL_SHADER_VERT_SRC = ''+
		'precision mediump float;\n'+
		'attribute vec2 aVertexPosition;'+
		'attribute vec2 aTextureCoord;'+
		'uniform mat3 uMVPMatrix;'+
		'varying vec2 vTextureCoord;'+
		'void main(void){'+
		'	vTextureCoord = aTextureCoord;'+
		'	gl_Position = vec4((uMVPMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);'+
		'}'+
	'';

	//TODO: http://wp.applesandoranges.eu/?p=14

	public static PS_GLOW_SHADER_FRAG_SRC = ''+
		'precision mediump float;\n'+
		'uniform sampler2D uSampler;\n'+
		'uniform vec2 uScreenSize;\n'+
		'uniform int uPass;\n'+
		'varying vec2 vTextureCoord;\n'+
		'void main(void){\n'+
		'	vec2 pixelSize = 1.1 / vec2(uScreenSize.x, uScreenSize.y);\n'+
		'	vec2 pos = gl_FragCoord.xy / (uScreenSize * 1.0);\n'+
		'	float values[25];\n'+	
		'	values[0]=0.005;\n'+
		'	values[1]=0.01;\n'+
		'	values[2]=0.015;\n'+
		'	values[3]=0.02;\n'+
		'	values[4]=0.03;\n'+
		'	values[5]=0.04;\n'+
		'	values[6]=0.05;\n'+
		'	values[7]=0.06;\n'+
		'	values[8]=0.07;\n'+
		'	values[9]=0.08;\n'+
		'	values[10]=0.10;\n'+
		'	values[11]=0.11;\n'+
		'	values[12]=0.35;\n'+
		'	values[13]=0.11;\n'+
		'	values[14]=0.10;\n'+
		'	values[15]=0.08;\n'+
		'	values[16]=0.07;\n'+
		'	values[17]=0.06;\n'+
		'	values[18]=0.05;\n'+
		'	values[19]=0.04;\n'+
		'	values[20]=0.03;\n'+
		'	values[21]=0.02;\n'+
		'	values[22]=0.015;\n'+
		'	values[23]=0.01;\n'+
		'	values[24]=0.005;\n'+

		'	vec4 result;\n'+
		'	vec2 curSamplePos;\n'+
		'					  \n'+ 
		'	if(uPass == 0){\n'+
		'		curSamplePos=vec2(pos.x, pos.y - 11.0 * pixelSize.y);\n'+
		'		for(int i=0;i<25;i++)\n'+
		'		{\n'+
		'			vec4 tempCol = texture2D(uSampler, curSamplePos);'+
		'			result += tempCol * values[i];\n'+
		'			curSamplePos.y += pixelSize.y;\n'+
		'		}\n'+
		//'		result += texture2D(uSampler, pos);\n'+
		'	} else {'+ //if(uPass == 0  uPass == 1){\n'+
		'		curSamplePos = vec2(pos.x - 11.0 * pixelSize.x, pos.y);\n'+
		'		for(int i=0;i<25;i++)\n'+ 
		'		{\n'+
		'			vec4 tempCol = texture2D(uSampler, curSamplePos);'+
		'			result += tempCol * values[i];\n'+
		'			curSamplePos.x += pixelSize.x;\n'+
		'		}'+
		//'		result += texture2D(uSampler, pos);\n'+
		//'	} else if(uPass == 2){\n'+
		//'		result = texture2D(uSampler, pos);\n'+
		'	}\n'+
		'	gl_FragColor = vec4(result.xyz, 1.0);\n'+
		'}\n'+
	'';
}

class PSBuiltInSpriteMaterial extends PSMaterial
{
	constructor(name : string, program : PSShaderProgram)
	{
		super(name, program);
	}

	loadUniforms(gl : WebGLRenderingContext) : void
	{
		var prog = this.m_ShaderProgram;
		this.m_uSamplerLocation = gl.getUniformLocation(prog.programId, 'uSampler');
		this.m_uMVPMatrixLocation = gl.getUniformLocation(prog.programId, 'uMVPMatrix');
		this.m_uColorLocation = gl.getUniformLocation(prog.programId, 'uColor');
		this.m_uTMatrixLocation = gl.getUniformLocation(prog.programId, 'uTMatrix');
	}

	apply(support : PSRenderSupport) : void 
	{ 
		this.releasePrevious(support);

		var gl = support.renderContext;
		var prog = this.m_ShaderProgram;

		prog.use(gl);

		var mvp = new PSMatrix(this.uModelMatrix);
		mvp.concat(this.uVPMatrix);

		gl.uniformMatrix3fv(this.m_uMVPMatrixLocation, false, mvp.arrayBuffer);
		gl.uniformMatrix3fv(this.m_uTMatrixLocation, false, this.uTMatrix.arrayBuffer);

		gl.uniform4fv(this.m_uColorLocation, this.uColor.arrayBuffer);

		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, this.uSampler.textureId);
		gl.uniform1i(this.m_uSamplerLocation, 0);

		
	}

	release(support : PSRenderSupport) : void 
	{ 
		var prog = this.m_ShaderProgram;
	}

	//Uniform
	public uSampler : PSTexture;
	public uModelMatrix : PSMatrix = new PSMatrix();
	public uVPMatrix : PSMatrix = new PSMatrix();
	public uColor : PSColor;
	public uTMatrix : PSMatrix = new PSMatrix();

	//Uniform Locations
	private m_uSamplerLocation : WebGLUniformLocation;
	private m_uMVPMatrixLocation : WebGLUniformLocation;
	private m_uColorLocation : WebGLUniformLocation;
	private m_uTMatrixLocation : WebGLUniformLocation;
	
}

class PSBuiltInGlowEffect extends PSMaterial implements IPSPostProcessEffect
{

	constructor(name : string, program : PSShaderProgram)
	{
		super(name, program);
	}

	setColorTexture(texture : WebGLTexture) : void
	{
		this.m_ColorTexture = texture;
	}

	setMVPMatrix(mvp : PSMatrix) : void
	{
		this.m_MVPMatrix = mvp;
	}
	setFBSize(width : number, height : number) : void
	{
		this.m_FBWidth = width;
 		this.m_FBHeight = height;
	}

	setPass(pass : number) : void
	{
		this.m_Pass = pass;
	}

	loadUniforms(gl : WebGLRenderingContext) : void
	{
		var prog = this.m_ShaderProgram;
		this.m_uSamplerLocation = gl.getUniformLocation(prog.programId, 'uSampler');
		this.m_uMVPMatrixLocation = gl.getUniformLocation(prog.programId, 'uMVPMatrix');
		this.m_uScreenSizeLocation = gl.getUniformLocation(prog.programId, 'uScreenSize');
		this.m_uPassLocation = gl.getUniformLocation(prog.programId, 'uPass');
	}

	apply(support : PSRenderSupport) : void 
	{ 
		this.releasePrevious(support);

		var gl = support.renderContext;
		var prog = this.m_ShaderProgram;

		prog.use(gl);

		gl.uniformMatrix3fv(this.m_uMVPMatrixLocation, false, this.m_MVPMatrix.arrayBuffer);
		gl.uniform2f(this.m_uScreenSizeLocation, this.m_FBWidth, this.m_FBHeight);

		gl.uniform1i(this.m_uPassLocation, this.m_Pass);

		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, this.m_ColorTexture);
		gl.uniform1i(this.m_uSamplerLocation, 0);
	}

	release(support : PSRenderSupport) : void 
	{ 
		
	}

	private m_MVPMatrix : PSMatrix = new PSMatrix();
	private m_ColorTexture : WebGLTexture;
	private m_FBWidth : number;
	private m_FBHeight : number;
	private m_Pass : number = 0;
	private m_uSamplerLocation : WebGLUniformLocation;
	private m_uMVPMatrixLocation : WebGLUniformLocation;
	private m_uScreenSizeLocation : WebGLUniformLocation;
	private m_uPassLocation : WebGLUniformLocation;
	
}

