

class PSMaterial implements IPSResource
{
	//current material
	static get CURRENT_MATERIAL() : PSMaterial { return PSMaterial.s_CurrentMaterial; }
		
	constructor(name : string, program : PSShaderProgram)
	{
		this.m_Path = name;
		this.m_ShaderProgram = program;
	}

	releasePrevious(support : PSRenderSupport) : void
	{
		if(PSMaterial.s_CurrentMaterial != null && PSMaterial.s_CurrentMaterial != this)
		{
			PSMaterial.s_CurrentMaterial.release(support);
		}

		PSMaterial.s_CurrentMaterial = this;
	}

	/** Do not call super.apply() or super.release() methods, overwrite them */
	//pure virtual methods (abstract methos not supported in typescript, runtime check is required)
	apply(support : PSRenderSupport) : void 
	{ 
		throw 'Implementation of apply() method required for PSMaterial derived class '+
				this['__proto__'].constructor.name+
				'. Do not call super.apply().'; 
	}

	release(support : PSRenderSupport) : void 
	{ 
		throw 'Implementation of release() method required for PSMaterial derived class '+
				this['__proto__'].constructor.name+
				'. Do not call super.release().'; 
	}

	loadUniforms(gl : WebGLRenderingContext) : void
	{

	}

	load(callback : IPSLoadCallback) : void
	{
		if(this.m_Loaded)
			return;

		this.m_Loading = true;

		this.m_LoaderCallback = callback;

		if(this.m_ShaderProgram.isLoaded())
		{
			this.loadUniforms(PSGame.CURRENT_GAME.renderSupport.renderContext);
			this.m_Loaded = true;

			if(this.m_LoaderCallback != null)
		    {
				this.m_LoaderCallback(this);
			}
		}
	}

	unload(callback : IPSLoadCallback) : void
	{
		this.m_LoaderCallback = callback;
	}

	isLoaded() : boolean
	{
		return this.m_Loaded;
	}

	getResourceName() : string
	{
		return this.m_Path;
	}

	getLoaderCallback() : IPSLoadCallback
	{
		return this.m_LoaderCallback;
	}

	//resource
	private m_Path : string = '';
	private m_Loaded : boolean = false;
	private m_Loading : boolean = false;
	private m_LoaderCallback : IPSLoadCallback;

	public m_ShaderProgram : PSShaderProgram;

	//static
	private static s_CurrentMaterial : PSMaterial;



};