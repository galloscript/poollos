/// <reference path="../system/ResourceManager.ts"/>
/// <reference path="../math/Matrix.ts"/>

//enum ETexFilterStyle
var ETexFilterStyle = {
	TS_STANDARD : 0,
	TS_PIXELART : 1
};

class PSTexture implements IPSResource
{
	public static TEXTURE_FILTER_STYLE : number = ETexFilterStyle.TS_STANDARD;

	constructor(path : string, repeat : boolean = false)
	{
		this.m_Path = path;
		this.m_Repeat = repeat;
	}

	get textureId() : WebGLTexture { return this.m_TextureId; }

	get width() : number { return this.m_POTWidth; }
	get height() : number { return this.m_POTHeight; }
	get sourceWidth() : number { return this.m_Width; }
	get sourceHeight() : number { return this.m_Height; }

	get textureMatrix() : PSMatrix { return this.m_TextureMatrix; }

	load(callback : IPSLoadCallback) : void
	{
		if(this.m_Loading || this.m_Loaded)
			return;

		this.m_Loading = true;

		this.m_LoaderCallback = callback;
		this.m_ImageElement = new Image();
		this.m_ImageElement['m_TextureObject'] = this; //uugh!
		this.m_ImageElement.onload = function() 
		{
			this['m_TextureObject'].innerCallback();
		}
		this.m_ImageElement.src = this.m_Path;
	}

	unload(callback : IPSLoadCallback) : void
	{
		this.m_LoaderCallback = callback;
	}

	isLoaded() : boolean
	{
		return this.m_Loaded;
	}

	getResourceName() : string
	{
		return this.m_Path;
	}

	getLoaderCallback() : IPSLoadCallback
	{
		return this.m_LoaderCallback;
	}

	innerCallback() : void
	{
		
		var image : any = this.m_ImageElement;
		this.m_POTWidth  = this.m_Width = this.m_ImageElement.width;
		this.m_POTHeight = this.m_Height = this.m_ImageElement.height;
		
		
		if(!PSTexture.isPowerOfTwo(this.m_Width) || !PSTexture.isPowerOfTwo(this.m_Height))
		{
			// Scale up the texture to the next highest power of two dimensions.
	        var canvas = document.createElement('canvas');
	        canvas.width = this.m_POTWidth = PSTexture.nextHighestPowerOfTwo(image.width);
	        canvas.height = this.m_POTHeight = PSTexture.nextHighestPowerOfTwo(image.height);
	        var ctx = <CanvasRenderingContext2D>canvas.getContext('2d');
	        ctx.drawImage(image, 0, 0, image.width, image.height);
	        //image = new Image('image/png');
	        //image.src = canvas.toDataUrl();
	        image = canvas;
		}

		this.createTexture(image, true);

		this.m_TextureMatrix = new PSMatrix();

		this.m_TextureMatrix.scale(this.m_Width / this.m_POTWidth, this.m_Height / this.m_POTHeight);

	    this.m_Loaded = true;
		this.m_Loading = false;
		
	    if(this.m_LoaderCallback != null)
	    {
			this.m_LoaderCallback(this);
		}
	}

	createTexture(image : any, genmipmap : boolean = true)
	{
		var gl = PSGame.CURRENT_GAME.renderSupport.renderContext;
		this.m_TextureId = gl.createTexture();
		this.updateTexture(image, genmipmap);
	}

	updateTexture(image : any, genmipmap : boolean = true)
	{
		var gl : WebGLRenderingContext = PSGame.CURRENT_GAME.renderSupport.renderContext;
		gl.bindTexture(gl.TEXTURE_2D, this.m_TextureId);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
		
		if(this.m_Repeat)
		{
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
	  		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
		}
		else
		{
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	  		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		}

		if(PSTexture.TEXTURE_FILTER_STYLE == ETexFilterStyle.TS_STANDARD)
		{
			
	        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

   		}
   		else if(PSTexture.TEXTURE_FILTER_STYLE == ETexFilterStyle.TS_PIXELART)
   		{
	        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
   		}

        if(genmipmap)
        {
	    	gl.generateMipmap(gl.TEXTURE_2D);
	    }
	    gl.bindTexture(gl.TEXTURE_2D, null);
	}

	static isPowerOfTwo(x : number) : boolean
	{
	    return (x & (x - 1)) == 0;
	}

	static nextHighestPowerOfTwo(x : number) : number
	{
	    --x;
	    for (var i = 1; i < 32; i <<= 1) {
	        x = x | x >> i;
	    }
	    return x + 1;
	}

	static getTextureResource(resource : string) : PSTexture
	{
		var res : any = PSResourceManager.instance.getResource(resource);

		if(res == null)
		{
			res = PSResourceManager.instance.loadResource(new PSTexture(resource));
		}

		return res;
	}

	private m_Path : string = '';
	private m_Loaded : boolean = false;
	private m_Loading : boolean = false;
	private m_LoaderCallback : IPSLoadCallback;

	private m_TextureId : WebGLTexture;
	private m_ImageElement : HTMLImageElement;
	private m_Width : number = 0;
	private m_Height : number = 0;
	private m_POTWidth : number = 0;
	private m_POTHeight : number = 0;
	private m_Repeat : boolean = false;
	
	private m_TextureMatrix : PSMatrix = new PSMatrix();
}