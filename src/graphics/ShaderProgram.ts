/// <reference path="../system/ResourceManager.ts"/>

class PSVertexAttribute
{
	constructor(n : string, l : number)
	{
		this.name = n;
		this.location = l;
	}

	public name : string;
	public location : number;
}

class PSBuiltInAttributes
{
	static POSITION : PSVertexAttribute = new PSVertexAttribute('aVertexPosition', 0);
	static TEXCOORD : PSVertexAttribute = new PSVertexAttribute('aTextureCoord', 1);
}

class PSShaderProgram implements IPSResource
{
	static get PROGRAM_IN_USE() : PSShaderProgram { return PSShaderProgram.s_ProgramInUse; }


	constructor(name : string)
	{
		this.m_Path = name;
		this.m_Loaded = false;
	}

	get programId() : WebGLProgram { return this.m_ProgramId; }

	load(callback : IPSLoadCallback) : void
	{
		//console.log('shader program load called for '+this.m_Path);

		this.m_LoaderCallback = callback || this.m_LoaderCallback;

		if(this.m_Loaded)
			return;

		var sourcesLoaded = true;

		//will not link until all attached shader sources has been compiled
		for(var shaderIndex in this.m_ShaderSources)
		{
			var shader = this.m_ShaderSources[shaderIndex];
			if(!shader.isLoaded())
			{
				sourcesLoaded = false;
				break;
			}
		}

		if(!sourcesLoaded)
			return;
		
		this.link();

		this.m_Loaded = true;

		if(this.m_LoaderCallback != null)
			this.m_LoaderCallback(this);
	}

	unload(callback : IPSLoadCallback) : void
	{
		this.m_LoaderCallback = callback;
	}

	isLoaded() : boolean
	{
		return this.m_Loaded;
	}

	getResourceName() : string
	{
		return this.m_Path;
	}

	getLoaderCallback() : IPSLoadCallback
	{
		return this.m_LoaderCallback;
	}

	attachSource(shader : PSShaderSource) : void
	{
		if(this.m_ShaderSources.indexOf(shader) == -1)
		{
			this.m_ShaderSources.push(shader);
		}
	}

	link() : void
	{
		var gl = PSGame.CURRENT_GAME.renderSupport.renderContext;
		this.m_ProgramId = gl.createProgram();

		for(var shaderIndex in this.m_ShaderSources)
		{
			var shader = this.m_ShaderSources[shaderIndex];
			gl.attachShader(this.m_ProgramId, shader.sourceId);
		}
		
		gl.linkProgram(this.m_ProgramId);
		if (!gl.getProgramParameter(this.m_ProgramId, gl.LINK_STATUS))
	 	{
			throw 'ERROR: Unable to link program '+this.m_Path;
		}
		
	  	this.use(gl);
	  	
	  	//all shaders can implement those vertex attributes
	  	gl.bindAttribLocation(this.m_ProgramId, PSBuiltInAttributes.POSITION.location, PSBuiltInAttributes.POSITION.name);
	  	gl.bindAttribLocation(this.m_ProgramId, PSBuiltInAttributes.TEXCOORD.location, PSBuiltInAttributes.TEXCOORD.name);

		PSShaderProgram.s_ProgramInUse = null;
	}

	use(gl : WebGLRenderingContext) : void
	{
		if(PSShaderProgram.s_ProgramInUse != this)
		{			
			gl.useProgram(this.m_ProgramId);
			PSShaderProgram.s_ProgramInUse = this;
		}
	}

	private m_ProgramId : WebGLProgram;

	private m_Loading : boolean = false;
	private m_Loaded : boolean = false;
	private m_Path : string = '';
	private m_LoaderCallback : IPSLoadCallback = null;
	private m_ShaderSources : Array<PSShaderSource> = [];

	private static s_ProgramInUse : PSShaderProgram;
}