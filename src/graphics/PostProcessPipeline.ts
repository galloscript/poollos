
interface IPSPostProcessEffect extends PSMaterial
{
	setColorTexture(texture : WebGLTexture) : void;
	setMVPMatrix(mvp : PSMatrix) : void;
	setFBSize(width : number, height : number) : void;
	setPass(pass : number) : void;
}

class PSPostProcessPipeline
{
	constructor(effect : IPSPostProcessEffect)
	{
		this.m_Effect = effect;
	}

	init(support : PSRenderSupport, gl : WebGLRenderingContext, screenW : number, screenH : number, passes : number = 1) : void
	{

		this.m_Passes = passes
		this.m_FBWidth = screenW;
		this.m_FBHeight = screenH; 

		if(!PSTexture.isPowerOfTwo(screenW) || !PSTexture.isPowerOfTwo(screenH))
		{
			// Scale up the texture to the next highest power of two dimensions.
	        this.m_FBWidth = PSTexture.nextHighestPowerOfTwo(screenW);
	       	this.m_FBHeight = PSTexture.nextHighestPowerOfTwo(screenH);
	    }

	    for(var i = 0; i < passes; i++)
	    {
	    	this.createRenderTarget(gl, this.m_FBWidth, this.m_FBHeight, i);
	    }

		this.m_FullScreenQuadVAO = PSBuiltInGfx.PS_DEFAULT_QUAD_VAO;

		this.m_MVP = new PSMatrix();
		this.m_MVP.scale(this.m_FBWidth, -this.m_FBHeight);
		this.m_MVP.translate(0, screenH); //0.587 * screenH
		this.m_MVP.concat(support.m_CurrentProjectionMatrix);
	}

	createRenderTarget(gl : WebGLRenderingContext, width : number, height : number, index : number) : void
	{
		// Create a color texture
		var colorTexture = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, colorTexture);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
		(<any>gl).texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

		// Create the depth texture
		/*
		var depthTexture = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, depthTexture);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT, screenW, screenH, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_SHORT, null);
		*/

		var renderbuffer = gl.createRenderbuffer();
	    gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
	    gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, this.m_FBWidth, this.m_FBHeight);

		var framebuffer = gl.createFramebuffer();
		gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);
		gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, colorTexture, 0);
		//gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, depthTexture, 0);
		gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);

		//set to default
		gl.bindTexture(gl.TEXTURE_2D, null);
		gl.bindFramebuffer(gl.FRAMEBUFFER, null);

		this.m_ColorTextures[index] = colorTexture;
		this.m_FrameBuffers[index] = framebuffer;
	}

	bind(support : PSRenderSupport, gl : WebGLRenderingContext) : void
	{
		gl.bindFramebuffer(gl.FRAMEBUFFER, this.m_FrameBuffers[0]);
		gl.blendFuncSeparate(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA, gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
	}

	render(support : PSRenderSupport, gl : WebGLRenderingContext)
	{	
		var meshVAO = PSBuiltInGfx.PS_DEFAULT_QUAD_VAO;

		gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
		
		this.m_Effect.setMVPMatrix(this.m_MVP);
		this.m_Effect.setFBSize(this.m_FBWidth, this.m_FBHeight);
		
		var nextFB = 1;
		var nextTexture = 0;
		for(var p = 0; p < this.m_Passes-1; ++p)
		{
			gl.bindFramebuffer(gl.FRAMEBUFFER, this.m_FrameBuffers[nextFB]);
			this.m_Effect.setColorTexture(this.m_ColorTextures[nextTexture]);
			this.m_Effect.setPass(p);
			this.m_Effect.apply(support);
			gl['ext'].vao.bindVertexArrayOES(meshVAO);
			gl.drawArrays(gl.TRIANGLES, 0, 6);
			gl['ext'].vao.bindVertexArrayOES(null);

			nextFB++;
			nextTexture++;
		}

		gl.bindFramebuffer(gl.FRAMEBUFFER, null);
		

		this.m_Effect.setColorTexture(this.m_ColorTextures[this.m_Passes-1]);
		this.m_Effect.setPass(this.m_Passes-1);
		this.m_Effect.apply(support);

		gl['ext'].vao.bindVertexArrayOES(meshVAO);
		gl.drawArrays(gl.TRIANGLES, 0, 6);
		gl['ext'].vao.bindVertexArrayOES(null);
	}

	private m_FrameBuffers : Array<WebGLFramebuffer> = [];
	private m_ColorTextures : Array<WebGLTexture> = [];

	private m_FullScreenQuadVAO : any;
	private m_Effect : IPSPostProcessEffect;
	public m_FBWidth : number;
	public m_FBHeight : number;
	private m_MVP : PSMatrix;

	public m_Passes : number = 1;

}