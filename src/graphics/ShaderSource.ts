/// <reference path="../system/ResourceManager.ts"/>

enum EShaderSourceType
{
	FRAGMENT_SHADER = 0x8B30,
	VERTEX_SHADER = 0x8B31
};

class PSShaderSource implements IPSResource
{
	constructor(name : string, 
				type : EShaderSourceType)
	{
		this.m_Path = name;
		this.m_SourceType = type;
	}

	get sourceId() : WebGLShader { return this.m_SourceId; }
	get sourceType() : EShaderSourceType { return this.m_SourceType; }

	isLoaded() : boolean
	{
		return this.m_Loaded;
	}

	getResourceName() : string
	{
		return this.m_Path;
	}

	innerCallback(data : string) : void
	{
		this.compileSource(data);

	    if(this.m_LoaderCallback != null)
	    {
			this.m_LoaderCallback(this);
		}
	}

	load(callback : IPSLoadCallback) : void
	{
		if(this.m_Loaded || this.m_Loading)
			return;

		this.m_Loading = true;

		this.m_LoaderCallback = callback;

		var shaderFileRequest = new XMLHttpRequest();
		shaderFileRequest['m_ShaderSource'] = this;
		shaderFileRequest.open("GET", this.m_Path, true);
		shaderFileRequest.responseType = "text";
		shaderFileRequest.onload = function() 
		{
			this['m_ShaderSource'].innerCallback(this.response);
		};
		
		shaderFileRequest.send();
	}

	unload(callback : IPSLoadCallback) : void
	{
		this.m_LoaderCallback = callback;
	}
	
	getLoaderCallback() : IPSLoadCallback
	{
		return this.m_LoaderCallback;
	}

	compileSource(source : string) : void
	{
		var gl = PSGame.CURRENT_GAME.renderSupport.renderContext;

		this.m_SourceId = gl.createShader(this.m_SourceType);
		gl.shaderSource(this.m_SourceId, source);
		gl.compileShader(this.m_SourceId);
		
		if (!gl.getShaderParameter(this.m_SourceId, gl.COMPILE_STATUS)) 
		{
			throw 'PSERROR: '+gl.getShaderInfoLog(this.m_SourceId);
		}

		this.m_Loaded = true;
	}

	private m_SourceType : EShaderSourceType;
	private m_SourceId : WebGLShader;
	private m_Loaded : boolean = false;
	private m_Loading : boolean = false;
	private m_Path : string = '';
	private m_LoaderCallback : IPSLoadCallback;
};