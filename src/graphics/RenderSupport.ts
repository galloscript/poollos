

class PSContextState implements PSPoolObject
{
	constructor(m : PSMatrix, c : PSColor)
	{
		this.matrix = new PSMatrix(m);
		this.color = new PSColor(c);
	}

	resetValues() : void
	{
		this.matrix.identity();
		this.color.setValues(1.0, 1.0, 1.0, 1.0);
	}

	public matrix : PSMatrix;
	public color : PSColor;
}

class PSRenderSupport
{
	constructor()
	{
		this.m_ContextStack = [];

		this.m_CurrentViewMatrix = new PSMatrix();
		this.m_CurrentProjectionMatrix = new PSMatrix();
		this.m_CurrentVPMatrix = new PSMatrix();

		this.pushState(this.m_CurrentModelMatrix, this.m_CurrentColor);
	}

	get renderContext() : WebGLRenderingContext { return this.m_GLContext; }

	set viewMatrix(vm : PSMatrix)
	{
		this.m_CurrentViewMatrix = vm;
		this.updateVPMatrix();
	}

	set projectionMatrix(pm : PSMatrix)
	{
		this.m_CurrentProjectionMatrix = pm;
		this.updateVPMatrix();
	}

	private updateVPMatrix()
	{
		this.m_CurrentVPMatrix.identity();
		this.m_CurrentVPMatrix.concat(this.m_CurrentViewMatrix);
		this.m_CurrentVPMatrix.concat(this.m_CurrentProjectionMatrix);
	}

	public setup(canvas : HTMLCanvasElement) : void
	{
		this.m_GLContext =  <WebGLRenderingContext>canvas.getContext('webgl', { alpha: false }) ||
						 	<WebGLRenderingContext>canvas.getContext('experimental-webgl', { alpha: false });

		if(this.m_GLContext == null)
		{
			console.error('Unnable to initalize WebGL context');
			return;
		}

		this.projectionMatrix = PSMatrix.make2DProjection(canvas.width, canvas.height);

		var gl = this.m_GLContext;

		gl['ext'] = {};
		gl['ext']['vao'] = null;
		
		var extContext = gl['rawgl'] || gl;

		var vao_ext = (
			  extContext.getExtension('OES_vertex_array_object') ||
			  extContext.getExtension('MOZ_OES_vertex_array_object') ||
			  extContext.getExtension('WEBKIT_OES_vertex_array_object')
		);

		if(vao_ext != null)
		{
			gl['ext']['vao'] = vao_ext;
		}
		else
		{
			throw 'WEBGL_ERR: OES_vertex_array_object not supported!';
		}

		gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
		gl.enable(gl.BLEND);
	}

	public pushState(m : PSMatrix, c : PSColor) : void
	{
		//var state : PSContextState = new PSContextState(this.m_CurrentModelMatrix, this.m_CurrentColor); 
		var state : PSContextState = PSPool.alloc(PSContextState);
		state.color.setFrom(this.m_CurrentColor);
		state.matrix.setFrom(this.m_CurrentModelMatrix);
		
		this.m_ContextStack.push(state);

		var newMatrix = PSPool.alloc(PSMatrix).setFrom(m).concat(this.m_CurrentModelMatrix);
		this.m_CurrentModelMatrix.setFrom(newMatrix);
		PSPool.free(newMatrix);
		this.m_CurrentColor.multiply(c);
	}

	public popState() : void
	{
		var state : PSContextState = this.m_ContextStack.pop();
		
		this.m_CurrentModelMatrix.setFrom(state.matrix);
		this.m_CurrentColor.setFrom(state.color);

		PSPool.free(state);

		//delete state;
	}

	public restoreBlendFunc()
	{
		var gl = this.m_GLContext;
		gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
		gl.enable(gl.BLEND);
	}


	public m_GLContext : WebGLRenderingContext;
	public m_ContextStack : Array<PSContextState> = [];
	public m_CurrentModelMatrix : PSMatrix = new PSMatrix();
	public m_CurrentColor : PSColor = new PSColor();

	public m_CurrentViewMatrix : PSMatrix;
	public m_CurrentProjectionMatrix : PSMatrix;
	public m_CurrentVPMatrix : PSMatrix;

}