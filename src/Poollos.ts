/// <reference  path="../src/display/Camera.ts" />
/// <reference  path="../src/display/Entity.ts" />
/// <reference  path="../src/display/Event.ts" />
/// <reference  path="../src/display/EventDispatcher.ts" />
/// <reference  path="../src/display/EventListener.ts" />
/// <reference  path="../src/display/Sprite.ts" />
/// <reference  path="../src/display/TextSprite.ts" />
/// <reference  path="../src/display/TouchEvent.ts" />

/// <reference  path="../src/graphics/BuiltInGfx.ts" />
/// <reference  path="../src/graphics/Material.ts" />
/// <reference  path="../src/graphics/PostProcessPipeline.ts" />
/// <reference  path="../src/graphics/RenderSupport.ts" />
/// <reference  path="../src/graphics/ShaderProgram.ts" />
/// <reference  path="../src/graphics/ShaderSource.ts" />
/// <reference  path="../src/graphics/StaticMesh.ts" />
/// <reference  path="../src/graphics/Texture.ts" />

/// <reference  path="../src/interpolators/AlphaTo.ts" />
/// <reference  path="../src/interpolators/ColorTo.ts" />
/// <reference  path="../src/interpolators/Delay.ts" />
/// <reference  path="../src/interpolators/EaseFunctions.ts" />
/// <reference  path="../src/interpolators/ScaleTo.ts" />

/// <reference  path="../src/math/Color.ts" />
/// <reference  path="../src/math/Matrix.ts" />
/// <reference  path="../src/math/Point.ts" />
/// <reference  path="../src/math/Rectangle.ts" />
/// <reference  path="../src/math/Circle.ts" />
/// <reference  path="../src/math/Transform.ts" />

/// <reference  path="../src/plugins/Tiled/TileMap.ts" />
/// <reference  path="../src/plugins/Tiled/TileMapDescriptor.ts" />
/// <reference  path="../src/plugins/Tiled/TileMapLayer.ts" />
/// <reference  path="../src/plugins/Tiled/TileSetDescriptor.ts" />

/// <reference  path="../src/shared/stdlib.ts" />

/// <reference  path="../src/system/Clock.ts" />
/// <reference  path="../src/system/Game.ts" />
/// <reference  path="../src/system/Input.ts" />
/// <reference  path="../src/system/Memory.ts" />
/// <reference  path="../src/system/ResourceManager.ts" />
/// <reference  path="../src/system/Screen.ts" />
/// <reference  path="../src/system/Timer.ts" />
