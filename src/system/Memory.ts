
//Memory utilities for Typescript and Javascript

interface PSPoolObject
{
	resetValues() : void;
}

class PSPool
{
	static createPool(classType : any, initialSize : number = 100)
	{
		var className = classType.name;
		if(className != 'Object' && PSPool.s_PoolMap[className] == null)
		{
			PSPool.s_PoolMap[className] = new PSPool(classType, initialSize);
		}
	}

	
	static alloc(classType : any) : any
	{
		var className = classType.name;
		return PSPool.s_PoolMap[className].getAvailableElement();

	}

	static free<T extends PSPoolObject>(obj : T) : void
	{
		var className = obj['__proto__'].constructor.name;
		PSPool.s_PoolMap[className].freeElement(obj);
	}
	

	static poolFor(classType : any) : PSPool
	{
		if(PSPool.s_PoolMap[classType.name] == null)
		{
			throw 'PSPool for '+classType.name+' uninitialized. Call PSPool.createPool('+classType.name+');';
		}

		return PSPool.s_PoolMap[classType.name];
	}

	constructor(classType : any, initialSize : number)
	{
		this.m_ClassType = classType;
		this.m_InitialSize = initialSize;

		this.fill();
	}

	private fill() : void
	{
		for(var i = 0; i < this.m_InitialSize ; i++)
		{
			this.m_FreeElements.push(new this.m_ClassType);
		}
	}

	private getAvailableElement() : any
	{
		if(this.m_FreeElements.length == 0)
		{
			this.fill();
		}

		var element = this.m_FreeElements.pop();
		this.m_UsedElements.push(element);
		element.resetValues();
		return element;
	} 

	private freeElement<T extends PSPoolObject>(element : T) : void
	{
		var index = this.m_UsedElements.indexOf(element);
		if(index != -1)
		{
			this.m_UsedElements.splice(index, 1);
			this.m_FreeElements.push(element);
		}
	}

	private m_ClassType : any;
	private m_FreeElements : Array<any> = [];
	private m_UsedElements : Array<any> = [];
	private m_InitialSize : number;
	private static s_PoolMap = {}
}