
interface IPSLoadCallback
{
	(newRes : IPSResource) : void;
}

interface IPSResource
{
	load(callback : IPSLoadCallback) : void;
	unload(callback : IPSLoadCallback) : void;
	isLoaded() : boolean;
	getResourceName() : string;
	getLoaderCallback() : IPSLoadCallback;
}

interface IPSResourcesMap
{
	[key : string] : IPSResource; 
}

class PSResourceManager
{
	static get instance() : PSResourceManager 
	{ 
		if(PSResourceManager.s_Instance == null)
			PSResourceManager.s_Instance = new PSResourceManager;

		return PSResourceManager.s_Instance;
	}

	constructor(){}

	get verbose() : boolean { return this.m_Verbose; }
	set verbose(b : boolean) { this.m_Verbose = b; }

	get resourceCountToLoad() : number { return this.m_ResourcesToLoad.length; }

	get loadingResources() : boolean { return this.m_ResourcesToLoad.length > 0; }

	get loadedPercentage() : number 
	{ 
		if(this.m_ResourcesToLoad.length > 0 || this.m_LoadedResources.length > 0)
		{
			return 1.0 - (this.m_ResourcesToLoad.length / (this.m_ResourcesToLoad.length + this.m_LoadedResources.length));  
		}
		else
		{
			return 0.0;
		}
	}

	loadResource(newRes : IPSResource, optCallback : IPSLoadCallback = PSResourceManager.onResourceLoaded) : any
	{
		if(newRes != null)
		{
			var asset = this.getResource(newRes.getResourceName());
			if(asset != null)
			{
				newRes = null;
				return asset;
			}
			else
			{
				this.m_ResourcesMap[ newRes.getResourceName() ] = newRes;
				this.m_ResourcesToLoad.push(newRes);
				newRes.load( optCallback );
				if(this.m_Verbose)
				{
					console.log('PSResourceManager :: Loading '+newRes.getResourceName()+'...');
				}
			}
		}

		return newRes;
	}

	getResource(name : string) : any
	{
		var asset = this.m_ResourcesMap[name];

		return asset;
	}

	static onResourceLoaded(newRes : IPSResource) : void
	{
		PSResourceManager.instance.resourceLoaded(newRes);
	}

	static onResourceUnLoaded(res : IPSResource) : void
	{
		PSResourceManager.instance.resourceUnLoaded(res);
	}

	resourceLoaded(newRes : IPSResource) : void
	{
		var assetIndex = this.m_ResourcesToLoad.indexOf(newRes);

		if(assetIndex != -1)
		{
			this.m_ResourcesToLoad.splice(assetIndex, 1);
		}

		this.m_LoadedResources.push(newRes);
		this.m_ResourcesMap[ newRes.getResourceName() ] = newRes;

		if(this.m_Verbose)
		{	
			console.log('PSResourceManager :: Loaded '+newRes.getResourceName());
		}
	}

	resourceUnLoaded(res : IPSResource) : void
	{
		var assetIndex = this.m_LoadedResources.indexOf(res);

		if(assetIndex != -1)
		{
			this.m_LoadedResources.splice(assetIndex, 1);
			this.m_ResourcesMap[res.getResourceName()] = null;
		}
	}

	updateLoads() : void
	{
		for(var resIndex in this.m_ResourcesToLoad)
		{
			var res = this.m_ResourcesToLoad[resIndex];
			res.load(res.getLoaderCallback());
		}
	}

	setResourceKey(key : string, res : IPSResource) : void
	{
		this.m_LoadedResources.push(res);
		this.m_ResourcesMap[key] = res;
	}

	private m_ResourcesToLoad : Array<IPSResource> = [];
	private m_LoadedResources : Array<IPSResource> = [];
	private m_ResourcesMap : IPSResourcesMap  = {};
	private m_Verbose : boolean = false;
	private static s_Instance = null;
};

var PS_RESMNGR = PSResourceManager.instance;

