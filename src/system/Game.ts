/// <reference path="../graphics/RenderSupport.ts"/>
/// <reference path="../display/Entity.ts"/>
/// <reference path="../system/ResourceManager.ts"/>

/** START requestAnimationFrame SETUP */
if(window.requestAnimationFrame == null)
{
	if(window['mozRequestAnimationFrame'] != null)
	{
		/** Firefox */ 
		window.requestAnimationFrame = window['mozRequestAnimationFrame'];
	}
	else if(window['msRequestAnimationFrame'] != null)
	{
		/** Firefox */
		window.requestAnimationFrame = window['msRequestAnimationFrame'];
	}
	else if(window['webkitRequestAnimationFrame'] != null)
	{
		/** Firefox */
		window.requestAnimationFrame = window['webkitRequestAnimationFrame'];
	}
}
/** END requestAnimationFrame SETUP */

class PSGame
{
	public static CURRENT_GAME : PSGame = null;

	static LOOP_FNC() : void
	{
		PSGame.CURRENT_GAME.requestFrame();
		window.requestAnimationFrame(PSGame.LOOP_FNC);
	}

	constructor()
	{
		PSGame.CURRENT_GAME = this;

		this.m_Clock = new PSClock();
		this.m_ClearColor = new PSColor(0.0, 0.0, 0.0, 1.0);
		this.m_ResourceManager = PSResourceManager.instance;
		this.m_Input = PSInput.instance;
	}

	get stage() : PSEntity { return this.m_Stage; }
	get currentCamera() : PSCamera { return this.m_CurrentCamera; }
	get canvas() : HTMLCanvasElement { return this.m_Canvas; }
	get renderSupport() : PSRenderSupport { return this.m_RenderSupport; }
	get clock() : PSClock { return this.m_Clock; }
	get input() : PSInput { return this.m_Input; }

	/** virtual */ 
	public init() : void
	{
		this.setupCanvas('DefaultCanvas');
	}
	
	/** virtual */ 
	public loadResources(manager : PSResourceManager) : void {} 

	/** virtual */ 
	public setupScene() : void {} 

	/** virtual */ 
	public update(clock : PSClock) : void 
	{
		this.m_Input.updateInput(this.m_Stage, this.m_CurrentTouchEvent);
		this.m_ResourceManager.updateLoads();
		this.m_Stage.innerUpdate(clock);
	}

	/** virtual */ 
	public render(support : PSRenderSupport, gl : WebGLRenderingContext) : void 
	{
		if(this.m_PostProcessPipeline != null)
		{
			this.m_PostProcessPipeline.bind(support, gl);
			//gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
		}
		
		gl.viewport(0, 0, this.m_Canvas.width, this.m_Canvas.height);
		gl.clearColor(this.m_ClearColor.red, 
					  this.m_ClearColor.green, 
					  this.m_ClearColor.blue, 
					  this.m_ClearColor.alpha);

		gl.clear(gl.COLOR_BUFFER_BIT);

		this.m_CurrentCamera.getTransformationMatrix(this.m_TempViewMatrix);
		support.viewMatrix = this.m_TempViewMatrix.invert();

		this.m_Stage.innerRender(support, gl);

		if(this.m_PostProcessPipeline != null)
		{
			//gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
			//gl.viewport(0, 0, this.m_PostProcessPipeline.m_FBWidth, this.m_PostProcessPipeline.m_FBHeight);
			this.m_PostProcessPipeline.render(support, gl);
		}
	} 

	public setupCanvas(canvasId : string = '', w : number = 0, h : number = 0)
	{
		if(canvasId == '' || document.getElementById(canvasId) == null)
		{
			var canvasDiv = document.createElement('div');
			canvasDiv.style.textAlign = 'center';
			canvasDiv.id = canvasId+'Div';
			this.m_Canvas = <HTMLCanvasElement>document.createElement('canvas');
			this.m_Canvas.width = (w > 0) ? w : 800;
			this.m_Canvas.height = (h > 0) ? h : 600;
			this.m_Canvas.id = canvasId;

			canvasDiv.appendChild(this.m_Canvas);
			document.body.appendChild(canvasDiv);
		}
		else
		{
			this.m_Canvas = <HTMLCanvasElement>document.getElementById(canvasId);
			this.m_Canvas.width = (w > 0) ? w : this.m_Canvas.width;
			this.m_Canvas.height = (h > 0) ? h : this.m_Canvas.height;
		}

		this.m_RenderSupport = new PSRenderSupport();
		this.m_RenderSupport.setup(this.m_Canvas);

		PSBuiltInGfx.load();

		this.m_Input.init();
	}

	public requestFrame()
	{
		this.m_Clock.startFrame();
		this.update(this.m_Clock);

		this.render(this.m_RenderSupport, this.m_RenderSupport.m_GLContext);

		this.m_Clock.endFrame();
	}

	public run() : void
	{
		PSPool.createPool(PSPoint);
		PSPool.createPool(PSMatrix);
		PSPool.createPool(PSContextState, 30);

		this.init();
		
		this.m_Stage = new PSEntity();
		this.m_CurrentCamera = new PSCamera();

		this.loadResources(PSResourceManager.instance);
		PSGame.runOnceLoaded(this);
		
		//this.setupScene();
		//this.startGameLoop();
	}
	
	private static runOnceLoaded(_game : PSGame)
	{
		if(PSResourceManager.instance.loadingResources)
		{
			setTimeout(PSGame.runOnceLoaded, 300, _game);
		}
		else
		{
			_game.setupScene();
			_game.startGameLoop();
		}
	}

	public startGameLoop()
	{
		this.m_Clock.init();
		PSGame.LOOP_FNC();
	}

	public m_ResourceManager : PSResourceManager;
	public m_Stage : PSEntity;
	public m_CurrentCamera : PSCamera;
	public m_Canvas : HTMLCanvasElement;

	public m_Input : PSInput;
	public m_RenderSupport : PSRenderSupport;
	public m_Clock : PSClock;

	public m_ClearColor : PSColor;

	public m_TempViewMatrix : PSMatrix = new PSMatrix();

	public m_CurrentTouchEvent : PSTouchEvent = new PSTouchEvent('');

	public m_PostProcessPipeline : PSPostProcessPipeline = null;
};