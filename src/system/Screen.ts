
class PSScreen
{
	static get width() : number { return PSGame.CURRENT_GAME.canvas.width; }
	static get height() : number { return PSGame.CURRENT_GAME.canvas.height; }

	static get center() : PSPoint
	{ 
		return new PSPoint(	PSGame.CURRENT_GAME.canvas.width * 0.5, 
							PSGame.CURRENT_GAME.canvas.height * 0.5); 
	}
}
