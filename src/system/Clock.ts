
class PSClock
{
	static get instance() : PSClock 
	{ 
		if(PSClock.s_Instance == null)
			PSClock.s_Instance = new PSClock;

		return PSClock.s_Instance;
	}

	constructor()
	{
		this.m_RealDeltaTime = 0.0;
		this.m_TimeScale = 1.0;
		this.m_LastFrameTimestamp = 0.0;
		this.m_StartFrameTimestamp = 0.0;

		this.m_FPS = 0.0;
		this.m_RealFPS= 0.0;
	}

	get time() : number { return new Date().getTime() / 1000.0; }
	get timeInMiliseconds() : number { return new Date().getTime(); }
	get deltaTime() : number { return this.m_RealDeltaTime * this.m_TimeScale; }
	get timeScale() : number { return this.m_TimeScale; }
	get fps() : number { return this.m_FPS; }
	//get realfps() : number { return this.m_RealFPS; }

	init() : void
	{
		this.m_LastFrameTimestamp = new Date().getTime();

	}

	startFrame() : void
	{
		this.m_StartFrameTimestamp = new Date().getTime();
		this.m_RealDeltaTime = (this.m_StartFrameTimestamp - this.m_LastFrameTimestamp) / 1000.0;
		this.m_RealFPS = 1.0 /  this.m_RealDeltaTime;
		this.m_RealDeltaTime = Math.min(this.m_RealDeltaTime, 0.033);
		//this.m_RealDeltaTime = Math.max(this.m_RealDeltaTime, 0.016);
		this.m_FPS = 1.0 / this.m_RealDeltaTime;
	}

	endFrame() : void
	{
		this.m_LastFrameTimestamp = this.m_StartFrameTimestamp;
	}

	private m_RealDeltaTime : number;
	private m_TimeScale : number;
	private m_LastFrameTimestamp : number;
	private m_StartFrameTimestamp : number;

	private m_FPS : number;
	private m_RealFPS : number;

	private static s_Instance : PSClock = null;

}
