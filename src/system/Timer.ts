/// <reference path="../display/Entity.ts"/>

class PSTimer extends PSEntity
{
	get duration() : number { return this.m_Duration; }
	get timeSinceStart() : number { return this.m_TimeSinceStart; }
	get started() : boolean { return this.m_Started; }
	get finished() : boolean { return this.m_Started && (this.m_TimeSinceStart >= this.m_Duration); }

	start(duration : number) : void
	{
		this.m_Duration = duration;
	}

	resetValues() : void
	{
		this.m_Duration = 0;
		this.m_TimeSinceStart = 0;
		this.m_Started = false;
	}


	getDelta(easeFunc : IPSEaseFunction = PSEaseFuncions.lineal) : number
	{
		if(this.m_Started && this.m_Duration > 0)
		{
			return easeFunc(this.m_Duration, this.m_TimeSinceStart);
		}
		else
		{
			return 0;
		}
	}

	update(clock : PSClock) : void
	{
		if(this.m_Started == false && this.m_Duration > 0)
		{
			this.m_Started = true;
		}

		if(this.m_Started)
		{
			if(this.m_TimeSinceStart < this.m_Duration)
			{
				this.m_TimeSinceStart += clock.deltaTime;
			}

			if(this.m_TimeSinceStart > this.m_Duration)
			{
				this.m_TimeSinceStart = this.m_Duration;
			}
		}
	}

	private m_Duration : number = 0;
	private m_TimeSinceStart : number = 0;
	private m_Started : boolean = false;
}