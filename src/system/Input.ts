
class PSKeyMap
{
	public static TAB = 9;
	public static ENTER = 13;
	public static SHIFT = 16;
	public static CTRL = 17;
	public static ALT = 18;
	public static SPACE = 32;
	public static ARROW_LEFT = 37;
	public static ARROW_UP = 38;
	public static ARROW_RIGHT = 39;
	public static ARROW_DOWN = 40;
 	
	public static DOT = 46;

	public static ZERO = 48;
	public static ONE = 49;
	public static TWO = 50;
	public static THREE = 51;
	public static FOUR = 52;
	public static FIVE = 53;
	public static SIX = 54;
	public static SEVEN = 55;
	public static EIGHT = 56;
	public static NINE = 57;

	public static A = 65;
	public static B = 66;
	public static C = 67;
	public static D = 68;
	public static E = 69;
	public static F = 70;
	public static G = 71;
	public static H = 72;
	public static I = 73;
	public static J = 74;
	public static K = 75;
	public static L = 76;
	public static M = 77;
	public static N = 78;
	public static O = 79;
	public static P = 80;
	public static Q = 81;
	public static R = 82;
	public static S = 83;
	public static T = 84;
	public static U = 85;
	public static V = 86;
	public static W = 87;
	public static X = 88;
	public static Y = 89;
	public static Z = 90;
}

class PSInput
{
	constructor()
	{

	}

	static get instance() : PSInput 
	{ 
		if(PSInput.s_Instance == null)
			PSInput.s_Instance = new PSInput;

		return PSInput.s_Instance;
	}

	static get mouseX() : number { return PSInput.s_Instance.m_MouseLocation.x; }
	static get mouseY() : number { return PSInput.s_Instance.m_MouseLocation.y; }

	static get mousePreviousX() : number { return PSInput.s_Instance.m_MousePreviousLocation.x; }
	static get mousePreviousY() : number { return PSInput.s_Instance.m_MousePreviousLocation.y; }

	static get mouseDown() : boolean { return PSInput.s_Instance.m_MouseDown; }
	static get mousePreviousDown() : boolean { return PSInput.s_Instance.m_MousePreviousDown; }

	static get mouseClick() : boolean { return PSInput.s_Instance.m_MouseDown && !PSInput.s_Instance.m_MousePreviousDown; }
	static get mouseRelease() : boolean { return !PSInput.s_Instance.m_MouseDown && PSInput.s_Instance.m_MousePreviousDown; }
	static get mouseMoving() : boolean 
	{ 
		return 	PSInput.s_Instance.m_MouseLocation.x != PSInput.s_Instance.m_MousePreviousLocation.x || 
				PSInput.s_Instance.m_MouseLocation.y != PSInput.s_Instance.m_MousePreviousLocation.y;
	}

	static isKeyPressed(keycode : number) : boolean
	{
		return PSInput.s_Instance.m_PressedKeys[keycode];
	}

	static isKeyJustPressed(keycode : number) : boolean
	{
		return PSInput.s_Instance.m_JustPressedKeys[keycode] > 0;
	}

	static isKeyJustReleased(keycode : number) : boolean
	{
		return PSInput.s_Instance.m_JustReleasedKeys[keycode] > 0;
	}

	static isMobileDevice() : boolean
	{
		return PSInput.s_Instance.m_MobileDevice;
	}

	get mouseX() : number { return this.m_MouseLocation.x; }
	get mouseY() : number { return this.m_MouseLocation.y; }

	get mousePreviousX() : number { return this.m_MousePreviousLocation.x; }
	get mousePreviousY() : number { return this.m_MousePreviousLocation.y; }

	get mouseDown() : boolean { return this.m_MouseDown; }
	get mousePreviousDown() : boolean { return this.m_MousePreviousDown; }

	get mouseClicked() : boolean { return this.m_MouseDown && !this.m_MousePreviousDown; }
	get mouseReleased() : boolean { return !this.m_MouseDown && this.m_MousePreviousDown; }
	get mouseMoving() : boolean 
	{ 
		return 	this.m_MouseLocation.x != this.m_MousePreviousLocation.x || 
				this.m_MouseLocation.y != this.m_MousePreviousLocation.y;
	}

	isKeyPressed(keycode : number) : boolean
	{
		return this.m_PressedKeys[keycode];
	}

	isMobileDevice() : boolean
	{
		return this.m_MobileDevice;
	}

	init() : void
	{
		var game = PSGame.CURRENT_GAME;
		var canvas = game.canvas;

		for(var i = 0; i < 256; ++i)
		{
			this.m_PressedKeys[i] = false;
			this.m_JustReleasedKeys[i] = 0;
			this.m_JustPressedKeys[i] = 0;
		}

		//check mobile devices
		if((navigator.userAgent.match(/Android/i)) || 
			(navigator.userAgent.match(/iPhone/i)) || 
			(navigator.userAgent.match(/iPod/i)) ||  
			(navigator.userAgent.match(/iPad/i)))
		{
			this.m_MobileDevice = true;
			canvas.style['webkitTapHighlightColor'] = 'rgba(0,0,0,0)';
	   	}

	   	if(!this.m_MobileDevice)
	   	{
	   		if(window.navigator['msPointerEnabled']) //Win 8 Touchscreen
	   		{
	   			window.addEventListener('MSPointerDown', PSInput.windowMouseEventHandler, true);
	   		}
	   		else
	   		{
	   			window.addEventListener('mousedown', PSInput.windowMouseEventHandler, true);
	   		}
	   	}

	   	window.addEventListener('keydown', PSInput.handleEventCaller, true);
		window.addEventListener('keyup',  PSInput.handleEventCaller, true);

		if (window.navigator['msPointerEnabled']) // WIN8
	   	{ 
	   		canvas.addEventListener("MSPointerDown", PSInput.handleEventCaller, true);
			canvas.addEventListener("MSPointerUp", PSInput.handleEventCaller, true);
			canvas.addEventListener("MSPointerMove", PSInput.handleEventCaller, true);
	   	} 
	   	else
	   	{
	   		canvas.addEventListener('mousedown', PSInput.handleEventCaller, true);
			canvas.addEventListener('mouseup', PSInput.handleEventCaller, true);
			canvas.addEventListener('mousemove', PSInput.handleEventCaller, true);
	   	}


		canvas.addEventListener("touchstart", PSInput.handleEventCaller, false);
		canvas.addEventListener("touchend", PSInput.handleEventCaller, false);
		canvas.addEventListener("touchcancel", PSInput.handleEventCaller, false);
		canvas.addEventListener("touchleave", PSInput.handleEventCaller, false);

		canvas.addEventListener("touchmove", PSInput.handleEventCaller, false);
	}

	static handleEventCaller(e : Event) : boolean
	{
		e = e || window.event;
		return PSInput.s_Instance.handleEvent(e);
	}

	static windowMouseEventHandler(e : Event) : void
	{
		e = e || window.event;
		var input = PSInput.s_Instance;

		if(((typeof e['toElement'] != 'undefined' && e['toElement'] != PSGame.CURRENT_GAME.canvas) ||
		    (typeof e['target'] != 'undefined' && e['target'] != PSGame.CURRENT_GAME.canvas)) && 
		    input.m_HaveFocus)
		{
			input.onFocusOut();
			input.m_HaveFocus = false;
		} 
		else
		{
			input.m_HaveFocus = true;
		}
	}

	updateInput(stage : PSEntity, touchEvent : PSTouchEvent) : void
	{
		
		

		var touchEventType : string = null;

		if(this.mouseClicked)
		{
			touchEventType = PSTouchEvent.TOUCH_BEGIN;
		} 
		else if(this.mouseReleased)
		{
			touchEventType = PSTouchEvent.TOUCH_END;
		} 
		else if(this.mouseMoving)
		{
			touchEventType = PSTouchEvent.TOUCH_MOVE;
		}

		if(touchEventType != null)
		{
			touchEvent.resetValues(touchEventType);
			var stagePoint = PSPool.alloc(PSPoint);
			this.getTransformedMousePoint(stagePoint);
			touchEvent.stageX = stagePoint.x;
			touchEvent.stageY = stagePoint.y;
			touchEvent.localX = this.m_MouseLocation.x;
			touchEvent.localY = this.m_MouseLocation.y;
			touchEvent.displacementX = this.m_MouseLocation.x - this.m_MousePreviousLocation.x;
			touchEvent.displacementY = this.m_MouseLocation.y - this.m_MousePreviousLocation.y;
			//TODO: velocity and other things

			this.updateTraversal(stage, touchEvent);
			PSPool.free(stagePoint);
		}

		for(var i = 0; i < 256; ++i)
	    {
	        this.m_JustReleasedKeys[i] -= (this.m_JustReleasedKeys[i] > 0) ? 1 : 0;
	        this.m_JustPressedKeys[i] -= (this.m_JustPressedKeys[i] > 0) ? 1 : 0;
	    }

	    this.m_MousePreviousLocation.setFrom(this.m_MouseLocation);
	    this.m_MouseLocation.setFrom(this.m_EventMouseLocation);
		this.m_MousePreviousDown = this.m_MouseDown;
		this.m_MouseDown = this.m_EventMouseDown;
		
	}

	updateTraversal(entity : PSEntity, touchEvent : PSTouchEvent)
	{
		if(entity.visible && entity.m_Color.alpha > 0.0)
		{
			var numChildren = entity.numChildren;
			for(var i = numChildren - 1; i >= 0; --i)
			{
				var child = entity.getChildAt(i);
				if(child != null)
				{
					this.updateTraversal(child, touchEvent);
				}
			}

			if(entity.captureInput && !touchEvent.captured)
			{
				entity.onNativeEvent(touchEvent);
			}
		}
	}

	handleEvent(e : Event) : boolean
	{
		e = e || window.event;
		var r = true;
		switch(e.type){
		case 'mousedown': case 'touchstart': case "MSPointerDown": r = this.onMouseDown(e); break;
		case 'mouseup': case 'touchend': case 'touchcancel': case "MSPointerUp": r = this.onMouseUp(e); break;
		case 'keydown': r = this.onKeyDown(e); break;
		case 'keyup': r = this.onKeyUp(e); break;
		case 'mousemove': case 'touchmove':  case "MSPointerMove": r = this.onMouseMove(e); break;
		}

		return r;
	}

	getTouch(touchIndex : number, pEvent : any) : any
	{
		if(pEvent && pEvent.targetTouches)
			return pEvent.targetTouches[touchIndex] || null;
			
		/*if(window.event && window.event.targetTouches)
			return window.event.targetTouches[touchIndex] || false;*/
	}

	onMouseDown(pe : any) : boolean
	{
		var event = this.m_MobileDevice ? this.getTouch(0, pe) : pe;
		if(event == null) return false;
		
		this.m_EventMouseLocation.x = event['offsetX'] || event['pageX'] - PSGame.CURRENT_GAME.canvas.offsetLeft;
		this.m_EventMouseLocation.y = event['offsetY'] || event['pageY'] - PSGame.CURRENT_GAME.canvas.offsetTop;

		this.onFocusIn();
		this.m_EventMouseDown = true;
		pe.preventDefault();
		pe.stopPropagation();
		//PSGame.CURRENT_GAME.canvas.addEventListener("touchmove", ss2d.Input.handleEventCaller, false);
	
		return false;
	}


	onMouseUp(pe : Event) : boolean
	{
		//var event = this.mMobileDevice ? this.getTouch(0, pe) : pe;
		
		//if((this.mMobileDevice && event) || !this.mMobileDevice){
			this.m_EventMouseDown = false;
			pe.preventDefault();
			pe.stopPropagation();
			//PSGame.CURRENT_GAME.canvas.removeEventListener("touchmove", ss2d.Input.handleEventCaller, false);
		//}
		return false;
	}

	
	onKeyDown(event : any) : boolean
	{
		if(this.m_HaveFocus)
		{ 
			var keyCode = parseInt(event.keyCode);
			if(!this.m_PressedKeys[keyCode])
				this.m_JustPressedKeys[keyCode] = 2;

			this.m_PressedKeys[keyCode] = true;
			if(keyCode == 37 || keyCode == 38 || keyCode == 39 || keyCode == 40 || keyCode == 32)
			{
				event.preventDefault();	
			}
			return false;
		}
		return true;
	}

	onKeyUp(event : any) : boolean
	{
		this.m_PressedKeys[parseInt(event.keyCode)] = false;
		this.m_JustReleasedKeys[parseInt(event.keyCode)] = 2;
		return !this.m_HaveFocus;
	}

	onMouseMove(pe : Event) : boolean
	{
		var event = this.m_MobileDevice ? this.getTouch(0, pe) : pe;
		
		if(event)
		{
			this.m_EventMouseLocation.x = event['offsetX'] || event['pageX'] - PSGame.CURRENT_GAME.canvas.offsetLeft;
			this.m_EventMouseLocation.y = event['offsetY'] || event['pageY'] - PSGame.CURRENT_GAME.canvas.offsetTop;
		}

		return false;
	}



	getTransformedMousePoint(targetPoint? : PSPoint) : PSPoint
	{
		targetPoint = targetPoint || new PSPoint();
		var tempMatrix = PSPool.alloc(PSMatrix);
		PSGame.CURRENT_GAME.currentCamera.getTransformationMatrix(tempMatrix).invert().transformPoint(this.m_MouseLocation, targetPoint);
		PSPool.free(tempMatrix);
		return targetPoint;
	}

	onFocusIn() : void
	{
		this.m_HaveFocus = true;
	}

	onFocusOut() : void
	{
		this.m_HaveFocus = false;
		this.m_EventMouseDown = false;
		
		//if(this.mCleanKeysOnFocusOut)
		//{
			for(var i = 0; i < 256; ++i)
		    {
		        this.m_PressedKeys[i] = false;
		        this.m_JustReleasedKeys[i] = 0;
		        this.m_JustPressedKeys[i] = 0;
		    }
		//}
	}

	private m_MobileDevice : boolean = false;

	private m_PressedKeys : Array<boolean> = new Array(256);
	private m_JustReleasedKeys : Array<number> = new Array(256);
	private m_JustPressedKeys : Array<number> = new Array(256);

	//frame to frame state
	private m_EventMouseLocation : PSPoint = new PSPoint(-50, -50);
	private m_MouseLocation : PSPoint = new PSPoint(-50, -50);
	private m_MousePreviousLocation : PSPoint = new PSPoint(-50, -50);
	private m_MouseDown : boolean = false;
	private m_MousePreviousDown : boolean = false;

	//browser events state
	private m_EventMouseDown : boolean = false;
	private m_HaveFocus : boolean = true;

	private static s_Instance : PSInput = null;
}