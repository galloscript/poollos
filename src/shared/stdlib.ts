/** stdlib.ts */

interface PSDictionary<T>
{
	[index : string] : T;
};

/*
class PSHashMap<T>
{
	
	get length() : number{ return m_Elements.length };
	set(key : string, value : T) : void
	{

	}
	private m_Elements : { [index: string]: T };
}
*/
