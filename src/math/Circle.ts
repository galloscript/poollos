/// <reference path="./Point.ts"/>
/// <reference path="./Rectangle.ts"/>
/// <reference path="../system/Memory.ts"/>

class PSCircle
{
	constructor(_locationX : number, _locationY : number, _radius : number)
	{
		this.radius = _radius;
		this.locationX = _locationX;
		this.locationY = _locationY;
	}
	
	intersectsPoint(other : PSPoint) : boolean
	{
		var thisPoint : PSPoint =  PSPool.alloc(PSPoint);
		thisPoint.setValues(this.locationX, this.locationY);
		var res = thisPoint.distanceTo(other) <= this.radius;
		PSPool.free(thisPoint);
		return res;
	}
	
	intersectsCircle(other : PSCircle) : boolean
	{
		var thisPoint : PSPoint =  PSPool.alloc(PSPoint);
		thisPoint.setValues(this.locationX, this.locationY);
		var otherPoint : PSPoint =  PSPool.alloc(PSPoint);
		otherPoint.setValues(other.locationX, other.locationY);
		PSPool.free(otherPoint);
		PSPool.free(thisPoint);
		return thisPoint.distanceTo(otherPoint) <= (this.radius + other.radius); 
	}
	
	intersectsRectangle(other : PSRectangle) : boolean
	{
		var res = false;
		var thisPoint : PSPoint =  PSPool.alloc(PSPoint);
		thisPoint.setValues(this.locationX, this.locationY);
		
		var otherPoint : PSPoint =  PSPool.alloc(PSPoint);
		otherPoint.setValues(other.x, other.y);
		res = res || this.intersectsPoint(otherPoint);
		otherPoint.setValues(other.x + other.width, other.y);
		res = res || this.intersectsPoint(otherPoint);
		otherPoint.setValues(other.x, other.y + other.height);
		res = res || this.intersectsPoint(otherPoint);
		otherPoint.setValues(other.x + other.width, other.y + other.height);
		res = res || this.intersectsPoint(otherPoint);
	
		res = res || other.containsPoint(thisPoint);
		
		PSPool.free(otherPoint);
		PSPool.free(thisPoint);
		return res;
	}
	
	updateLocation(_locationX : number, _locationY : number) : void
	{
		this.locationX = _locationX;
		this.locationY = _locationY;
	}
	
	radius : number = 0;
	locationX : number = 0;
	locationY : number = 0;
}