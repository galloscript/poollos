/// <reference path="./Matrix.ts"/>

/** math/Transform.ts */

class PSTransform
{
	constructor()
	{
		this.m_Pivot = new PSPoint(0, 0);
		this.m_Location = new PSPoint(0, 0);
		this.m_Scale = new PSPoint(1, 1);
		this.m_Rotation = 0.0;
		this.m_Width = 0.0;
		this.m_Height = 0.0;
	}

	get pivotX() : number { return this.m_Pivot.x; }
	set pivotX(v : number) { this.m_Pivot.x = v; }

	get pivotY() : number { return this.m_Pivot.y; }
	set pivotY(v : number) { this.m_Pivot.y = v; }

	get x() : number { return this.m_Location.x; }
	set x(v : number) { this.m_Location.x = v; }

	get y() : number { return this.m_Location.y; }
	set y(v : number) { this.m_Location.y = v; }

	get scaleX() : number { return this.m_Scale.x; }
	set scaleX(v : number) { this.m_Scale.x = v; }

	get scaleY() : number { return this.m_Scale.y; }
	set scaleY(v : number) { this.m_Scale.y = v; }

	get rotation() : number { return this.m_Rotation; }
	set rotation(angle : number) { this.m_Rotation = angle; }

	get width() : number { return this.m_Width; }
	set width(w : number) { this.m_Width = w; }

	get height() : number { return this.m_Height; }
	set height(h : number) { this.m_Height = h; }

	get pivot() : PSPoint { return this.m_Pivot; }
	set pivot(p : PSPoint) 
	{ 
		this.m_Pivot.x = p.x;  
		this.m_Pivot.y = p.y; 
	}

	get location() : PSPoint { return this.m_Location; }
	set location(p : PSPoint) 
	{ 
		this.m_Location.x = p.x;  
		this.m_Location.y = p.y; 
	}

	get scale() : any { return this.m_Scale; }
	set scale(p : any) 
	{
		if(p instanceof PSPoint)
		{
			this.m_Scale.x = p.x;  
			this.m_Scale.y = p.y;
		}
		else if(typeof p == 'number')
		{
			this.m_Scale.x = this.m_Scale.y = p;
		}
	}


	getTransformationMatrix(targetMatrix? : PSMatrix, includeWH : boolean = false) : PSMatrix
	{
		targetMatrix = targetMatrix || new PSMatrix();
	 	targetMatrix.identity();

	 	var tempScaleX = this.m_Scale.x;
	 	var tempScaleY = this.m_Scale.y;

	 	if(includeWH && this.m_Width != 0.0 && this.m_Height != 0.0)
	 	{  
	 		tempScaleX = this.m_Scale.x * this.m_Width;
	 		tempScaleY = this.m_Scale.y * this.m_Height;
	 	}
	 	
	 	if(this.m_Pivot.x != 0.0 || this.m_Pivot.y != 0.0)			{ targetMatrix.translate(-this.m_Pivot.x, -this.m_Pivot.y); 		}
	 	if(tempScaleX != 1.0 || tempScaleY != 1.0)			{ targetMatrix.scale(tempScaleX, tempScaleY); 				}
	 	if(this.m_Rotation != 0.0)			 						{ targetMatrix.rotate(this.m_Rotation); 							}
	 	if(this.m_Location.x != 0.0 || this.m_Location.y != 0.0)	{ targetMatrix.translate(this.m_Location.x, this.m_Location.y); 	}
	    
	 	return targetMatrix;
	}

	public m_Pivot : PSPoint;
	public m_Location : PSPoint;
	public m_Scale : PSPoint;
	public m_Rotation : number;
	public m_Width : number;
	public m_Height : number;
};