
class PSRectangle
{

	constructor(x = 0.0, y = 0.0, w=0.0, h=0.0)
	{
		this.m_X = x;
		this.m_Y = y;
		this.m_Width = w;
		this.m_Height = h;
	}

	get x() : number { return this.m_X; }
	set x(v : number) { this.m_X = v; }

	get y() : number { return this.m_Y; }
	set y(v : number) { this.m_Y = v; }

	get width() : number { return this.m_Width; }
	set width(v : number) { this.m_Width = v; }

	get height() : number { return this.m_Height; }
	set height(v : number) { this.m_Height = v; }

	clone() : PSRectangle
	{
		return new PSRectangle(this.m_X, this.m_Y, this.m_Width, this.m_Height);
	}

	rectUnion(other : PSRectangle) : PSRectangle
	{
		if(other.m_Width == 0 || other.m_Height == 0)
		{
			return this.clone();
		}

		if(this.m_Width == 0 || this.m_Height == 0)
		{
			return other.clone();
		}

		var left : number = Math.min(this.m_X, other.m_X);
		var top : number = Math.min(this.m_Y, other.m_X);
		var right : number = Math.max(this.m_X + this.m_Width, other.m_X + other.m_Width);
		var bottom : number = Math.max(this.m_X + this.m_Height, other.m_X + other.m_Height);

		return new PSRectangle(left, top, right - left, bottom - top);
	}

	unite(other : PSRectangle) : void
	{
		if(other.m_Width == 0 || other.m_Height == 0) return;

		if(this.m_Width == 0 || this.m_Height == 0)
		{
			this.m_X = other.m_X;
			this.m_Y = other.m_Y;
			this.m_Width = other.m_Width;
			this.m_Height = other.m_Height;
		}
		
		var left : number = Math.min(this.m_X, other.m_X);
		var top : number = Math.min(this.m_Y, other.m_Y);
		var right : number = Math.max(this.m_X + this.m_Width, other.m_X + other.m_Width);
		var bottom : number = Math.max(this.m_Y + this.m_Height, other.m_Y + other.m_Height);

		this.m_X = left;
		this.m_Y = top;
		this.m_Width = right - left;
		this.m_Height = bottom - top;

	}

	containsPoint(point : PSPoint) : boolean
	{
		var x : number = point.x;
		var y : number = point.y;

		return (x >= this.m_X && 
				y >= this.m_Y &&
            	x <= this.m_X + this.m_Width && 
            	y <= this.m_Y + this.m_Height);
	}

	intersectsWidth(toIntersect : PSRectangle) : boolean
	{
		return !( this.m_X + this.m_Width < toIntersect.m_X || 
				  this.m_X > toIntersect.m_X + toIntersect.m_Width || 
				  this.m_Y + this.m_Height < toIntersect.m_Y || 
				  this.m_Y > toIntersect.m_Y + toIntersect.m_Height );

	}

	intersectionResult(toIntersect : PSRectangle) : PSRectangle
	{
		if(!this.intersectsWidth(toIntersect))
    	{
	        return new PSRectangle(0, 0, 0, 0);
	    }
	    
	    //they do intersect, so get the intersection
	    var xMin : number, 
	    	xMax : number,
	     	yMin : number, 
	     	yMax : number;
	    
	    xMin = Math.max( this.m_X, toIntersect.m_X );
	    xMax = Math.min( this.m_X + this.m_Width, toIntersect.m_X + toIntersect.m_Width );
	    
	    yMin = Math.max( this.m_Y, toIntersect.m_Y );
	    yMax = Math.min( this.m_Y + this.m_Height, toIntersect.m_Y + toIntersect.m_Height );
	    
	    return new PSRectangle(xMin, yMin, xMax - xMin, yMax - yMin);
	}

	toString() : string
	{
		return '(x ='+this.m_X+', y ='+this.m_Y+', width ='+this.m_Width+', height='+this.m_Height+')';
	}

	private m_X : number;
	private m_Y : number;
	private m_Width : number;
	private m_Height : number;

};