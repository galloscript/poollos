/** math/Point.ts */

class PSPoint implements PSPoolObject
{
	/** public */
	//constructor(xy : number);
	//constructor(x : number, y : number);
	//constructor(p : PSPoint);
	constructor(a?, b?)
	{
		if(typeof a == 'number')
		{
			this.m_X = a;
			this.m_Y = (typeof b == 'number') ? b : a;
		}
		else if (a instanceof PSPoint)
		{
			this.m_X = a.m_X;
			this.m_Y = a.m_Y;
		}
		else
		{
			this.m_X = 0;
			this.m_Y = 0;
		}
	}

	get x() : number { return this.m_X; }
	get y() : number { return this.m_Y; } 

	set x(x : number){ this.m_X = x; }
	set y(y : number){ this.m_Y = y; } 
	
	get length() : number
	{
		return Math.sqrt((this.m_X * this.m_X) + (this.m_Y * this.m_Y));
	}

	setValues(x : number, y : number) : PSPoint
	{
		this.m_X = x;
		this.m_Y = y;
		return this;
	}

	setFrom(other : PSPoint) : PSPoint
	{
		this.m_X = other.m_X;
		this.m_Y = other.m_Y;
		return this;
	}

	normalize() : PSPoint 
	{
		var len = this.length;
  		var scalar : number = (len > 0) ? 1.0 / len : 0.0;
  		return PSPoint.multiply(this,  scalar);
	}

	multiply(p2 : PSPoint) : PSPoint;
	multiply(p2 : number) : PSPoint;
	multiply(p2 : any) : PSPoint
	{
		if(typeof p2 == 'number')
		{
			this.m_X *= p2;
			this.m_Y *= p2;
		}
		else if(typeof p2 == 'object')
		{
			this.m_X *= p2.m_X; 
			this.m_Y *= p2.m_Y;
		}
		return this;
	}

	add(p2 : PSPoint) : PSPoint
	{
		this.m_X += p2.m_X; 
		this.m_Y += p2.m_Y;
		return this;
	}

	subtract(p2 : PSPoint) : PSPoint
	{
		this.m_X -= p2.m_X;
		this.m_Y -= p2.m_Y;
		return this;
	}

	resetValues() : void
	{
		this.m_X = 0;
		this.m_Y = 0;
	}

	distanceTo(other : PSPoint) : number
	{
		return PSPoint.subtract(this, other).length;
	}

	/** static */
	static get X_AXIS() : PSPoint { return new PSPoint(1, 0); }
	static get Y_AXIS() : PSPoint { return new PSPoint(0, 1); }
	static get ZERO() : PSPoint { return new PSPoint(0, 0); }
	static get ONE() : PSPoint { return new PSPoint(1, 1); }

	static add(p1 : PSPoint, p2 : PSPoint) : PSPoint
	{
		return new PSPoint(p1.m_X + p2.m_X, p1.m_Y + p2.m_Y);
	}

	static subtract(p1 : PSPoint, p2 : PSPoint) : PSPoint
	{
		return new PSPoint(p1.m_X - p2.m_X, p1.m_Y - p2.m_Y);
	}

	static multiply(p1 : PSPoint, p2 : PSPoint) : PSPoint
	static multiply(p1 : PSPoint, p2 : number) : PSPoint
	static multiply(p1 : PSPoint, p2 : any) : PSPoint
	{
		if(typeof p2 == 'number')
		{
			return new PSPoint(p1.m_X * p2, p1.m_Y * p2);
		}
		else(typeof p2 == 'object')
		{
			return new PSPoint(p1.m_X * p2.m_X, p1.m_Y * p2.m_Y);
		}
	}

	static dot(p1 : PSPoint, p2 : PSPoint) : number
	{
		return (p1.m_X * p2.m_X) + (p1.m_Y * p2.m_Y);
	}

	static angle(p1 : PSPoint, p2 : PSPoint) : number
	{
		var dotProd : number = PSPoint.dot(p1, p2);
		var lenProd : number = p1.length * p2.length;
 		var divOp : number = dotProd / lenProd;
 		return Math.acos(divOp);
	}

	static distanceBetween(p1 : PSPoint, p2 : PSPoint) : number
	{
		return PSPoint.subtract(p1, p2).length;
	}

	/** private */
	private m_X : number;
	private m_Y : number;

	public static POOL = [];
};	


