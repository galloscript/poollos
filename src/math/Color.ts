
class PSColor
{
	static get WHITE() : PSColor { return new PSColor(1.0, 1.0, 1.0, 1.0); }

	/** public */
	//constructor(c : PSColor);
	//constructor(r : number, g : number, b : number, a : number);
	constructor(r : any=1.0, g : number=1.0, b : number=1.0, a : number=1.0)
	{
		if(r instanceof PSColor)
		{
			this.m_ColorVector = new Float32Array(r.m_ColorVector);
		}
		else
		{
			this.m_ColorVector = new Float32Array(4);

			if(typeof r == 'number')
			{
				this.m_ColorVector[0] = r;
				this.m_ColorVector[1] = g;
				this.m_ColorVector[2] = b;
				this.m_ColorVector[3] = a;
			}
		}
	}

	get red() : number { return this.m_ColorVector[0]; }
	set red(v : number) { this.m_ColorVector[0] = v; }

	get green() : number { return this.m_ColorVector[1]; }
	set green(v : number) { this.m_ColorVector[1] = v; }

	get blue() : number { return this.m_ColorVector[2]; }
	set blue(v : number) { this.m_ColorVector[2] = v; }

	get alpha() : number { return this.m_ColorVector[3]; }
	set alpha(v : number) { this.m_ColorVector[3] = v; }

	/// Returns a reference to the internal Float32Array 
	get arrayBuffer() : Float32Array
	{
		return this.m_ColorVector;
	}

	setFrom(other : PSColor) : PSColor
	{
		this.m_ColorVector[0] = other.m_ColorVector[0];
		this.m_ColorVector[1] = other.m_ColorVector[1];
		this.m_ColorVector[2] = other.m_ColorVector[2];
		this.m_ColorVector[3] = other.m_ColorVector[3];
		return this;
	}

	resetValues() : void
	{
		this.m_ColorVector[0] = 1.0;
		this.m_ColorVector[1] = 1.0;
		this.m_ColorVector[2] = 1.0;
		this.m_ColorVector[3] = 1.0;
	}

	setValues(r : number, g : number, b : number, a : number=1.0) : PSColor
	{
		this.m_ColorVector[0] = r;
		this.m_ColorVector[1] = g;
		this.m_ColorVector[2] = b;
		this.m_ColorVector[3] = a;
		return this;
	}

	multiply(other : PSColor) : PSColor
	{
		this.m_ColorVector[0] *= other.m_ColorVector[0];
		this.m_ColorVector[1] *= other.m_ColorVector[1];
		this.m_ColorVector[2] *= other.m_ColorVector[2];
		this.m_ColorVector[3] *= other.m_ColorVector[3];

		return this;
	}	


	divide(other : PSColor) : PSColor
	{
		this.m_ColorVector[0] /= other.m_ColorVector[0];
		this.m_ColorVector[1] /= other.m_ColorVector[1];
		this.m_ColorVector[2] /= other.m_ColorVector[2];
		this.m_ColorVector[3] /= other.m_ColorVector[3];

		return this;
	}

	getRGBHexString() : string
	{
		var r = Math.floor(this.m_ColorVector[0] * 255).toString(16); r = (r.length == 1)?'0'+r:r;
		var g = Math.floor(this.m_ColorVector[1] * 255).toString(16); g = (g.length == 1)?'0'+g:g;
		var b = Math.floor(this.m_ColorVector[2] * 255).toString(16); b = (b.length == 1)?'0'+b:b;
		
		return '#' + r + g + b;
	}

	static multiply(other : PSColor, result? : PSColor) : PSColor
	{
		result = result || new PSColor(this);
		return result.multiply(other);
	}	
	
	static divide(other : PSColor, result? : PSColor) : PSColor
	{
		result = result || new PSColor(this);
		return result.divide(other);
	}	

	private m_ColorVector : Float32Array;
};