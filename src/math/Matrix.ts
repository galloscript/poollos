/// <reference path="./Point.ts"/>
/// <reference path="../system/Memory.ts"/>

/** math/Matrix.ts */

class PSMatrix implements PSPoolObject
{
	/** public */
	constructor(m? : PSMatrix)
	{
		if(m instanceof PSMatrix)
		{
			this.m_M = new Float32Array(9);

			this.setValues(m.m_M);
		}
		else
		{
			this.m_M = new Float32Array(9);
			this.identity();
		}
	}

	setFrom(other : PSMatrix) : PSMatrix
	{
		return this.setValues(other.m_M);
	}
	
	setValues(otherArray : any) : PSMatrix
	{
		this.m_M[0] = otherArray[0]; /** a */
		this.m_M[1] = otherArray[1]; /** b */
		this.m_M[2] = otherArray[2];
		this.m_M[3] = otherArray[3]; /** c */
		this.m_M[4] = otherArray[4]; /** d */
		this.m_M[5] = otherArray[5];
		this.m_M[6] = otherArray[6]; /** tx */
		this.m_M[7] = otherArray[7]; /** ty */
		this.m_M[8] = otherArray[8];

		return this;
	}

	/// Resets this matrix to an identity matrix
	identity() : PSMatrix
	{
		this.m_M[0] = 1.0; /** a */
		this.m_M[1] = 0.0; /** b */
		this.m_M[2] = 0.0;
		this.m_M[3] = 0.0; /** c */
		this.m_M[4] = 1.0; /** d */
		this.m_M[5] = 0.0;
		this.m_M[6] = 0.0; /** tx */
		this.m_M[7] = 0.0; /** ty */
		this.m_M[8] = 1.0;

		return this;
	}

	/// Returns the determinant
	get determinant() : number
	{
		return (this.m_M[0] * this.m_M[4]) - (this.m_M[3] * this.m_M[1]);
	}

	/// Returns a reference to the internal Float32Array 
	get arrayBuffer() : Float32Array
	{
		return this.m_M;
	}

	/// multply this matrix with another and stores the result itslef
	concat(otherMatrix : PSMatrix) : PSMatrix
	{
		var newThis = PSPool.alloc(PSMatrix);

		newThis.m_M[0] = otherMatrix.m_M[0] * this.m_M[0] + (otherMatrix.m_M[3] * this.m_M[1]);
		newThis.m_M[1] = otherMatrix.m_M[1] * this.m_M[0] + (otherMatrix.m_M[4] * this.m_M[1]);
		newThis.m_M[3] = otherMatrix.m_M[0] * this.m_M[3] + (otherMatrix.m_M[3] * this.m_M[4]);
		newThis.m_M[4] = otherMatrix.m_M[1] * this.m_M[3] + (otherMatrix.m_M[4] * this.m_M[4]);
		newThis.m_M[6] = otherMatrix.m_M[0] * this.m_M[6] + (otherMatrix.m_M[3] * this.m_M[7]) + otherMatrix.m_M[6] * 1;
		newThis.m_M[7] = otherMatrix.m_M[1] * this.m_M[6] + (otherMatrix.m_M[4] * this.m_M[7]) + otherMatrix.m_M[7] * 1;

		this.setValues(newThis.m_M);
		PSPool.free(newThis);

		return this;
	}

	/// Move the matrix in x and y
	translate(x : number, y : number) : PSMatrix
	{
		this.m_M[6] += x;
		this.m_M[7] += y;
		return this;
	}

	/// Rotate the matrix in Z
	rotate(angle : number) : PSMatrix
	{
		var rotMatrix : PSMatrix = PSPool.alloc(PSMatrix);
 		rotMatrix.m_M[0] = Math.cos(angle);
 		rotMatrix.m_M[1] = -Math.sin(angle);
 		rotMatrix.m_M[3] = Math.sin(angle);
 		rotMatrix.m_M[4] = Math.cos(angle);
 		rotMatrix.m_M[6] = 0.0; 
		rotMatrix.m_M[7] = 0.0; 
 		var result = this.concat(rotMatrix);
 		PSPool.free(rotMatrix);
 		return result;
	}

	/// Scale the matrix in x and y
	scale(sx : number, sy : number) : PSMatrix
	{
		this.m_M[0] *= sx;
		this.m_M[1] *= sy;
		this.m_M[3] *= sx;
		this.m_M[4] *= sy;
		this.m_M[6] *= sx;
		this.m_M[7] *= sy;
		return this;
	}

	/// Transfomr a given point to the matrix space and return a new one
	transformPoint(point : PSPoint, targetPoint? : PSPoint) : PSPoint
	{
		targetPoint = targetPoint || new PSPoint(0.0, 0.0);	
		var orgX : number = point.x;
		targetPoint.x = (this.m_M[0] * point.x) + (this.m_M[3] * point.y) + this.m_M[6];
		targetPoint.y = (this.m_M[1] * orgX) + (this.m_M[4] * point.y) + this.m_M[7];	
		
		return targetPoint;
	}

	/// Inverts this matrix
	invert() : PSMatrix
	{
		var det : number = this.determinant;
		var newThis = PSPool.alloc(PSMatrix);
		newThis.m_M[0] = this.m_M[4]/det;
		newThis.m_M[1] = -this.m_M[1]/det;
		newThis.m_M[3] = -this.m_M[3]/det;
		newThis.m_M[4] = this.m_M[0]/det;
		newThis.m_M[6] = ((this.m_M[3] * this.m_M[7]) - (this.m_M[4] * this.m_M[6])) / det;
		newThis.m_M[7] = ((this.m_M[1] * this.m_M[6]) - (this.m_M[0] * this.m_M[7])) / det;
		this.setValues(newThis.m_M);
		PSPool.free(newThis);
		return this;
	}

	/// Returns an inverse of this matrix as a new matrix
	inverse() : PSMatrix
	{
		return new PSMatrix(this).invert();
	}

	resetValues() : void
	{
		this.identity();
	}

	static make2DProjection(width : number, height : number, target? : PSMatrix) : PSMatrix
	{
		var mat = target || new PSMatrix();
		mat.identity();
		mat.m_M[0] = 2 / width;
		mat.m_M[4] = -2 / height;
		mat.m_M[6] = -1;
		mat.m_M[7] = 1;
		mat.m_M[8] = 1;
		return mat;
	}

	/** private */
	private m_M : Float32Array;
}