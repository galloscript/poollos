/// <reference path="./EventDispatcher.ts"/>
/// <reference path="../math/Transform.ts"/>
/// <reference path="../math/Matrix.ts"/>
/// <reference path="../math/Color.ts"/>
/// <reference path="../math/Rectangle.ts"/>

/// <reference path="../display/TouchEvent.ts"/>

/** display/Entity.ts */

class PSEntity extends PSEventDispatcher
{
	constructor()
	{
		super();
		this.m_Transform = new PSTransform();
		this.m_Parent = null;
		this.m_Children = [];
		this.m_TimeScale = 1;
		this.m_Name = 'Unnamed';
		this.m_Visible = true;
		this.m_WorldMatrix = new PSMatrix();
		this.m_Color = new PSColor();
		this.m_Bounds = new PSRectangle();
		this.m_ClipRectangle = null;


	}

	/** Public properties */
	get transform() : PSTransform { return this.m_Transform; }

	get pivotX() : number { return this.m_Transform.m_Pivot.x; }
	set pivotX(v : number) { this.m_Transform.m_Pivot.x = v; }

	get pivotY() : number { return this.m_Transform.m_Pivot.y; }
	set pivotY(v : number) { this.m_Transform.m_Pivot.y = v; }

	get x() : number { return this.m_Transform.m_Location.x; }
	set x(v : number) { this.m_Transform.m_Location.x = v; }

	get y() : number { return this.m_Transform.m_Location.y; }
	set y(v : number) { this.m_Transform.m_Location.y = v; }

	get scaleX() : number { return this.m_Transform.m_Scale.x; }
	set scaleX(v : number) { this.m_Transform.m_Scale.x = v; }

	get scaleY() : number { return this.m_Transform.m_Scale.y; }
	set scaleY(v : number) { this.m_Transform.m_Scale.y = v; }

	get rotation() : number { return this.m_Transform.m_Rotation; }
	set rotation(angle : number) { this.m_Transform.m_Rotation = angle; }

	get width() : number { return this.m_Transform.m_Width; }
	set width(w : number) { this.m_Transform.m_Width = w; }

	get height() : number { return this.m_Transform.m_Height; }
	set height(h : number) { this.m_Transform.m_Height = h; }

	get name() : string { return this.m_Name; }
	set name(newName : string) { this.m_Name = newName; }

	get numChildren() : number { return this.m_Children.length; }

	get timeScale() : number { return this.m_TimeScale; }
	set timeScale(ts : number) { this.m_TimeScale = ts; }

	get visible() : boolean { return this.m_Visible; }
	set visible(v : boolean) { this.m_Visible = v; }

	get captureInput() : boolean { return this.m_CaptureInput; }
	set captureInput(v : boolean) { this.m_CaptureInput = v; }

	get color() : PSColor { return  this.m_Color; }

	get parent() : PSEntity { return this.m_Parent }

	get alpha() : number { return this.m_Color.alpha; }
	set alpha(a : number) { this.m_Color.alpha = a; }

	/** Returns a copy of the world matrix */
	public getWorldMatrix() : PSMatrix { return  new PSMatrix(this.m_WorldMatrix); }

	/** Override this method to change it's behaviour */
	/** virtual */ public update(clock : PSClock) : void {}

	/** Override this method to change the way is rendered */
	/** virtual */ public render(support : PSRenderSupport, gl : WebGLRenderingContext) : void {}

	/** called when the object is going to be destroyed */
	/** virtual */ public onDestroy() : void {}

	/** Add a child object */
	public addChild(child : PSEntity) : void
	{
		this.addChildAt(child, this.m_Children.length);
	}

	/** Add the child at the specified position */
	public addChildAt(child : PSEntity, index : number) : void
	{
		if(child == null){ return; }

		if(child.m_Parent == this){ return; }

		if(child.m_Parent != null)
		{
			child.m_Parent.removeChild(child);
		}

		child.m_Parent = this;
		index = Math.min(index, this.m_Children.length);
		this.m_Children.splice(index, 0, child);
		
		this.updateBounds(true);
	}

	/** Remove and returns the child reseting it's parent to null */
	public removeChild(child : PSEntity) : PSEntity
	{
		if(child == null)
			return;

		var index = this.m_Children.indexOf(child);
		if(index != -1)
		{
			this.m_Children.splice(index, 1);
			child.m_Parent = null;
		}

		return child;
	}
	
	/** Returns the first child found with the specified name */
	public getChildByName(childName : string, recursive : boolean = true) : PSEntity
	{
		for(var childIndex in this.m_Children)
		{
			var child = this.m_Children[childIndex];
			if(child != null && child.m_Name == childName)
				return child;
		}

		if(recursive)
		{
			for(var childIndex in this.m_Children)
			{
				var child = this.m_Children[childIndex];
				if(child != null)
				{
					var entity : PSEntity = child.getChildByName(childName, recursive);
					if(entity != null)
						return entity;
				}
			}
		}

		return null;
	}

	/** Returns the child in the specified position or null if the  */
	public getChildAt(index : number) : PSEntity
	{

		if(index >= this.m_Children.length)
			return null;

		return this.m_Children[index];
	}

	/** Returns the index of the specified child */
	public getChildIndex(child : PSEntity) : number
	{
		return this.m_Children.indexOf(child);
	}

	/** Sets the specified index to the child */
	public setChildIndex(child : PSEntity, index : number) : void
	{
		this.removeChild(child);
		this.addChildAt(child, index);
	}

	/** Remove this object from it's parent and destroys it */
	public destroy() : void
	{
		if(this.m_Parent != null)
		{
			this.m_Parent.destroyChild(this);
		}

		this.onDestroy();
	}

	/** Remove and destroy all object's children */
	public destroyAllChilds() : void
	{
		for(var childIndex in this.m_Children)
		{
			var child = this.m_Children[childIndex];
			child.m_Parent = null;
			this.addEntityToDestroy(child);
			child.onDestroy();
		}

		this.m_Children = [];
	}

	/** Updates the world matrix */
	public forceMatrixUpdate() : void
	{
		this.m_Transform.getTransformationMatrix(this.m_WorldMatrix, false);
		if(this.m_Parent != null)
		{
			this.m_WorldMatrix.concat(this.m_Parent.m_WorldMatrix);
		}
	}

	/** Internally updates the object (do not manually call this method) */
	public innerUpdate(time : PSClock) : void
	{
		this.forceMatrixUpdate();

		var prevDeltaTime = time.timeScale;
		time.timeScale = this.m_TimeScale;

		this.update(time);
		for(var childIndex in this.m_Children)
		{
			var child = this.m_Children[childIndex];
			if(child != null)
				child.innerUpdate(time);
		}
		time.timeScale = prevDeltaTime;
	}

	/** Internally render the object (do not manually call this method) */
	public innerRender(support : PSRenderSupport, gl : WebGLRenderingContext) : void
	{
		if(!this.m_Visible)
			return;
		
		//entity renders itself using with and height
		support.pushState(this.m_Transform.getTransformationMatrix(null, true), this.m_Color);
		this.render(support, gl);
		support.popState();
		support.pushState(this.m_Transform.getTransformationMatrix(null, false), this.m_Color);
		for(var childIndex in this.m_Children)
		{
			var child = this.m_Children[childIndex];
			if(child != null)
				child.innerRender(support, gl);
		}
		support.popState();
	}


	/** Add a child entity to destroy */
	public addEntityToDestroy(obj : PSEntity) : void 
	{
		if(obj != null)
			this.m_EntitiesToDestroy.push(obj);
	}

	/** Delete objects to be destroyed */
	public deletePendentObjects() : void
	{
		while(this.m_EntitiesToDestroy.length > 0)
		{
			var child = this.m_EntitiesToDestroy.splice(0, 1)[0];
			child.deletePendentObjects();
		}
	}
	
	/** Update the bounds rectangle with the object and childs boundaries */
	public updateBounds(updateChilds : boolean=false) : void
	{
		// v1 --- v2
	    // |      |
	    // v3 --- v4

		var pivot = PSPool.alloc(PSPoint).setFrom(this.m_Transform.pivot);
		pivot.x *= this.m_Transform.scale.x;
		pivot.y *= this.m_Transform.scale.y;
		var width = this.m_Transform.width * this.m_Transform.scale.x;
		var height = this.m_Transform.height * this.m_Transform.scale.y;

		var v1 : PSPoint = PSPool.alloc(PSPoint).setValues(-pivot.x * width, -pivot.y * height);
	    var v2 : PSPoint = PSPool.alloc(PSPoint).setValues(width - (pivot.x * width), - pivot.y * height);
	    var v3 : PSPoint = PSPool.alloc(PSPoint).setValues(-pivot.x * width, height - (pivot.y * height));
	    var v4 : PSPoint = PSPool.alloc(PSPoint).setValues(width - (pivot.x * width), height - (pivot.y * height));

	    v1 = this.m_WorldMatrix.transformPoint(v1, v1);
	    v2 = this.m_WorldMatrix.transformPoint(v2, v2);
	    v3 = this.m_WorldMatrix.transformPoint(v3, v3);
	    v4 = this.m_WorldMatrix.transformPoint(v4, v4);

	    var maxX : number, 
	    	maxY : number,
	    	minX : number,
	    	minY : number;

	   	maxX = v1.x > v2.x? v1.x :v2.x;
	    maxX = v3.x > v4.x? (v3.x > maxX? v3.x : maxX):(v4.x > maxX? v4.x : maxX);
	    maxY = v1.y > v2.y? v1.y :v2.y;
	    maxY = v3.y > v4.y? (v3.y > maxY? v3.y : maxY):(v4.y > maxY? v4.y : maxY);
	    minX = v1.x < v2.x? v1.x :v2.x;
	    minX = v3.x < v4.x? (v3.x < minX? v3.x : minX):(v4.x < minX? v4.x : minX);
	    minY = v1.y < v2.y? v1.y :v2.y;
	    minY = v3.y < v4.y? (v3.y < minY? v3.y : minY):(v4.y < minY? v4.y : minY);


	    this.m_Bounds.width = maxX-minX;
	    this.m_Bounds.height = maxY-minY;
	    this.m_Bounds.x = minX;
	    this.m_Bounds.y = minY;

	    for(var childIndex in this.m_Children)
		{
			var child = this.m_Children[childIndex];
			if(child != null && child.m_Visible && child.m_Color.alpha > 0.0)
			{
				var childBounds = child.getBounds(updateChilds);
				this.m_Bounds.unite(childBounds);
			}
		}

		PSPool.free(pivot)
		PSPool.free(v1)
		PSPool.free(v2)
		PSPool.free(v3)
		PSPool.free(v4)
	}

	/** Returns a reference to the bounds rectnagle */
	public getBounds(forceUpdate : boolean=true, updateChilds : boolean=true) : PSRectangle 
	{ 
		if(forceUpdate)
		{
			this.updateBounds(updateChilds);
		}

		return this.m_Bounds;
	}

	/** Returns true if the point is inside the boundaries of this object */
	public hitTestPoint(p : PSPoint) : boolean
	{
		return this.m_Bounds.containsPoint(p);
	}

	/** Converts a point to global to local object space, returns a new point */
	public globalToLocal(p : PSPoint, targetPoint? : PSPoint) : PSPoint
	{
	    this.forceMatrixUpdate();
	    return this.m_WorldMatrix.inverse().transformPoint(p, targetPoint);
	}

	/** Converts a point from local space to global space, returns a new point */
	public localToGlobal(p : PSPoint, targetPoint? : PSPoint) : PSPoint
	{
	    this.forceMatrixUpdate();
	    return this.m_WorldMatrix.transformPoint(p, targetPoint);
	}

	/** Called on every object when a Touch event occurs (finger touch or mouse click) */
	public onNativeEvent(event : PSTouchEvent) : void
	{
		if(!event.captured && this.m_Visible)
		{
			this.updateBounds(true);
			var stageLoc = PSPool.alloc(PSPoint);

			if(this.hitTestPoint(stageLoc.setValues(event.stageX, event.stageY)))
			{
				var hasListener : boolean = this.hasEventListener(event.type);
				event.captured = this.m_CaptureInput && hasListener;

				if(event.type == PSTouchEvent.TOUCH_BEGIN)
				{
					this.m_MouseDown = true;
					this.m_MouseDownPoint.setValues(event.stageX, event.stageY);
					if(hasListener)
					{
						event.target = this;
						this.dispatchEvent(event);
					}
				}
				else if(event.type == PSTouchEvent.TOUCH_TAP)
				{
					if(hasListener)
					{
						this.m_MouseDown = false;
						event.target = this;
						this.dispatchEvent(event);
					}
				}
				else if(event.type == PSTouchEvent.TOUCH_END && this.m_MouseDown)
				{
					this.m_MouseDown = false;
					if(hasListener)
					{
						event.target = this;
						this.dispatchEvent(event);
					}
				}
				else if(event.type == PSTouchEvent.TOUCH_MOVE)
				{
					if(!this.m_MouseDown)
					{
						if(hasListener)
						{
							var e : PSTouchEvent = new PSTouchEvent(PSTouchEvent.TOUCH_OVER);
							e.stageX = event.stageX;
							e.stageY = event.stageY;
							e.target = this;
							this.dispatchEvent(e);
						}
					}
					else if(hasListener)
					{
						event.target = this;
						this.dispatchEvent(event);
					}
				}
			}
			else
			{
				if(this.m_MouseDown)
				{
					this.m_MouseDown = false;

					var e : PSTouchEvent = new PSTouchEvent(PSTouchEvent.TOUCH_OUT);
					e.stageX = event.stageX;
					e.stageY = event.stageY;
					e.target = this;
					this.dispatchEvent(e);
				}
			}
			
			PSPool.free(stageLoc);
		}
	}

	/** Remove and destroy the child */
	private destroyChild(child : PSEntity) : void
	{
		var index = this.m_Children.indexOf(child);
		if(index != -1)
		{
			this.m_Children.splice(index, 1);
		}

		child.m_Parent = null;
		this.addEntityToDestroy(child);
	}

	//TODO: clip rectangle, material

	/** Properties */
	public m_Transform : PSTransform;
	public m_Parent : PSEntity;
	public m_Children : Array<PSEntity>;
	public m_TimeScale : number;
	public m_Name : string;
	public m_Visible : boolean;
	public m_WorldMatrix : PSMatrix;
	public m_Color : PSColor;
	public m_Bounds : PSRectangle;
	public m_ClipRectangle : PSRectangle;

	/** Inner deferred object destruction */
	private m_EntitiesToDestroy : Array<PSEntity> = [];
	private m_DirtyList : boolean = false;

	/** Touch events handling */
	public m_MouseDown : boolean = false;
	public m_MouseDownPoint : PSPoint = new PSPoint();
	public m_CaptureInput : boolean = false;

	public m_IncludeWidthHeight : boolean = false;
};
