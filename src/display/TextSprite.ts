/// <reference path="./Sprite.ts"/>

class PSTextSprite extends PSSprite
{
	constructor(text : string='', 
				fontSize : number=12, 
				fontFamily : string='Helvetica', 
				forcedWidth? : number, 
				forcedHeight? : number)
	{
		super();

		this.m_FontSize = fontSize;
		this.m_FontFamily = fontFamily;

		this.m_InnerCanvas = document.createElement('canvas');
		this.m_InnerContext = this.m_InnerCanvas.getContext('2d');
		this.m_Texture = new PSTexture('PSTextSprite-InnerTexture');

		this.m_Texture.createTexture(this.m_InnerCanvas, false);
		this.setText(text, forcedWidth, forcedHeight);

		this.m_MaxWidth = -1;
	}

	setText(text : string, forcedWidth? : number, forcedHeight? : number) : void
	{
		this.m_Text = text;
		this.m_MaxWidth = forcedWidth || -1;

		this.width = forcedWidth || 0;
		this.height = forcedHeight || this.m_FontSize + 4;

		this.compose();
	}

	compose() : void
	{

		this.m_InnerContext.textBaseline = 'top';
		this.m_InnerContext.font = (this.m_FontSize+'px '+this.m_FontFamily);
		
		if(this.width == 0)
		{
			this.width = this.m_InnerContext.measureText(this.m_Text).width;
		}

		if(!PSTexture.isPowerOfTwo(this.width) || !PSTexture.isPowerOfTwo(this.height))
		{
	        //this.width = PSTexture.nextHighestPowerOfTwo(this.width);
	        this.height = PSTexture.nextHighestPowerOfTwo(this.height);
		}

		this.m_InnerCanvas.width = this.width;
		this.m_InnerCanvas.height = this.height;

		this.m_InnerContext.fillStyle = this.m_Color.getRGBHexString();
		this.m_InnerContext.textBaseline = 'top';
		this.m_InnerContext.font = (this.m_FontSize+'px '+this.m_FontFamily);
		this.m_InnerContext.fillText(this.m_Text, 0, 0, (this.m_MaxWidth < 1) ? this.width : this.m_MaxWidth);

		this.m_Texture.updateTexture(this.m_InnerCanvas, false);
		
	}

	private m_Text : string;
	private m_FontSize : number;
	private m_FontFamily : string;
	private m_InnerCanvas : HTMLCanvasElement;
	private m_InnerContext : any;
	private m_MaxWidth : number;
	//private m_InnerTexture : PSTexture;
}