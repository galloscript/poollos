/// <reference path="./Event.ts"/>
/// <reference path="../math/Point.ts"/>

/** display/TouchEvent.ts */

class PSTouchEvent extends PSEvent
{
	/** static */
	static get TOUCH_BEGIN(){ return 'TouchBegin'; 	}
	static get TOUCH_END()	{ return 'TouchEnd'; 	}
	static get TOUCH_MOVE()	{ return 'TouchMove'; 	}
	static get TOUCH_OUT()	{ return 'TouchOut'; 	}
	static get TOUCH_OVER()	{ return 'TouchOver'; 	}
	static get TOUCH_TAP()	{ return 'TouchTap'; 	}
	static get TOUCH_PINCH(){ return 'TouchPinch'; 	}

	/** public */
	constructor(type : string)
	{
		super(type);
		this.m_LocalX = 0;
		this.m_LocalY = 0;
		this.m_StageX = 0;
		this.m_StageY = 0;
		this.m_DisplacementX = 0;
		this.m_DisplacementY = 0;
		this.m_Duration = 0;
		this.m_Velocity = 0;
		this.m_Factor = 0;
	}

	get localX() : number { return this.m_LocalX; }
	set localX(v : number) { this.m_LocalX = v; }

	get localY() : number { return this.m_LocalY; }
	set localY(v : number) { this.m_LocalY = v; }

	get stageX() : number { return this.m_StageX; }
	set stageX(v : number) { this.m_StageX = v; }

	get stageY() : number { return this.m_StageY; }
	set stageY(v : number) { this.m_StageY = v; }

	get displacementX() : number { return this.m_DisplacementX; }
	set displacementX(v : number) { this.m_DisplacementX = v; }

	get displacementY() : number { return this.m_DisplacementY; }
	set displacementY(v : number) { this.m_DisplacementY = v; }

	get duration() : number { return this.m_Duration; }
	set duration(v : number) { this.m_Duration = v; }

	get velocity() : number { return this.m_Velocity; }
	set velocity(v : number) { this.m_Velocity = v; }

	get factor() : number { return this.m_Factor; }
	set factor(v : number) { this.m_Factor = v; }


	toString() : string
	{
		return '(type = '+this.type+', lx = '+this.m_LocalX+', ly = '+this.m_LocalY+', sx = '+this.m_StageX+', sy = '+this.m_StageY+')';
	}

	resetValues(type : string) : void
	{
		super.resetValues(type);
	}

	/** private */
	private m_LocalX : number;
	private m_LocalY : number;
	private m_StageX : number;
	private m_StageY : number;
	private m_DisplacementX : number;
	private m_DisplacementY : number;
	private m_Duration : number;
	private m_Velocity : number;
	private m_Factor : number;
};
