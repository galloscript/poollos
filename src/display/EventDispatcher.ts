/// <reference path="./EventListener.ts"/>
/// <reference path="../shared/stdlib.ts"/>

/** display/EventDispatcher.ts */

class PSEventDispatcher extends PSEventListener
{
	constructor()
	{
		super();
		this.m_Listeners = {};
	}

	addEventListener(type : string, listener : PSEventListener ) : void
	{
		if(this.m_Listeners[type] == null)
		{
			var newVec : Array<PSEventListener> = [];
			newVec.push(listener);
			this.m_Listeners[type] = newVec;
		}
		else
		{
			if(this.m_Listeners[type].indexOf(listener) != -1)
			{
				this.m_Listeners[type].push(listener);
			}
		}
	}

	dispatchEvent(event : PSEvent) : void
	{
	    var elisteners : Array<PSEventListener> = this.m_Listeners[event.type];
	    if(elisteners != null)
	    {
	    	var size : number = elisteners.length;
	    	for(var i :number = 0; i < size; ++i)
	    	{
	    		elisteners[i].onEvent(event.type, event);
	    	}
	    }
	}

	trigger(type : string) : void
	{
		var elisteners : Array<PSEventListener> = this.m_Listeners[type];
	    if(elisteners != null)
	    {
	    	var size : number = elisteners.length;
	    	for(var i :number = 0; i < size; ++i)
	    	{
	    		elisteners[i].onEvent(type, new PSEvent(type));
	    	}
	    }
	}

	hasEventListener(type : string) : boolean
	{
		return (this.m_Listeners[type] != null);
	}

	removeEventListener(type : string, listener : PSEventListener) : void
	{
		if(this.hasEventListener(type) != null)
		{
			var indexOfListener : number = this.m_Listeners[type].indexOf(listener);
			if(indexOfListener != -1)
			{
				this.m_Listeners[type].splice(indexOfListener, 1);
			}
		}
	}

	clearEventListeners() : void
	{
		this.m_Listeners = {};
	}


	private m_Listeners : PSDictionary< Array<PSEventListener> >;
};
