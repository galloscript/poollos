/// <reference path="./Entity.ts"/>
/// <reference path="../graphics/Texture.ts"/>

class PSSprite extends PSEntity
{
	constructor(p? : PSTexture);
	constructor(p? : string);
	constructor(p? : any)
	{
		super();

		if(p != null)
		{
			this.setTexture(p);
		}
		else
		{
			this.m_Texture = PSBuiltInGfx.PS_DEFAULT_TEXTURE;
		}

		this.m_IncludeWidthHeight = true;
		this.setTextureTransform(0.0, 0.0, 1.0, 1.0, 0.0);
	}

	setTexture(resource : PSTexture) : void
	setTexture(resource : string) : void;
	setTexture(resource : any) : void
	{
		if(typeof resource == 'string')
		{
			this.m_Texture = PSResourceManager.instance.getResource(resource);

			if(this.m_Texture == null)
			{
				this.m_Texture = PSResourceManager.instance.loadResource(new PSTexture(resource));
			}
		}
		else if(resource instanceof PSTexture)
		{
			this.m_Texture = resource;
		}

		if(this.m_Texture.isLoaded())
		{
			this.setScaleFromTexture()
		}
	}
	
	setTextureTransform(panX : number, panY : number, scaleX : number, scaleY : number, rotation : number) : void
	{
		this.m_TextureTransform.identity();
		this.m_TextureTransform.translate(panX, panY);
		this.m_TextureTransform.scale(scaleX, scaleY);
		this.m_TextureTransform.rotate(rotation);
	};

	render(support : PSRenderSupport, gl : WebGLRenderingContext) : void
	{
		var material : PSBuiltInSpriteMaterial = PSBuiltInGfx.PS_DEFAULT_MATERIAL;
		var meshVAO = PSBuiltInGfx.PS_DEFAULT_QUAD_VAO;
		var tempTexMatrix : PSMatrix = PSPool.alloc(PSMatrix);

		material.uSampler = this.m_Texture;
		material.uVPMatrix = support.m_CurrentVPMatrix;
		material.uModelMatrix = support.m_CurrentModelMatrix;
		material.uColor = support.m_CurrentColor;
		tempTexMatrix.concat(this.m_Texture.textureMatrix);
		tempTexMatrix.concat(this.m_TextureTransform);
		material.uTMatrix = tempTexMatrix; 

		material.apply(support);

		gl['ext'].vao.bindVertexArrayOES(meshVAO);
		gl.drawArrays(gl.TRIANGLES, 0, 6);
		gl['ext'].vao.bindVertexArrayOES(null);
		
		PSPool.free(tempTexMatrix);
	}

	setScaleFromTexture() : void
	{
		this.width = this.m_Texture.sourceWidth;
		this.height = this.m_Texture.sourceHeight;
	}

	public m_Texture : PSTexture;
	private m_TextureTransform : PSMatrix = new PSMatrix();

};