/** display/Event.ts */

class PSEvent
{
	constructor(type : string)
	{
		this.m_Type = type;
	}

	get type() : string { return this.m_Type; }
	set type(t : string) { this.m_Type = t; }

	get captured() : boolean {return this.m_Captured; }
	set captured(c : boolean) { this.m_Captured = c; }

	get target() : PSEntity { return this.m_Target; }
	set target(e : PSEntity){ this.m_Target = e; }

	toString(){ return 'PSEvent("'+this.m_Type+'")'; }

	resetValues(type : string) : void
	{
		this.m_Type = type;
		this.m_Captured = false;
		this.m_Target = null;
	}

	private m_Type : string;
	private m_Captured : boolean = false;
	private m_Target : PSEntity = null;
};